({
	loadComments : function(cmp) {
		var comments = cmp.get("v.comments");
		var currentId = cmp.get("v.lawItemId");
		var viewComment = null;

		if ( window.location.hash.indexOf("#comment-")===0) {
			viewComment = window.location.hash.replace("#comment-",'');
		}

		if( null == currentId ) return;

		var action = cmp.get("c.getComments");
		
		action.setParams({
			lawItemId: currentId,
			excludedCommentIds:comments.map(i=>i.Id),
			requireComment: viewComment
		});
		
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				
				var returnValue = response.getReturnValue();
				comments =  comments.concat(returnValue.comments);

				cmp.set("v.comments", comments);
				cmp.set("v.unFetchedComments", (returnValue.total - comments.length));
            }
        });
		$A.enqueueAction(action);
	}
})