({
    formFields:  function() { 
        return [
            //First_Name
            {
                label: $A.get('$Label.c.First_Name'),
                ariaLabel: 'alFirstName', 
                required: true,
                serverField: 'FirstName',
                value: '',
                class: 'halfLine first'
            }, 
            //Last_Name
            {
                label: $A.get('$Label.c.Last_Name'),
                required: true,
                serverField: 'LastName',
                value: '',
                class:'halfLine'
            }, 
            // Company
            {
                label: $A.get('$Label.c.Amuta_Company'),
                value: '',
                serverField: 'Company'
            }, 
            //Role
            {
                label: $A.get('$Label.c.Role'),
                value: '',
                serverField: 'Role'
            }, 
            //Phone
            {
                label: $A.get('$Label.c.Phone'),
                ariaLabel: 'v.alPhone', 
                serverField: 'Phone',
                value: '',
                validation: [{
                    regex: /^\+?1?\s*?\(?\d{3}(?:\)|[-|\s])?\s*?\d{3}[-|\s]?\d{4}$/,
                    message: $A.get("$Label.c.Invalid_Phone_Number")
                }]
            }, 
           // Email
           {
                label: $A.get('$Label.c.Email'),
                ariaLabel: 'v.alEmail', 
                serverField: 'Email',
                value: '',
                disabled:true,
                validation:[{
                    regex: /(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})/,
                    message: $A.get("$Label.c.Invalid_email")
                }]
            },
            // CurrentPassword
            {
                label: $A.get('$Label.c.OldPassword'),
                ariaLabel: 'v.alPass',
                required: true,
                display: false,
                type:'password',
                serverField: 'currentPassword',
                validation: [
                    {
                        regex: /.{8}/,
                        message: $A.get("$Label.c.Password_Validation")
                    },

                    {
                        regex: /(^[\x20-\x7E]*$).*$/,
                        message: $A.get("$Label.c.Password_Validation")
                    }
                ]
            },
            // Password
            {
                label: $A.get('$Label.c.Password_Sign_in_Page'),
                ariaLabel: 'v.alPass',
                required: true,
                display: false,
                type:'password',
                serverField: 'newPassword',
                validation: [
                    {
                        regex: /.{8}/,
                        message: $A.get("$Label.c.Password_Validation")
                    },

                    {
                        regex: /(^[\x20-\x7E]*$).*$/,
                        message: $A.get("$Label.c.Password_Validation")
                    }
                ]
            },
            // PasswordVerification"
            {
                label: $A.get('$Label.c.Password_verification_Page_Sign_up'),
                ariaLabel: 'v.alPassVer',
                type:'password',
                required: true,
                display: false,
                allowBlur:true,
                validation: [{
                    match:"Password",
                    message: $A.get("$Label.c.Password_Verification_Error")
                }]
            }
        ];
       
    },
    setCleanForm: function(cmp){
        var action = cmp.get("c.getCurrentUser");
        var helper = this;

        action.setCallback(this, function(response) {
            var state = response.getState();
            var formFields =  helper.formFields();
            if (state === "SUCCESS") {
                var user = response.getReturnValue();
                formFields.forEach(function(formField){
                    formField.value = user[formField.serverField];
                });
                console.log(formFields);
                cmp.set("v.formFields", formFields);
            }
        });
        $A.enqueueAction(action);
    }
})