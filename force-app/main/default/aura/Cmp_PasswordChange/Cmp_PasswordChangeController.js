({
    doInit: function(cmp, event, helper) {
            helper.setCleanForm(cmp);
    },

    cancel: function(cmp, event, helper) {
        helper.setCleanForm(cmp);
    },

    showPasswordFields: function(cmp, event, helper) {
        var resetPasswordStatus = !cmp.get("v.resetPassword");
        cmp.set("v.resetPassword", resetPasswordStatus);

        var formFields = cmp.get("v.formFields");
        formFields.filter(s=>!s.serverField || ["currentPassword" , "newPassword"].indexOf(s.serverField) > -1 ).forEach(formField=>formField.display = resetPasswordStatus);
        cmp.set("v.formFields", formFields);
    },

    saveUser: function(cmp, event, helper) {
        var params = {};
        var isValid = true;
        var formFields = cmp.get("v.formFields");
        formFields.forEach(function(formField) {
            formField.error = '';
            if ( formField.display === false ) return;
            if( !formField.value ) {
                if ( formField.required ) {
                    formField.error =  $A.get("$Label.c.Mandatory_field");
                    isValid = false;
                    return;
                }
            }
            else {
                if( formField.validation ) {
                    formField.validation.forEach(function(rgx){
                        if ( rgx.match ) {
                            var matchValue = formFields.find(c=>c.serverField=="newPassword").value;
                            if ( formField.value != matchValue ) {
                                formField.error =  rgx.message;
                                isValid = false;
                                return;
                            }
                        }

                        var pattern = new RegExp(rgx.regex);
                        if( !pattern.test(formField.value) ){
                            formField.error =  rgx.message;
                            isValid = false;
                            return;
                        }
                    });
                }
            }
            params[formField.serverField] = formField.value;
        });
        cmp.set("v.formFields", formFields);

        if (! isValid ) return;

        var action = cmp.get("c.saveUserData");
        action.setParams(params);

        var callbackFunction = function(reponse) {

        }

        var ajaxCallWrapper = $A.get("e.c:Evt_sendAjaxCall"); 
        ajaxCallWrapper.setParams({ "callback": callbackFunction  });
        ajaxCallWrapper.setParams({"actionObject": action });
        ajaxCallWrapper.fire();	
    },

    forgotPass: function(cmp, event, helper) {
        console.log('forgotPass');
    },
})