({
	doInit: function (cmp, event, helper) {

		var lawItem = cmp.get("v.newLawItem");
		var filesForEditMode = cmp.get("v.filesForEditMode");
		var action = cmp.get("c.getObjects");

		switch ( cmp.get("v.currentRecordType") ) {
			case "Secondary Legislation Draft":
				cmp.find("nameOfLawItem").set("v.label","שם טיוטת חקיקת המשנה");
				cmp.find("brief").set("v.label","עיקרי טיוטת חקיקת המשנה");
			break;
			case "Support Test Draft":
				cmp.find("nameOfLawItem").set("v.label","שם מבחן התמיכה");
				cmp.find("brief").set("v.label","עיקרי מבחן התמיכה");
			break;
			case "Ministry Directive or Procedure Draft":
				cmp.find("nameOfLawItem").set("v.label","שם טיוטת הנוהל או ההנחיה");
				cmp.find("brief").set("v.label","עיקרי הנוהל או ההנחיה");
			break;
		}
		action.setCallback(this, function (response) {
			var status = response.getState();
			if (cmp.isValid() && status === "SUCCESS") {
				var res = JSON.parse(response.getReturnValue());
				var classLstforPills = res.lawClasses.map( lawClass => ({ 'type':'basic', 'label':lawClass.Name, 'Name': lawClass.Name, 'sfId':lawClass.Id  }));
				cmp.set("v.classLstStatic", classLstforPills);
				cmp.set("v.distLst", res.accounts);
			}
		});
		$A.enqueueAction(action);

		helper.loadAdvisors(cmp);

		if (filesForEditMode && filesForEditMode.MainFile ) {
			cmp.set("v.fileName", filesForEditMode.MainFile.Name);
		}

		if( cmp.get("v.classLst").length == 0 ) {
			var action = cmp.get("c.populateClasses");
			action.setParams({ lawItemId: lawItem.Id, lawsIds: [ lawItem.Law__c ] });
			action.setCallback(this, function (response) {
				var status = response.getState();
				if (cmp.isValid() && status === "SUCCESS") {
					var res = response.getReturnValue();
					var classLstforPills = res.map( classification=>({ sfId: classification.Id, type:'basic', label:classification.Name, Name:classification.Name }));
					cmp.set("v.classLst", classLstforPills);
				}
			});
			$A.enqueueAction(action);
		}
	},

	deleteFile: function (cmp, event, helper) {
		helper.deleteMainFile(cmp, function(){
			cmp.set("v.fileName", "עדיין לא נבחר קובץ");
			var filesForEditMode = cmp.get("v.filesForEditMode");
			filesForEditMode.MainFile = null;
			cmp.set("v.filesForEditMode", filesForEditMode);
		});	
	},
	
	handleFileUpload: function (cmp, event, helper) {
		helper.deleteMainFile(cmp, function(){
				var uploadedFiles = event.getParam("files");
				cmp.set("v.fileName", uploadedFiles[0].name);
				var filesForEditMode = cmp.get("v.filesForEditMode");
				if(!filesForEditMode) filesForEditMode = {};
				filesForEditMode.MainFile = {"Name":  uploadedFiles[0].name , "Id": uploadedFiles[0].documentId};
				cmp.set("v.filesForEditMode", filesForEditMode);
				cmp.set("v.showFileError", "false");
		});	
	},

	pillRemoval: function (cmp, evt, helper) {
		var clicked2 = evt.getParam("item").sfId;
		var items = cmp.get('v.classLst');
		var item = evt.getParam("index");
		items.splice(item, 1);
		cmp.set('v.classLst', items);
		var classLstToRemove = cmp.get("v.classLstToRemove");
		classLstToRemove.push(clicked2);
		cmp.set("v.classLstToRemove", classLstToRemove);
	},

	clickPills: function (cmp, evt, helper) {
		var clsLst = cmp.get("v.classLst");
		if (clsLst.length < 3) {
			cmp.set('v.showResults', "true");
		}
	},

	closeResults: function (cmp, evt, helper) {
		cmp.set('v.showResults', "false");
	},

	clickPill: function (cmp, evt, helper) {
		console.log("~~~ Page2 cmo function: clickPill ~~~");
		var clicked = evt.target.textContent;
		var pill = new Object();
		pill.type = 'basic';
		pill.label = clicked;
		pill.Name = clicked;
		pill.sfId = evt.target.nextSibling.textContent;
		var currentItems = cmp.get("v.classLst");
		if (currentItems.length < 3) {
			var exist = false;
			for (const key in currentItems) {
				if (pill.Name == currentItems[key].Name) {
					exist = true;
				}
			}
			if (!exist) {
				currentItems.push(pill);
				cmp.set("v.classLst", currentItems);
			}
		}
		cmp.set('v.showResults', "false");
	},

	onDistChange: function (cmp, evt, helper) {
		helper.onDropDownChange(cmp, "accountDropDown", "distLst" , "Publisher_Account");
		helper.loadAdvisors(cmp);
	},

	onAdvChange: function (cmp, evt, helper) {
		helper.onDropDownChange(cmp, "advisorDropDown", "advLst" , "Legal_Counsel_Name");
	},

	

	doValidation: function (cmp, event, helper) {
		var lawItem = cmp.get("v.newLawItem");
		var isValid = true;
		if (lawItem.Law_Item_Name__c == "" || lawItem.Law_Item_Name__c == null || lawItem.Law_Item_Name__c == undefined) {
			isValid = false;
			cmp.set("v.showNameError", "true");
		} else {
			cmp.set("v.showNameError", "false");
		}
		var fileName = cmp.get("v.fileName");
		if (fileName == "עדיין לא נבחר קובץ") {
			isValid = false;
			cmp.set("v.showFileError", "true");
		} else {
			cmp.set("v.showFileError", "false");
		}
		if (lawItem.Publisher_Account__c == "" || lawItem.Publisher_Account__c == null || lawItem.Publisher_Account__c == undefined) {
			isValid = false;
			cmp.set("v.showPubError", "true");
		} else {
			cmp.set("v.showPubError", "false");
		}
		if (lawItem.Legal_Counsel_Name__c == "" || lawItem.Legal_Counsel_Name__c == null || lawItem.Legal_Counsel_Name__c == undefined) {
			isValid = false;
			cmp.set("v.showCounsError", "true");
		} else {
			cmp.set("v.showCounsError", "false");
		}
		if (lawItem.Document_Brief__c == "" || lawItem.Document_Brief__c == null || lawItem.Document_Brief__c == undefined) {
			isValid = false;
			cmp.set("v.showMainMemoError", "true");
		} else {
			cmp.set("v.showMainMemoError", "false");
		}
		var classLst = cmp.get("v.classLst");
		if (classLst.length == 0) {
			isValid = false;
			cmp.set("v.showClassesError", "true");
		} else {
			cmp.set("v.showClassesError", "false");
		}
		return isValid;
	}
})