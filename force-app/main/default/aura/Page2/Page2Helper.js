({
    deleteMainFile : function(cmp, callbackAfterDelete) {
        var action = cmp.get("c.deleteFile");
        var filesForEditMode = cmp.get("v.filesForEditMode");
        if ( (filesForEditMode && filesForEditMode.MainFile) ) {
            action.setParams({ "fileId": filesForEditMode.MainFile.Id });
            action.setCallback(this, function (response) {
                var status = response.getState();
                if (cmp.isValid() && status === "SUCCESS") {
                    callbackAfterDelete();
                }
            });
            $A.enqueueAction(action);
        }
        else {
            callbackAfterDelete();
        }
    },

    onDropDownChange: function (cmp , dropDownId , listId, fieldName) {
        var lawItem = cmp.get("v.newLawItem");
		var selectedId = cmp.find(dropDownId).get("v.value");
		var selectableItems = cmp.get("v."+listId);
		var selectedItem = selectableItems.find(account=>account.Id == selectedId);
		
		if( !selectedItem ) { 
			lawItem[fieldName+'__c'] = null;
			lawItem[fieldName+'__r'] = null;
			cmp.set("v."+listId  , lawItem);
			return;
		}

		lawItem[fieldName+'__c'] = selectedItem.Id;
		lawItem[fieldName+'__r'] = selectedItem;
		cmp.set("v.newLawItem" , lawItem);
    },

	loadAdvisors: function (cmp) {

        var lawItem = cmp.get("v.newLawItem");

        cmp.set("v.advLst", []);
        if ( lawItem.Publisher_Account__c ) {
            var action = cmp.get("c.getAdvisor");
            action.setParams({ pubAccId: lawItem.Publisher_Account__c });
            action.setCallback(this, function (response) {
                var status = response.getState();
                if (cmp.isValid() && status === "SUCCESS") {
                    var resBody = JSON.parse(response.getReturnValue());
                    cmp.set("v.advLst", resBody);
                    if (resBody.length == 1) {
                        lawItem.Legal_Counsel_Name__c = resBody[0].Id;
                        lawItem.Legal_Counsel_Name__r = resBody[0];
                    }
                }
            });
            $A.enqueueAction(action);
        }
    }
})