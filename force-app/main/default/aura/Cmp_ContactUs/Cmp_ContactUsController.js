({
    doInit: function (cmp, event, helper) {        
        helper.setCleanForm(cmp);
    },

    closeModal: function (cmp, event, helper) {
        cmp.set("v.popup","");
        window.location.reload();
    },
    cancel: function (component, event, helper) {
        helper.setCleanForm(cmp);
    },

    sendMessage: function (cmp, event, helper) {
        var params = {};
        var isValid = true;
        var formFields = cmp.get("v.formFields");
        formFields.forEach(function(formFields){
            formFields.error = '';
            if(formFields.value) {
                if( formFields.validation ) {
                    formFields.validation.forEach(function(rgx){
                        var pattern = new RegExp(rgx.regex);
                        if( !pattern.test(formFields.value) ){
                            formFields.error =  rgx.message;
                            return;
                        }
                    });
                }
            }
            else {
                if ( formFields.required) {
                    formFields.error =  $A.get("$Label.c.Mandatory_field");
                    isValid = false;
                    return;
                }
            }
            if( formFields.serverField ) {
                params[formFields.serverField] = formFields.value;
            }
        });
        cmp.set("v.formFields", formFields);
        
        if (! isValid ) return;
        
        if( cmp.find('fileUpload').get('v.SObjectId') ) {
            params.caseId = cmp.find('fileUpload').get('v.SObjectId');
        }

        console.log(params);

        var action = cmp.get("c.SaveFeedback");

        action.setParams(params);

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {  
              cmp.set("v.popup","success");
            }
        });

        $A.enqueueAction(action);
    }
})