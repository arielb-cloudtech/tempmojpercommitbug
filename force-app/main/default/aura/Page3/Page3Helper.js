({
	/**
	 * Sets the dates for publication input and last date input 
	 * According to the number Of Days To Respond (for the lawItem)
	 * 
	 * @param {Object} cmp - the component object
	 * @param {Integer} numberOfDaysToRespond - Number of days to respond
	 */
	setDatesByNumber : function( cmp, numberOfDaysToRespond ) {
		
		var lawItem = cmp.get("v.newLawItem");
		var p3Data = {exp: ''};

		var lastDate = new Date();
		lastDate.setDate(lastDate.getDate() + numberOfDaysToRespond)
		p3Data.lastDate = lastDate.toISOString().substring(0,10);
		p3Data.lastHour = "23:59:00.000";

		var publicationDate = new Date();
		if ( lawItem.Publish_Date__c ) {
			publicationDate = new Date(lawItem.Publish_Date__c);
		}
		p3Data.pubDate = publicationDate.toISOString().substring(0,10);
		p3Data.pubHour = "08:00:00.000";

		/**
			lawItem.Last_Date_for_Comments__c = p3Data.lastDate + "T08:00:00.000Z";
		*/
		if ( numberOfDaysToRespond < 21 ){
			cmp.set("v.showExtra", "true");
		} else {
			cmp.set("v.showExtra", "false");
			p3Data.exp = '';
			cmp.set("v.reasonChoice", "נא לבחור")
		}

		cmp.set("v.p3Data", p3Data);

		lawItem.Number_of_Days_to_Respond__c = numberOfDaysToRespond;

		cmp.set("v.newLawItem", lawItem);
		
		
	},

	showDeductionReason: function( cmp, lawItem ) {
		if ( lawItem.Time_Deduction_Reason__c ) {
			switch(lawItem.Time_Deduction_Reason__c){
				case 'Urgent Requirement':
					cmp.set("v.reasonChoice", 'צורך חיוני או דחיפות בפרסום');
					break;
				case 'Minimal Public Impact':
					cmp.set("v.reasonChoice", 'השלכות מינימליות על הציבור');
					break;
				case 'Other':
					cmp.set("v.reasonChoice", 'אחר');
					cmp.set("v.showExtraOther", "true");
					break;
			}
		}
	}
})