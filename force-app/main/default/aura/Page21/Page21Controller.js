({ 	
	motherLawAreaOnChange: function (cmp, event, helper) {

		if(cmp.get("v.radioValue") == 2) {
			if(  cmp.get("v.searchVar") == null ||  cmp.get("v.searchVar") == undefined || cmp.get("v.searchVar") == '' ){

				cmp.set("v.MotherLawId",'');

				//cmp.set("v.preventCheckBoxUncheck",true);

				if( cmp.get("v.includeChkBx") ){ //checked



					helper.getParentLawsWithLawItemsRadio2(cmp, event, helper);
					console.log(">>>>>MOther value change - checked checkbox");


				} else { //unchecked
					var action = cmp.get("c.getParentLaws");
					action.setParams({ "searchVar": "", "inc": "false" });
					action.setCallback(this, function (response) {
						var status = response.getState();
						var result = JSON.parse(response.getReturnValue());
						cmp.set("v.parentLaws", result);
						cmp.set("v.parentLawsSearchRes", result);
						
					});
					$A.enqueueAction(action);
				}
			} 
			
			var lawLines = cmp.get("v.lawLines");
			for(const key in lawLines){
				lawLines[key] = new Object();
				lawLines[key].Name = '';
				lawLines[key].Id = '';
				lawLines[key].showResults = false;
				lawLines[key].showError = 'false';
			}
			cmp.set("v.lawLines", lawLines);
		}
	},

	checkBoxChange: function (cmp, event, helper) {
		var checkValue = cmp.get("v.includeChkBx");
		var parentLaws = [];


		if (checkValue) {
			
			if(cmp.get("v.radioValue") == 2){
				helper.getParentLawsWithLawItemsRadio2(cmp,event,helper);
			} else {
				helper.getParentLawsUnfilteredByMotherWithOrWithoutLawItems(cmp,event,helper, "true");
			}
		} else {

			if(cmp.get("v.radioValue") == 2){
				
				//if(cmp.get("v.preventCheckBoxUncheck") == false){
					var action = cmp.get("c.getParentLaws");
					action.setParams({ "searchVar": "", "inc": "false", "lawId" : cmp.get("v.MotherLawId") });
					action.setCallback(this, function (response) {
						var result = JSON.parse(response.getReturnValue());
						cmp.set("v.parentLaws", result);
						cmp.set("v.parentLawsSearchRes", result);
					});
					$A.enqueueAction(action);
				//}

			} else {

				var action = cmp.get("c.getParentLaws");
				action.setParams({ "searchVar": "", "inc": "false" });
				action.setCallback(this, function (response) {
					var status = response.getState();
					var result = response.getReturnValue();
					var parsedResult = JSON.parse(response.getReturnValue());
					for (const key in parsedResult) {
						var line = new Object();
						line.Id = parsedResult[key].Id;
						line.Name = parsedResult[key].Name;
						line.isLawItem = parsedResult[key].IsLi;
						parentLaws.unshift(line);
					}
					cmp.set("v.parentLaws", parentLaws);
					cmp.set("v.parentLawsSearchRes", parentLaws);
				});
				$A.enqueueAction(action);
			}
		}
	},

	checkBoxChangeFx: function (cmp, event, helper) {
		console.log("~~~ Page21 cmp function: checkBoxChangeFx ~~~");
	},

	radioChangeAction: function (cmp, event, helper) {
		cmp.set("v.MotherLawId",'');
		cmp.set("v.searchVar",'');

		if (cmp.get("v.radioValue") == '0') {
			if( cmp.get("v.includeChkBx") ){
				
				helper.getParentLawsUnfilteredByMotherWithOrWithoutLawItems(cmp,event,helper, "true");

			} else {

				var action = cmp.get("c.getParentLaws");
				action.setParams({ "searchVar": "", "inc": "false" });
				action.setCallback(this, function (response) {
					var result = JSON.parse(response.getReturnValue());
					cmp.set("v.parentLaws", result);
					cmp.set("v.parentLawsSearchRes", result);
				});
				$A.enqueueAction(action);
				cmp.set("v.showLists", "true");
			}


		}else{ //radio == 2

			var action = cmp.get("c.getMotherLaws");
			action.setParams({ "searchVar": "" });
			action.setCallback(this, function (response) {
				var result = JSON.parse(response.getReturnValue());
				cmp.set("v.childLaws", result);
				cmp.set("v.childLawsSearchRes", result);
			});
			$A.enqueueAction(action);
			cmp.set("v.showLists", "false");
			cmp.set("v.searchVar", "");
			cmp.set("v.lawChoice", "");

			if( cmp.get("v.includeChkBx") ){
				helper.getParentLawsWithLawItemsRadio2(cmp,event,helper);
			} else {
				helper.getParentLawsUnfilteredByMotherWithOrWithoutLawItems(cmp,event,helper, "false");
			}
		}

		var lines = [];
		var newLine = new Object();
		newLine.Name = '';
		newLine.Id = '';
		newLine.showResults = false;
		newLine.showError = 'false';
		lines.push(newLine);
		cmp.set("v.lawLines", lines);
		cmp.find("addNewLine").set("v.disabled", false);

		var lawItem = cmp.get("v.newLawItem");
		lawItem.Law__c = null;
		cmp.set("v.newLawItem", lawItem);
	},

	addLine: function (cmp, evt, helper) {
		setTimeout((evt) => {
			var lines = cmp.get("v.lawLines");
			if (lines.length < 5) {
				var newLine = new Object();
				newLine.Name = '';
				newLine.Id = '';
				newLine.showResults = false;
				newLine.showError = 'false';
				lines.push(newLine);
				cmp.set("v.lawLines", lines);
			}
			if(lines.length == 5){
				evt.getSource().set('v.disabled', true);
			}
		}, 100, evt);
	},

	removeLine: function (cmp, evt, helper) {
		console.log("~~~ Page21 cmp function: removeLine ~~~");
		setTimeout(() => {
			var lines = cmp.get("v.lawLines");
			var lineToRemove = evt.getSource().get("v.value");
			var removedLine = lines.splice(parseInt(lineToRemove), 1)[0];
			var removedLawLines = [];
			removedLawLines = cmp.get("v.removedLawLines");
			removedLawLines.push(removedLine);
			cmp.set("v.removedLawLines", removedLawLines);
			cmp.set("v.lawLines", lines);
			cmp.find("addNewLine").set('v.disabled', false);
		}, 100);
	},

	doInit: function (cmp, event, helper) {

		cmp.set("v.includeChkBx", false);

		var lawItem = cmp.get("v.newLawItem");

		var lawLines = [];
		if( lawItem.Related_to_Laws__r ) {
			lawLines = lawItem.Related_to_Laws__r.records;
		}
		
		cmp.set("v.lawLines", lawLines.map(lawLine=> ({showResults:false, Name:lawLine.Law__r.Name, Id:lawLine.Law__r.Id })));

		if ( lawItem.Law__c ) {
			cmp.set("v.radioValue", "2");
			cmp.set("v.showLists", "true");
			cmp.set("v.searchVar", lawItem.Law__r.Name);
		} else {
			cmp.set("v.radioValue", "0");
			cmp.set("v.showLists", "true");
		}
	},

	lawChoiceAction: function (cmp, event) {
		var action = cmp.get("c.getChildLaws");
		action.setParams({ parent: cmp.get("v.lawChoice") });
		action.setCallback(this, function (response) {
			var status = response.getState();
			if (cmp.isValid() && status === "SUCCESS") {
				cmp.set("v.childLaws", JSON.parse(response.getReturnValue()));
				cmp.set("v.childLawsSearchRes", JSON.parse(response.getReturnValue()));
			}
		});
		$A.enqueueAction(action);
	},

	handleKeyUp: function (cmp, evt, helper) {
		var lineNum = evt.target.classList[0].substring(8);
		var isEnterKey = true;
		// var parentLaws = cmp.get("v.parentLaws");
		var parentLaws = [];
		var lawlines = cmp.get("v.lawLines");
		var queryTerm = lawlines[lineNum].Name;

		var key = evt.key;
		if(key == "ArrowUp" || key == "ArrowDown" || key == "ArrowRight" || key == "ArrowLeft" ){
			//prevent closing drop down - search is based on string of arrow key and returns 0 results
		} else{


			if (isEnterKey && queryTerm != '') {
				setTimeout(function () {
					var action = cmp.get("c.getParentLaws");
					action.setParams({ "searchVar": queryTerm, "inc": cmp.get("v.includeChkBx") });
					action.setCallback(this, function (response) {
						var parsedResult = JSON.parse(response.getReturnValue());
						for (const key in parsedResult) {
							var line = new Object();
							line.Id = parsedResult[key].Id;
							line.Name = parsedResult[key].Name;
							line.isLi = parsedResult[key].isLi;
							// parentLaws.unshift(line);
							parentLaws.push(line);
						}
						cmp.set("v.parentLaws", parentLaws);
						cmp.set("v.parentLawsSearchRes", parentLaws);
					});
					$A.enqueueAction(action);
				}, 10);
			}
		}
	},
	handleClick: function (cmp, evt) {
		var rowNum = evt.target.classList[0].substring(8);
		var lines = cmp.get("v.lawLines");
		lines[parseInt(rowNum)].showResults = true;
		cmp.set("v.lawLines", lines);
	},

	clear: function (cmp, evt) {
		var rowNum = evt.target.classList[0].substring(8);
		setTimeout(function (cmp, rowNum) {
				var lines = cmp.get("v.lawLines");
				lines[rowNum].showResults = false;
				cmp.set("v.lawLines", lines);
		}, 200, cmp, rowNum);
	},
	clickOption: function (cmp, evt, helper) {
		var rowNum = evt.target.classList[0].substring(8);
		var clickedName = evt.target.textContent;
		var clickedId = evt.target.nextSibling.textContent;
		var lawLines = JSON.parse(JSON.stringify(cmp.get("v.lawLines")));
		var newLine = new Object();
		newLine.Name = clickedName;
		newLine.Id = clickedId;
		newLine.showResults = false;
		newLine.showError = 'false';
		lawLines[rowNum] = newLine;
		cmp.set("v.lawLines", lawLines);
	},

	handleKeyUpAmend: function (cmp, evt) {
		var isEnterKey = true;
		var childLaws = cmp.get("{!v.childLaws}");
		var childLawsRes = [];
		var queryTerm = cmp.find('enter-search1').get('v.value');

		var key = evt.key;
		if(key == "ArrowUp" || key == "ArrowDown" || key == "ArrowRight" || key == "ArrowLeft" ){
			//prevent closing drop down - search is based on string of arrow key and returns 0 results
		} else{


			if (isEnterKey && queryTerm != '') {
				setTimeout(function () {
					var action = cmp.get("c.getMotherLaws");
					action.setParams({ "searchVar": queryTerm });
					action.setCallback(this, function (response) {
						var status = response.getState();
						var result = JSON.parse(response.getReturnValue());
						cmp.set("v.childLawsSearchRes", result);
						console.log(">>>>>HERE");
						
					});
					$A.enqueueAction(action);
				}, 10);
			}
		}
	},

	handleClickAmend: function (cmp, evt) {
		console.log("~~~ Page21 cmp function: handleClickAmend ~~~");
		console.log("v.lawLines", cmp.get("v.lawLines"));
		cmp.set("v.showResults", "true");
	},

	clearAmend: function (cmp, evt) {
		var childLawsRes = cmp.get("{!v.childLaws}");
		setTimeout(function () {
			cmp.set("v.childLawsSearchRes", childLawsRes);
			cmp.set("v.showResults", "false");
		}, 200);
	},


	clickOptionAmend: function (cmp, evt, helper) {
		cmp.set("v.showResults", "false");

		var selectedLawId = evt.currentTarget.nextSibling.innerText;
		
		cmp.set("v.MotherLawId", selectedLawId);

		if( cmp.get("v.includeChkBx") ){//checked
			setTimeout(function (cmp, selectedLawId) { 
				helper.getParentLawsWithLawItemsRadio2(cmp,evt,helper);

				var lawItem = cmp.get("v.newLawItem");
				var clicked = evt.target.textContent;
				cmp.set("v.searchVar", clicked);
				cmp.set("v.lawChoice", evt.target.nextSibling.textContent);
				lawItem.Law__c = evt.target.nextSibling.textContent;
				cmp.set("v.showLists", "true");
				cmp.set("v.newLawItem", lawItem);

			}, 10, cmp, selectedLawId);

		} else{ //unchecked
			setTimeout(function (cmp, selectedLawId) {
				var action = cmp.get("c.getParentLaws");
				action.setParams({ "searchVar": "", "inc": "false", "lawId" : selectedLawId});
				action.setCallback(this, function (response) {
					var result = JSON.parse(response.getReturnValue());
					console.log('result:', result);
					cmp.set("v.parentLaws", result);
					cmp.set("v.parentLawsSearchRes", result);
				});
				$A.enqueueAction(action);
				var lawItem = cmp.get("v.newLawItem");
				var clicked = evt.target.textContent;
				cmp.set("v.searchVar", clicked);
				cmp.set("v.lawChoice", evt.target.nextSibling.textContent);
				lawItem.Law__c = evt.target.nextSibling.textContent;
				cmp.set("v.showLists", "true");
				cmp.set("v.newLawItem", lawItem);
			}, 10, cmp, selectedLawId);
		}
	},

	doValidation: function (cmp, event, helper) {
		var lawLines = cmp.get("v.lawLines");
		var lawIds = [];
		var lawLinesNames = [];
		var errorCounter = 0;

		for (const key in lawLines) {
			lawLinesNames.push(lawLines[key].Name);
			if (lawLines[key] && lawLines[key].Id && lawLines[key].Id.startsWith("a0A")) {
				lawIds.push(lawLines[key].Id);
			}
		}
		var amendLaw = cmp.get("v.newLawItem").Law__c;
		if(amendLaw){
			lawIds.push(amendLaw);
		}

		var iterator_1;
		var iterator_2;
		for(iterator_1 = 0 ; iterator_1 < lawLines.length ; iterator_1++){
			for(iterator_2 = (iterator_1 + 1) ; iterator_2 < lawLines.length ; iterator_2++){
				if(lawLines[iterator_2].Id == lawLines[iterator_1].Id){
					lawLines[iterator_2].showError = 'true';
					errorCounter++;
				}
			}
		}
		cmp.set("v.lawLines", lawLines);

		if (lawLines.length < 1 || lawLines.length == 1 && lawLines[0].Name == '') {
			errorCounter++;
			cmp.set("v.showLawReqError", "true");
		} else {
			cmp.set("v.showLawReqError", "false");
		}

		var popP621 = $A.get("e.c:updateVarsFromP21");
		popP621.setParams({
			"fixValue": cmp.get("v.searchVar"),
			"authValues": lawLinesNames
		});
		popP621.fire();

		return errorCounter == 0;
	}
})