({
	showSpinner: function (cmp, evt, helper) {
		console.log("~~~ Page21 helper function: showSpinner ~~~");
		cmp.set("v.showSpinner", "true");
	},

	hideSpinner: function (cmp, evt, helper) {
		console.log("~~~ Page21 helper function: hideSpinner ~~~");
		cmp.set("v.showSpinner", "false");
	},

	getParentLawsUnfilteredByMotherWithOrWithoutLawItems: function (cmp, evt, helper, includeLawItems) {
		console.log('>>>>>HELPER: getParentLawsUnfilteredByMotherWithOrWithoutLawItems');
		var parentLaws = [];
		var action = cmp.get("c.getParentLaws");
		action.setParams({ "searchVar": "", "inc": includeLawItems });
		action.setCallback(this, function (response) {
			var status = response.getState();
			var result = response.getReturnValue();
			var parsedResult = JSON.parse(response.getReturnValue());
			for (const key in parsedResult) {
				var line = new Object();
				line.Id = parsedResult[key].Id;
				line.Name = parsedResult[key].Name;
				line.isLawItem = parsedResult[key].IsLi;
				parentLaws.unshift(line);
			}
			cmp.set("v.parentLaws", parentLaws);
			cmp.set("v.parentLawsSearchRes", parentLaws);
			cmp.set("v.showSpinner", "false");
			console.log('>>>>> checked Callback parentLaws:' + parentLaws);
		});
		$A.enqueueAction(action);
	},

	getParentLawsWithLawItemsRadio2: function (cmp, evt, helper){
		var action = cmp.get("c.getParentLaws");
		action.setParams({ "searchVar": "", "inc": "true", "lawId" : cmp.get("v.MotherLawId") });
		action.setCallback(this, function (response) {
			var result = JSON.parse(response.getReturnValue());
			console.log('result:', result);
			cmp.set("v.parentLaws", result);
			cmp.set("v.parentLawsSearchRes", result);
			cmp.set("v.showSpinner", "false");
		});
		$A.enqueueAction(action);
	}	
})