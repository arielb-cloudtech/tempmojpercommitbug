({
	doInit : function(cmp, event, helper) {
		cmp.set('v.pageClasses', location.pathname.replace('/s','').split('/').filter(f=>f).join(" "));

	},
	sendAjaxCall : function(cmp, event, helper) {
		var action = event.getParams().actionObject;
		var callback = event.getParams().callback;
		
		action.setCallback(this, function (response) {
			var state = response.getState();
		
       		if( state === 'SUCCESS'){
				response = response.getReturnValue();
			}
			else {
				response = {status: "error", message: response.getError()[0].message};
			}	
			
			if ( response.message ) {
				var messages = cmp.get("v.Messages");
				messages.push({text: response.message, status: response.status});
				cmp.set("v.Messages", messages);
			}

			if( response.status == 'success') {
				callback(response);
			}
			
		});
		
		$A.enqueueAction(action);   
	}
})