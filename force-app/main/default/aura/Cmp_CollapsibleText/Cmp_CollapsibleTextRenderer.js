({
    rerender : function(cmp, helper){
        this.superRerender();
        var stateId  = cmp.get("v.StateId");
        if ( helper.openItems.indexOf(stateId ) > -1 ) {
            console.log(cmp.set("v.isExpended", true));
        }
    }
})