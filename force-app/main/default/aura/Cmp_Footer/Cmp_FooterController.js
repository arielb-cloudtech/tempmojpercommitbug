({
	doInit : function(cmp, event, helper) {
	},

	handleComponentEvent : function (cmp, event) {
        var currentUser = event.getParam('currentUser');
        cmp.set('v.CurrentUser', currentUser);
	},
	
	goToMail : function(cmp, event, helper) {
		var currentUser = cmp.get("v.CurrentUser");
		if ( currentUser.Name ) {
			window.location.href="/s/myaccount#subscriptionSettings";
		} else {
			window.location.href="/s/moj-login";
		}
	}
})