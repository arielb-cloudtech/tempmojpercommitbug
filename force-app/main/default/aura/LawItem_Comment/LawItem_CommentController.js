({
	showText: function (component, event, helper) {
		console.log('showText: ...');
		var comment = JSON.parse(JSON.stringify(component.get("v.comment")));
		var o = 'הרחב';
		var c = 'צמצם';

		var element = document.getElementById('textPart_1' + comment.mId);
		var elFile = document.getElementById('fileDiv' + comment.mId);
		var elDivText = document.getElementById('comText' + comment.mId);
		if (element.classList.contains('slds-truncate')) {
			element.classList.remove('slds-truncate');
			element.classList.add('preStyle');
			component.set('v.isShowSection', true);
			// elFile.classList.add('showFile');
			// elFile.classList.remove('hideFile');
			// elDivText.classList.remove('slds-size_12-of-12');
			// elDivText.classList.add('slds-size_9-of-12'); 
			component.set('v.btnText', c);
		}
		else {
			element.classList.add('slds-truncate');
			element.classList.remove('preStyle');
			// elFile.classList.remove('showFile');
			// elFile.classList.add('hideFile');
			// elDivText.classList.remove('slds-size_9-of-12');
			// elDivText.classList.add('slds-size_12-of-12');
			component.set('v.btnText', o);
			component.set('v.isShowSection', false);
		}
	},

	openModel: function (component) {
		// for Display Model,set the "isOpen" attribute to "true"		
		component.set("v.isSaving", false);
		component.set("v.isOpen", true);
		console.log(component.get("v.isOpen"));
	},
	doInit: function (component) {
		try
		{
			var comment = JSON.parse(JSON.stringify(component.get("v.comment")));
			var elFile = document.getElementById('fileDiv' + comment.mId);
			elFile.classList.add('showFile');
		} catch (err) 
		{
			console.log("err:");	
			console.log(err.message);	
		}
	},

	closeModel: function (component, event, helper) {
		// for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
		var eve = $A.get("e.c:LawItem_CancelUpdate");
		eve.fire();
	},

	saveAndClose: function (component, event, helper) {
		//console.log('after param ' + component.get("v.recordId"));
		component.set("v.isSaving", true);
		console.log(component.get("v.isSaving"));
		var eve = $A.get("e.c:LawItem_SaveEvent");
		eve.setParams({
			"recordId": component.get("v.recordId")
		});
		eve.fire();
		//component.set("v.isOpen", false);		
	},
	
	addRemoveIndex: function(component, event, helper) {
		var myMap = JSON.parse(JSON.stringify(component.get("v.textMap")));
		
		console.log("START", myMap);

		var myIndex = component.get("v.index");
		console.log("myIndex: ", myIndex);

		if(myMap == null || myMap == undefined){
			console.log("hyy");
			myMap = new Map();
			var commentText = component.get('v.comment').mBody;
			myMap[myIndex] =  commentText;
		}else{
			console.log("byy");
			if(myMap[myIndex]){
				console.log("true");
				delete myMap[myIndex];
			}else {
				console.log("false");
				var commentText = component.get('v.comment').mBody;
				myMap[myIndex] = commentText;
			}
		}
		component.set('v.textMap', myMap);
		console.log("END", JSON.parse(JSON.stringify(component.get("v.textMap"))));

		var commentMap = JSON.parse(JSON.stringify(component.get("v.commentIdMap")));
		var commentId = component.get('v.comment.mId');
		console.log("CommentId: ", commentId);

		if(commentMap == null || commentMap == undefined){
			console.log("new Map");
			commentMap = new Map();
			commentMap[myIndex] =  commentId;
		}else{
			console.log("Map ");
			if(commentMap[myIndex]){
				console.log("remove");
				delete commentMap[myIndex];
			}else {
				console.log("add");
				var commentText = component.get('v.comment.mId');
				commentMap[myIndex] = commentId;
			}
		}
		component.set('v.commentIdMap', commentMap);
		console.log("CommentIdMap: ", JSON.parse(JSON.stringify(component.get("v.commentIdMap"))));
		console.log('size::');
		console.log(helper.isEmpty(commentMap));
		component.set("v.cmtLstisEmpty", helper.isEmpty(commentMap));
	},
	
	onClearSelection: function (component) {
		console.log("onClearSelection: uncheck");
		component.set("v.isChecked", false);
	},

	openCancelModel:function(component){
		component.set("v.isDeleteOpen", true);
	},

	closeCancelModel:function(component){
		component.set("v.isDeleteOpen", false);
	},

	removeComment: function (component) {
		console.log("deleteComment");
		var commentId = component.get("v.comment.mId");
		console.log(commentId);
		var action = component.get("c.DeleteComment");
		action.setParams({ "commentId": commentId });
		action.setCallback(this, function (response) {
			component.set("v.myComment", "");
			var state = response.getState();
			console.log("state = " + state);
			if (component.isValid() && state === "SUCCESS") {				
				var comments = component.get('v.comments');
				for (var i = 0; i < comments["mComments"].length; i++) {
					if (comments["mComments"][i].mId == commentId) {						
						comments["mComments"].splice(i, 1);
					}
				}
				component.set("v.isDeleteOpen", false);
				component.set("v.comments", comments);
				var fireEvent = component.getEvent("newCommentAdded");
				fireEvent.setParams({ "comments": comments });
				fireEvent.fire();
				console.log("DONE inner");
			}
			else {
				alert("Custom ERROR!!!");
			}
		});
		$A.enqueueAction(action);

	}


})