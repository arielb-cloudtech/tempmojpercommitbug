({
	closeFiltersOffice : function(cmp, evt, helper) 
	{
		cmp.set("v.isFiltersOpen", "true");
		cmp.set("v.isFiltersOpenOffice", "false");

		var selectedOffices = [];
		var abcMap = cmp.get("v.abcMap");
		for(const key in abcMap)
		{
			for(const key2 in abcMap[key].offices)
			{
				abcMap[key].offices[key2].isSelected = false;
			}
		}
		cmp.set("v.abcMap", abcMap);
		cmp.set("v.selectedOffices", selectedOffices);
	},
	confirmChoice : function(cmp, evt, helper) 
	{
		cmp.set("v.isFiltersOpen", "true");
		cmp.set("v.isFiltersOpenOffice", "false");

		// var selectedOffices = [];
		// var abcMap = cmp.get("v.abcMap");
		// for(const key in abcMap)
		// {
		// 	for(const key2 in abcMap[key].offices)
		// 	{
		// 		abcMap[key].offices[key2].isSelected = false;
		// 	}
		// }
		// cmp.set("v.abcMap", abcMap);
		// cmp.set("v.selectedOffices", selectedOffices);
	},
	clearFiltersOffice : function(cmp, evt, helper) 
	{
		var selectedOffices = [];
		var abcMap = cmp.get("v.abcMap");
		for(const key in abcMap)
		{
			for(const key2 in abcMap[key].offices)
			{
				abcMap[key].offices[key2].isSelected = false;
			}
		}
		cmp.set("v.abcMap", abcMap);
		cmp.set("v.selectedOffices", selectedOffices);
	},
	chooseOffice : function(cmp, evt, helper) 
	{
		try
		{
			var abcMap = cmp.get("v.abcMap");
			var selectedIds = [];
			var selectedOffices = cmp.get("v.selectedOffices");
			
			for(const key in selectedOffices)
			{
				selectedIds.push(selectedOffices[key].Id);
			}

			for(const key in abcMap)
			{
				for(const key2 in abcMap[key].offices)
				{
					//console.log(abcMap[key].offices[key2]);
					if(abcMap[key].offices[key2].isSelected == true && !selectedIds.includes(abcMap[key].offices[key2].office.Id))
					{
						var selectedOffice = new Object();
						selectedOffice.Id = abcMap[key].offices[key2].office.Id;
						selectedOffice.Name = abcMap[key].offices[key2].office.Name;
						selectedOffices.push(selectedOffice);
						//console.log(JSON.stringify(selectedOffice));
					}else if(abcMap[key].offices[key2].isSelected == false && selectedIds.includes(abcMap[key].offices[key2].office.Id))
					{
						// var selectedOffice = new Object();
						// selectedOffice.Id = abcMap[key].offices[key2].office.Id;
						// selectedOffice.Name = abcMap[key].offices[key2].office.Name;
						for(var i = 0;i < selectedOffices.length;i++)
						{
							if(selectedOffices[i].Name == abcMap[key].offices[key2].office.Name)
							{
								selectedOffices.splice(i, 1);
							}
						}
					}
				}
			}
			//console.log(JSON.stringify(selectedOffices));
			cmp.set("v.selectedOffices", selectedOffices);
			try 
			{
				console.log("in office change func");
				var pills = cmp.get("v.fltPills");
				var exists = false;
				if(selectedOffices.length > 0)
				{
					for(const key in pills)
					{
						if(pills[key].fltType == 'office')
						{
							pills[key].label =  '(משרדים ' +  '(' + selectedOffices.length; 
							exists = true;
						}
					}
					if(!exists)
					{
						var pill = new Object();
						pill.type = 'basic';
						pill.fltType = 'office';
						pill.label = '(משרדים ' +  '(' + selectedOffices.length; 
						pills.push(pill);
					}
				}else
				{
					for(const key in pills)
					{
						if(pills[key].fltType == 'office')
						{
							pills.splice(key,1);
						}
					}
				}

				cmp.set("v.fltPills", pills);
				console.log("in office change func2");	
			} catch (err) 
			{
				console.log("Err:");
				console.log(err.message);
			}
		}catch(err)
		{
			console.log(err.message);
		}
	},
	doInit : function(cmp, evt, helper) 
	{
		// var abcMap = new Object();
		// var aLst = [];
		// var aLst2 = [];
		// aLst.push("משרד 1");
		// aLst.push("משרד 2");
		// aLst2.push(aLst);
		// abcMap.a = aLst2;
		// console.log(aLst);
		// cmp.set("v.abcMap", aLst);

		var action = cmp.get("c.getOffices");
		action.setCallback(this, function(response) {
			var STATE = response.getState();
			
			if(STATE === "SUCCESS") {
				console.log(response.getReturnValue());
				var returnValue = JSON.parse(response.getReturnValue()); 
				console.log(returnValue);
				for(const key in returnValue)
				{
					for(const key2 in returnValue[key].offices)
					{
						returnValue[key].offices[key2].isSelected = false;
					}
				}
				cmp.set("v.abcMap", returnValue);
			}
			
			else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + 
									errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
	
		$A.enqueueAction(action);
	}
})