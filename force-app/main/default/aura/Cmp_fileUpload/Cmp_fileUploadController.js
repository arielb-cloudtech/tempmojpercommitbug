({
    doInit: function (cmp, event, helper) {

        if( !cmp.get("v.files") ){
            cmp.set("v.files", []);
        }

    },

    onUploadedFilesFinish: function (cmp, event, helper) {
        helper.readFiles(cmp, event);
    },

    removeFile: function (cmp, event, helper) {
        var fileIndex = event.target.getAttribute("data-indexVar");
        var files = cmp.get("v.files");
        
        files[fileIndex].size = 0;
        files[fileIndex].size = 100;

        cmp.set("v.files", files);

        var action = cmp.get("c.DeleteAttachment");
       
        action.setParams({ deleteAttachment: files[fileIndex].attachmentId });

        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if ( state === "SUCCESS") {
                files[fileIndex].isDeleted = true;
            }
            cmp.set("v.files", files);
        });
        $A.enqueueAction(action);
    }
})
