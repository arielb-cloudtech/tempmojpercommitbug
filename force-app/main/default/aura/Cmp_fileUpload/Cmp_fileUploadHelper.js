({
    CHUNK_SIZE: 7500, 
    /** 
        Read files from component 
    **/
    readFiles: function(cmp, event){
        var uploadedFiles = event.getParam("files");
        var reader = new FileReader();  
        var helper = this;
        function readFile(index) {
            if( index >= uploadedFiles.length ) {
                helper.uploadFiles(cmp);
                return;
            } 
            var file = uploadedFiles[index];
            reader.onload = function(e) {  
                // get file content  
                var fileContents = e.target.result;
                var base64 = 'base64,';
                var dataStart = fileContents.indexOf(base64) + base64.length;
                fileContents = fileContents.substring(dataStart);
                var files = cmp.get("v.files");
                files.push({
                    name: file.name, 
                    size: fileContents.length, 
                    type: file.type, 
                    uploadPercent: 0,
                    content: fileContents
                });
                cmp.set("v.files", files);
                
                readFile(index+1);
            }
            
            reader.readAsDataURL(file);
        }
        readFile(0);
    },
    /**
     * Upload chunks of files 
     */
    uploadFiles: function(cmp){
        // lock the uploading functionality
        if(cmp.get("v.IsUploading")) {
            return;
        }

        cmp.set("v.IsUploading", true);
        this.fileUploadByChunk(cmp, 0, 0);
    },

    /** 
     * Recrusive function to upload chunk.
     * when a chunk is uploaded it calls this function again
     ***/
    fileUploadByChunk: function(cmp, fileIndex, startPosition) {
        var files = cmp.get("v.files");
        var chunkEnds = Math.min(files[fileIndex].size, startPosition + this.CHUNK_SIZE);
        var SObjectId = cmp.get("v.SObjectId");
        var SObjectType = cmp.get("v.parentType");
        var helper = this;

        if (files[fileIndex].uploadPercent == 100 || files[fileIndex].size == 0 ) {
            helper.fileUploadByChunk(cmp, fileIndex + 1, 0);
            return;
        } 

        var action = cmp.get("c.SaveAttachment");
       
        action.setParams({
            AttachmentId: files[fileIndex].attachmentId,
            fileName: files[fileIndex].name,
            ParentId: SObjectId,
            parentType: SObjectType,
            IsLastBatch: (files[fileIndex].size <= startPosition + this.CHUNK_SIZE),
            data: encodeURIComponent(files[fileIndex].content.substring(startPosition, chunkEnds)) 
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if ( state === "SUCCESS") {
                var responseObj = response.getReturnValue();

                if ( responseObj.status == 'success') {
                    if( ! SObjectId ) {
                        SObjectId =  responseObj.ParentId;
                        cmp.set("v.SObjectId", SObjectId);
                    }
                    files[fileIndex].uploadPercent = chunkEnds/files[fileIndex].size*100;
                    files[fileIndex].attachmentId = responseObj.FileId;
                    cmp.set("v.files", files);
                    if (files[fileIndex].uploadPercent < 100 ) {
                        helper.fileUploadByChunk(cmp, fileIndex , chunkEnds);
                        return;
                    } 
                }
            }
            else {
                console.log(response.getError());
            }
            
            fileIndex = fileIndex + 1;
            if( fileIndex  < files.length ) {
                helper.fileUploadByChunk(cmp, fileIndex, 0);
                return;
            }
            cmp.set("v.IsUploading", false);
        });
        $A.enqueueAction(action);
    }
})
