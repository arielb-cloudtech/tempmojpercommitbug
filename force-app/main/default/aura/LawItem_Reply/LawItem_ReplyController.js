({  doInit : function(component, event, helper) {
		
		console.log (JSON.parse(JSON.stringify(component.get('v.reply'))));		
		var action = component.get("c.getUserId");
		action.setCallback(this, function (response) {
			var state = response.getState();
			console.log("state = " + state);
			if (component.isValid() && state === "SUCCESS") {				
				component.set("v.userId", response.getReturnValue());
			}
			else {
				alert("Custom ERROR!!!");
			}
		});
		$A.enqueueAction(action);
	},
	edit : function(component, event, helper) {
		component.set("v.edit", true);		
		if (component.get('v.reply').bDisplay) {			
			component.set('v.pubChoice', 'true');
		}
		else {			
			component.set('v.pubChoice', 'false');
		}

		//////////
		component.set("v.replyId", component.get('v.reply').mId);
		var uploadedFiles = component.get('v.reply').mFiles; 
			console.log("uploadedFiles", uploadedFiles.length);
			var fList = component.get("v.filesList");
			console.log("~~~for");
			console.log("~~~component.get(v.filesList)", component.get("v.filesList"));
			for (var i = 0; i < uploadedFiles.length; i++) {
				console.log("~~~fList", fList);
				if(fList == null || fList == undefined){
					fList = [];
				}
				fList.push(uploadedFiles[i].documentId);
				console.log(uploadedFiles[i] + ' ' + uploadedFiles[i].documentId );
				//filesList
				console.log("~~~component");
				$A.createComponent(
					"c:LawItem_FileLoaded",
					{
						"aura:id": "section1",
						"file": uploadedFiles[i],
						"deleteFile": true,
						"fileId": uploadedFiles[i].documentId
					},
					function (newComponent, status, errorMessage) {
						console.log("~~~func");
						if (status === "SUCCESS") {
							console.log("~~~SUCCESS");
							var body = component.get("v.body");
							console.log("newComponent", newComponent);
							console.log("body", body);

							body.push(newComponent);
							component.set("v.body", body);
						}
						else if (status === "INCOMPLETE") {
							console.log("No response from server or client is offline.");
						}
						else if (status === "ERROR") {
							console.log("Error: " + errorMessage);
						}
					}
				);
			}



	},
	cancel : function(component, event, helper) {
		component.set("v.edit", false);
	},

	removeFile : function(component, event, helper){
		var del = component.get("v.deletedFiles");
		var iD = event.getParam("docId");
		console.log('iD = ' + iD);
		del.push(iD);		
		component.set("v.deletedFiles", del);
		console.log('fileUploadedController = ' + component.get("v.deletedFiles"));
	},

	save : function(component, event, helper) {
		// var action = component.get("c.saveReplyEdit");
		var bDisp;

		if (component.get("v.pubChoice") == 'true') {
			bDisp = true;
		}
		else {
			bDisp = false;
		}

		var action = component.get("c.SaveNewReply");
		var comment = JSON.parse(JSON.stringify(component.get("v.comment")));

		console.log("body - " + component.get("v.myReply") + " replyId - " + component.get("v.replyId") + " deletedFiles  - " + component.get("v.deletedFiles"));
		action.setParams({
			"body": component.get("v.reply").mBody,
			"replyId": component.get("v.reply").mId,
			"filesToDelete": component.get("v.deletedFiles"),
			"disp": bDisp
		});

		action.setCallback(this, function (response) {

			var state = response.getState();
			if (component.isValid() && state === "SUCCESS") {
				var returnValue = response.getReturnValue();				
				component.set("v.comment", comment);		
				location.reload();
			}
			else {
				alert("Custom ERROR!!!");
			}
		});
		$A.enqueueAction(action);
	},
	showContent : function(component, event, helper) {
		
		console.log('showContent: ...');
		var reply = JSON.parse(JSON.stringify(component.get("v.reply")));
		var element = document.querySelector('#' + reply.mId + '.txt');
		if (element)
		{
			if (element.classList.contains('slds-truncate'))
				element.classList.remove('slds-truncate');
			else
				element.classList.add('slds-truncate');
		}


		var element = document.querySelector('#' + reply.mId + '.files');
		if (element)
		{
			if (element.classList.contains('groupHidden'))
			{
				element.classList.remove('groupHidden');
				element.classList.add('groupVisible');
			}
			else
			{
				element.classList.remove('groupVisible');
				element.classList.add('groupHidden');
			}
		}

		var element = document.querySelector('#' + reply.mId + '.slds-button');
		if (element)
		{
			if (element.classList.contains('open'))
			{
				element.classList.remove('open');
				element.classList.add('close');
				element.innerHTML = component.get("v.btnClose");
			}
			else
			{
				element.classList.remove('close');
				element.classList.add('open');
				element.innerHTML = component.get("v.btnOpen");
			}
		}

		
	}
})