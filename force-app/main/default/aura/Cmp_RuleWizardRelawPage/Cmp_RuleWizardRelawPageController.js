({
	removeLaw : function(cmp, evt, helper) 
	{
		console.log(evt.target.classList[0]);
		var indxVar = evt.target.classList[0];

		var selLaws = cmp.get("v.selectedLaws");
		var selLawsIds = cmp.get("v.selectedLawsIds");

		var laws = cmp.get("v.lawLst");
		// console.log(laws[0]);
		// console.log(selLaws[indxVar]);
		for(const key in laws)
		{
			if((laws[key].Id == selLaws[indxVar].Id))
			{
				laws[key].isSelected = false;
			}
		}

		cmp.set("v.lawLst", laws);
		selLaws.splice(indxVar, 1);
		selLawsIds.splice(indxVar, 1);

		cmp.set("v.selectedLaws", selLaws);
		cmp.set("v.selectedLawsIds", selLawsIds);
	},
	closeFilters : function(cmp, evt, helper) 
	{
		cmp.set("v.showWizard", "false");	
	},

	clearChoice : function(cmp, evt, helper) 
		{
			cmp.set("v.selectedSub", "");
			cmp.set("v.selectedSubName", "");
			cmp.set("v.selectRecordName", "");
			cmp.set("v.selectedLaws", []);
			cmp.set("v.selectedLawsIds", []);
			var laws = cmp.get("v.lawLst");
			for(const key in laws)
			{
				laws[key].isSelected = false;
			}
			// cmp.set("v.lawLst", laws);
			cmp.set("v.lawLst", []);

		},

		searchField : function(component, event, helper) {
			try
			{
				component.set("v.firstSearch", "true");
				var currentText = event.getSource().get("v.value");
				console.log("currentText", currentText);
				console.log("sadf", component.get('v.selectRecordId'));
				var resultBox = component.find('resultBox');
				if(currentText.length > 0) {
					$A.util.addClass(resultBox, 'slds-is-open');
				}else {
					$A.util.removeClass(resultBox, 'slds-is-open');
				}
				// if(currentText)
				if(true)
				{
					var action = component.get("c.getLaws");
						action.setParams({ subSearch : component.get('v.selectRecordName'),
											subName : component.get("v.selectedSubName") == $A.get("$Label.c.Select_a_law_topic") ?  '' : component.get("v.selectedSubName")});
						action.setCallback(this, function(response) {
						var STATE = response.getState();
						if(STATE === "SUCCESS") {
							//console.log(response.getReturnValue());
							var returnValue = JSON.parse(response.getReturnValue()); 
							component.set("v.lawLst", returnValue);
						}
						
						else if (state === "ERROR") {
							var errors = response.getError();
							if (errors) {
								if (errors[0] && errors[0].message) {
									console.log("Error message: " + 
												errors[0].message);
								}
							} else {
								console.log("Unknown error");
							}
						}
					});
					
					$A.enqueueAction(action);
				}else
				{
					var lst = [];
					component.set("v.lawLst", lst);
				}
			}catch(err)
			{
				console.log(err.message);
			}
		},
		selectSub : function(cmp, evt, helper) 
		{
			// var isOp = cmp.get("v.isSubFilter");
			console.log(evt.target.nextSibling.textContent);
			cmp.set("v.selectedSubName", evt.target.textContent);
			cmp.set("v.selectedSub", evt.target.nextSibling.textContent);
			cmp.set("v.isSubFilter", false);
		},
		checkLaws : function(cmp, evt, helper) 
		{
			console.log("in checklaws");
			try
			{
				var laws = cmp.get("v.lawLst");
				var selLaws = cmp.get("v.selectedLaws");
				var selectedLawsIds = cmp.get("v.selectedLawsIds");
		
				for(const key in laws)
				{
					if(laws[key].isSelected && !selectedLawsIds.includes(laws[key].Id))
					{
						// console.log(laws[key]);
						selectedLawsIds.push(laws[key].Id);
						selLaws.push(laws[key]);
					}else if(!laws[key].isSelected && selectedLawsIds.includes(laws[key].Id))
					{
						for(var i = 0;i < selLaws.length;i++)
						{
							if(selLaws[i].Id == laws[key].Id)
							{
								selLaws.splice(i, 1);
							}
						}
						for(var i = 0;i < selectedLawsIds.length;i++)
						{
							if(selectedLawsIds[i] == laws[key].Id)
							{
								selectedLawsIds.splice(i, 1);
							}
						}
					}
				}
		
				cmp.set("v.selectedLaws", selLaws);
				cmp.set("v.selectedLawsIds", selectedLawsIds);
			}catch(err)
			{
				console.log(err);
			}
		},
		openSubFilter : function(cmp, evt, helper) 
		{
			try
			{
				setTimeout(() => {
					console.log(cmp.find("dropdownMenu").getElement());
					cmp.find("dropdownMenu").focus();
				}, 10);
				var isOp = cmp.get("v.isSubFilter");
				cmp.set("v.isSubFilter", !isOp);
			}catch(err)
			{
				console.log(err.message);
			}
		},
		doInit : function(cmp, evt, helper) 
		{
			var action = cmp.get("c.getSubs");
				action.setParams({ subSearch : "" });
				action.setCallback(this, function(response) {
				var STATE = response.getState();
				if(STATE === "SUCCESS") {
					//console.log(response.getReturnValue());
					var returnValue = JSON.parse(response.getReturnValue()); 
					var emptyOpt = new Object;
					emptyOpt.Name = "בחירת נושא חוק";
					emptyOpt.Id = "";
					returnValue.unshift(emptyOpt);
					cmp.set("v.subLst", returnValue);
					// var subLst = cmp.get();
				}
				
				else if (state === "ERROR") {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.log("Error message: " + 
										errors[0].message);
						}
					} else {
						console.log("Unknown error");
					}
				}
			});
			
			$A.enqueueAction(action);

			try
			{
				var action2 = cmp.get("c.getLaws");
				action2.setParams({ subSearch : ' ',
									subName : ''});
				action2.setCallback(this, function(response) {
				var STATE = response.getState();
				console.log("init state");
				console.log(STATE);
				if(STATE === "SUCCESS") {
					console.log("laws len");
					console.log(response.getReturnValue().length);
					var returnValue = JSON.parse(response.getReturnValue()); 
					cmp.set("v.lawLst", returnValue);
				}
				
				else if (state === "ERROR") {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.log("Error message: " + 
										errors[0].message);
						}
					} else {
						console.log("Unknown error");
					}
				}
			});
			
			$A.enqueueAction(action2);
			} catch (err) 
			{
				console.log("error?: ");	
				console.log(err.message);	
			}
			
		},

		applyChoice : function(cmp, evt, helper) 
		{
			try
			{
				console.log("im here");
				cmp.set("v.showSpinner", "true");							
				// evt.stopPropagation();
				// var LawItems = cmp.get("v.LawItems");
				// var liIdsLst = [];
				// for(const key in LawItems)
				// {
				// 	liIdsLst.push(LawItems[key].Itemcard.Id);
				// }
				var itmTypeChoices = cmp.get("v.itmTypeChoicesObj");
				if(evt.getParam("itmTypeChoices"))
				{
					itmTypeChoices = evt.getParam("itmTypeChoices");
				}
				var rtCounter = 0;
				for(const key in itmTypeChoices)
				{
					if(itmTypeChoices[key].isSelected == true)
					{
						rtCounter++;
					}
				}
				itmTypeChoices.numOfChoices = rtCounter;
				var startDate = cmp.get("v.startDate");
				var endDate = cmp.get("v.endDate");
				var chosenSubs = cmp.get("v.chosenSubs");
				var selectedLaws = cmp.get("v.selectedLaws");
				var selectedSecLaws = cmp.get("v.selectedSecLaws");
				var selectedOffices = cmp.get("v.selectedOffices");
				var openForComments = cmp.get("v.value");
				var ria = cmp.get("v.RIAFilter");
				var ordb = cmp.get("v.orderBy");
				if(ordb)
				{
					ordb = "Desc";
				}
				if(chosenSubs.length == 0)
				{
					chosenSubs = evt.getParam("classes");
				}
				console.log("selectedLaws: ");
				console.log(selectedLaws);
		
				var filterObj = new Object();
				filterObj.rtChoices = itmTypeChoices
				filterObj.distDateStart = startDate;
				filterObj.distDateEnd = endDate;
				filterObj.chosenClassifications = chosenSubs;
				filterObj.relatedLaws = selectedLaws;
				filterObj.relatedSecLaws = selectedSecLaws;
				filterObj.selectedOffices = selectedOffices;
				filterObj.openForComments = openForComments;
				filterObj.RIAFilter = ria;
				filterObj.orderBy = ordb;
				console.log(JSON.stringify(filterObj));
				
				console.log("searchval: ");
				// var searchVal = cmp.get("v.searchVal");
				var indx = window.location.search.indexOf("search4");
				var searchVal = "%" + decodeURIComponent(window.location.search.substring(indx).substring(8)).replace(/\+/g, " ") + "%";
				var indxType = window.location.search.indexOf("type");
				var type = decodeURIComponent(window.location.search.substring(indxType).substring(5,6));
				filterObj.searchVar = searchVal;
				console.log(searchVal);
				var action = cmp.get("c.getLawItems");
				action.setParams({ filters : JSON.stringify(filterObj),
									searchType : type});
				action.setCallback(this, function(response) {
				var STATE = response.getState();
				
				if(STATE === "SUCCESS") {
				
					console.log("i have returned");
					console.log(response.getReturnValue());
					if(response.getReturnValue() != "empty")
					{
						var returnValue = JSON.parse(response.getReturnValue()); 
						// cmp.set("v.LawItems", returnValue);
						console.log(returnValue.length);
						var processRes = $A.get("e.c:ProcessRes");
						processRes.setParams({lawItems : returnValue});
						console.log(processRes);
						processRes.fire();
						//cmp.set("v.showSpinner", "false");							
					}else
					{
						var lis = [];
						cmp.set("v.lawItems", lis);
						var processRes = $A.get("e.c:ProcessRes");
						processRes.setParams({lawItems : lis});
						console.log(processRes);
						processRes.fire();
					}

				}
				
				else if (state === "ERROR") {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.log("Error message: " + 
										errors[0].message);
						}
					} else {
						console.log("Unknown error");
					}
				}
				});
				cmp.set("v.isFiltersOpen", "false");
				$A.enqueueAction(action);
				// evt.pause();
			}catch(err)
			{
				console.log("error: ");
				console.log(err.message);
			}
			var filter = $A.get("e.c:runFilterDesktop");	
			//var filter = cmp.getEvent("filterRes");	
			filter.setParams({classes : cmp.get("v.chosenSubs")});
			console.log("filter");
			console.log(filter);
			//filter.fire();
			console.log("selectedLaws2");
			console.log(cmp.get("v.selectedLaws"));
			cmp.set("v.isLawFilterOpen", "false");

	},
})