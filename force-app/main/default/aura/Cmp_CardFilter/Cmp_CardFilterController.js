({
	doInit: function (component, event, helper) {
		var action = component.get("c.getfilterOptions");
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {	
				var responseObj = response.getReturnValue();
				console.log(responseObj);
				var opt = responseObj.filters;
				var profUrl = $A.get('$Resource.communityAssets');
				component.set("v.num", opt.length);
				for (var i = 0; i < opt.length; i++) {
					opt[i].url = profUrl + '/' + opt[i].image + '.png';
				}
				component.set('v.filterOptions', opt);
			}
			else if (state === "ERROR") {
				console.log(response.getError());
			}
		});
		$A.enqueueAction(action);
	}
})