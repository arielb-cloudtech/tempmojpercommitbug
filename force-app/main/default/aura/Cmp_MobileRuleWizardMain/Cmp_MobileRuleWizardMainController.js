({
	closeFilters : function(cmp, evt, helper) 
	{
		cmp.set("v.inMain", "true");
		var sObjectEvent = $A.get("e.c:updateDisplayIt");
		sObjectEvent.fire();
	},

	goToSub : function(cmp, evt, helper) 
	{
		var choice = "1";
		cmp.set("v.choice", choice);
		cmp.set("v.chosenProcess", "1");
		var sObjectEvent = $A.get("e.c:updateDisplayIt");
		sObjectEvent.fire();

		if(choice)
		{
			cmp.set("v.inMainWizard", "false");
		}
	},
	goToLaw : function(cmp, evt, helper) 
	{
		var choice = "2";
		cmp.set("v.choice", choice);	
		cmp.set("v.chosenProcess", "2");
		var sObjectEvent = $A.get("e.c:updateDisplayIt");
		sObjectEvent.fire();
	
		if(choice)
		{
			cmp.set("v.inMainWizard", "false");
		}
	},
	goToPub : function(cmp, evt, helper) 
	{
		var choice = "3";
		cmp.set("v.chosenProcess", "3");
		var sObjectEvent = $A.get("e.c:updateDisplayIt");
		sObjectEvent.fire();

		cmp.set("v.choice", choice);
		if(choice)
		{
			cmp.set("v.inMainWizard", "false");
		}
	},
	goToType : function(cmp, evt, helper) 
	{
		var choice = "5";
		cmp.set("v.chosenProcess", "5");
		var sObjectEvent = $A.get("e.c:updateDisplayIt");
		sObjectEvent.fire();

		cmp.set("v.choice", choice);
		if(choice)
		{
			cmp.set("v.inMainWizard", "false");
		}
	},

	check : function(cmp, evt, helper) 
	{
		var choice = evt.getSource().get('v.value');
		cmp.set("v.choice", choice);
	}
})