({
	getPages:  function (currentRecordType) {
		var paths = [
			{ label:'הפצת תזכיר חוק', recoredTypeName: 'Memorandum of Law', pages:['Page1','Page2','Page3','Page4','Page5','Page6']},
			{ label:'הפצת טיוטת חקיקת משנה', recoredTypeName: 'Secondary Legislation Draft',  pages:['Page21','Page2','Page3','Page4','Page5','Page6']},
			{ label:'הפצת טיוטת מבחן תמיכה', recoredTypeName: 'Support Test Draft',  pages:['Page2','Page3','Page5','Page6']},
			{ label:'הפצת טיוטת נוהל או הנחיה', recoredTypeName: 'Ministry Directive or Procedure Draft',  pages:['Page41','Page2','Page3','Page4','Page5','Page6']},
		];
		if ( !currentRecordType ) {
			return paths;
		}
		return paths.find( path=> path.recoredTypeName == currentRecordType );
	},

	gotoPage: function (cmp , pageIndex) {
		var currentRecordType = cmp.get("v.currentRecordType");
		var processData = this.getPages(currentRecordType);


		var pages = cmp.get("v.pages");

		if ( pages.length == 0 ) {
			pages = processData.pages.map(pageName=> ({
				"hasError": false,
				"label": "שלב - " + ( 1 + processData.pages.indexOf(pageName)),
				"index": processData.pages.indexOf(pageName),
				"name": pageName
			}));
		}

		var callerPage = cmp.get("v.currentPage");
		var callerPageObject = pages.find( page=> page.name == callerPage );
		if( callerPageObject ) {
			var pageComponent = cmp.find( callerPage );
			if( pageComponent ) {
				callerPageObject.hasError = !pageComponent.validate();
			}
		}

		var newPage = processData.pages[pageIndex];
		var newPageObject = pages.find( page=> page.name == newPage );
		var pageErrorHasErrors = newPageObject.hasError;

		cmp.set("v.pages",  pages);
		cmp.set("v.currentPage",  newPage );
		cmp.set("v.pageIndex", pageIndex );

		if( pageErrorHasErrors ) {
			cmp.find( newPage ).validate();
		}

		switch ( cmp.get("v.currentPage") ) {
			case 'Page5':
				cmp.set("v.modalTitle", "צירוף קבצים נוספים");
				break;
			case 'Page6':
				cmp.set("v.modalTitle", "סיכום לפני הפצה");
				break;
			default:
				cmp.set("v.modalTitle", processData.label);
		}
	},

	save: function (cmp, isDraft) {

		var lawItem = cmp.get("v.newLawItem");
		var lawLines = cmp.get("v.lawLines");
		var childLawLines = cmp.get("v.childLawLines");
		var removedLawLines = cmp.get("v.removedLawLines");
		var classLst = cmp.get("v.classLst");
		var classLstToRemove = cmp.get("v.classLstToRemove");
		lawItem.Status__c = isDraft?'Draft':'Distributed';


		var dts = new Object();
		var now = new Date(new Date());
		dts.pubDate = lawItem.Publish_Date__c?  lawItem.Publish_Date__c :  now.toISOString();
		dts.lastDate = lawItem.Last_Date_for_Comments__c ;

		var action = cmp.get("c.saveLawItem");

		var clonedLawItem = JSON.parse(JSON.stringify(lawItem));

		Object.keys(clonedLawItem).forEach(function(fieldName) {
			if( fieldName.indexOf("__r") > 0 ) {
				delete clonedLawItem[fieldName];
			}
		});
		var recordTypeName =  cmp.get("v.currentRecordType");
		var recordTypeOptions = cmp.get('v.recordTypeLst');
		clonedLawItem.RecordTypeId  = recordTypeOptions.find(item=> item.Name == recordTypeName).Id;
		
		var filesForEditMode = cmp.get("v.filesForEditMode");
		var mainFileId = (filesForEditMode && filesForEditMode.MainFile )? filesForEditMode.MainFile.Id  : null;

		action.setParams({
			"lawItem": JSON.stringify(clonedLawItem),
			"dates": JSON.stringify(dts),
			"lawLines": JSON.stringify(lawLines),
			"childLawLines": JSON.stringify(childLawLines),
			"removedLawLines": JSON.stringify(removedLawLines),
			"classLst": JSON.stringify(classLst),
			"removedClsLst": JSON.stringify(classLstToRemove),
			"mainFileId": mainFileId
		});

		action.setCallback(this, function (response) {
			var status = response.getState();
			if (cmp.isValid() && status === "SUCCESS") {
				var parsedRes = JSON.parse(response.getReturnValue());
				location.href = "/" + parsedRes.Id;
			}
		});

		$A.enqueueAction(action);
	},
})