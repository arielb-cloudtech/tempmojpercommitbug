({
    doInit: function (cmp, evt, helper) {
        if( location.href.indexOf("register") > -1 ){
            cmp.set("v.formType", "register");
        }

        window.addEventListener("message", function (event) {
            if ( event.data == 'captchChecked' ) {
                cmp.set("v.reCaptchaChecked", true);
            } 
        }, false);

        var formFields = helper.formFields( cmp.get("v.formType") );
        var fieldNames = Object.keys(formFields);

        fieldNames.forEach(function(formId){
            var formField = formFields[formId];
            $A.createComponents([
                    [   
                        "ui:inputText",
                        {
                            "aura:id" : formId,
                            "label": formField.label,
                            "aria-required": formField.required,
                            "class": "field "+formField.class, 
                            "required": formField.required, 
                            "blur": cmp.getReference("c.validate"),
                            "type": formField.type
                        }
                    ],
                    [
                        "aura:html",
                        { 
                            tag: "div",
                            HTMLAttributes:{
                                "class": "fieldWrapper"+(formField.class?" "+(formField.class):"")
                            }
                        }
                    ]
                ],
                function(component, status, errorMessage){
                    if ( status === "SUCCESS") {
                        var textField = component[0];
                        var wrapper = component[1];
                        wrapper.set("v.body", textField);      

                        var body = cmp.get("v.body");
                        body.push(wrapper);
                        cmp.set("v.body", body);
                    }
                }
            );
        });
    },

    validate : function(component, event, helper) {
        var formFields = helper.formFields( cmp.get("v.formType")  );
        var formFieldId = event.getSource().getLocalId();
        var field = component.find(formFieldId);
        
        var isValid = helper.validateField(field, formFields[formFieldId].validation, component);

        if ( !isValid && !formFields[formFieldId].allowBlur) {
            field.focus();
        }
    },
    
    registerUser: function (cmp, event, helper) { 
        var params = helper.validateFormAndGetValues(cmp);

        /*
        var captchError = "";

        if(! cmp.get("v.reCaptchaChecked")){ //callback is set in community header
            captchError = $A.get("$Label.c.Must_mark");

            if ( !firstInvalid ){
                firstInvalid = true;
            }
        }
        cmp.set("v.captchError", captchError);
        */
        
        if(! cmp.find("checkbox").get("v.value") ) {
            cmp.find("checkbox").set("v.errors", [
                { message: $A.get("$Label.c.Must_mark") }
            ]);
            return;
        }

        if(params.length==0) {
            return;
        }

        var action = cmp.get("c.AddPortalUser");
        action.setParams(params);

        action.setCallback(this, function (res) {
            cmp.set("v.showSpinner", false);

            var state = res.getState();
            // no server exception
            if (state === "SUCCESS") {
                var responseObj = res.getReturnValue();

                if ( responseObj.status == 'success' ) {
                    cmp.set("v.popup", 'registration_complete');  
                } else {
                    cmp.set("v.ServerErrors",   responseObj.errors);
                }
            }
            // server exception has been caught
            else if (state === "ERROR") {
                cmp.set("v.ServerErrors",  [$A.get("$Label.c.Error")]);
                var errors = res.getError();

                errors.forEach(function(errorMessage){
                    if(errors[0].message) {
                        console.log(errorMessage.message);
                    }
                });
            }
        });
        cmp.set("v.showSpinner", true);
        setTimeout(() => {
            cmp.set("v.showSpinner", false);
        }, 3000);
        $A.enqueueAction(action);
    },

    openSendPassword: function (component, event, helper) {
        component.set("v.popup", "send password");
    },

    resetPassword: function(cmp, event, helper) {
        var action = cmp.get("c.sendForgottenPassword");
        var emailField = cmp.find("LostPasswordEmail");
        action.setParams({ 'username': emailField.get("v.value") });
        action.setCallback(this, function(response) {
            var popupErrors = [];
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseObj = response.getReturnValue();
                if ( responseObj.status == 'success' ) {
                    cmp.set("v.popup", 'password has been sent' );
                }
                if ( responseObj.message ) {
                    emailField.set("v.errors", [{message:responseObj.message}]); 
                }
            } 
            else if (state === "ERROR") {
                var errors = response.getError();
                errors.forEach(function(err){
                    if(err.message) {
                        popupErrors.push(err.message);
                    }
                });
                cmp.set("v.ErrorMsg", popupErrors);   
            }
        });
        $A.enqueueAction(action);
    },

    closeModel: function (component, event, helper) {
        component.set("v.popup", "");
        window.location = '/s/moj-login';
    },

    handleRegisterGoogle: function (cmp, event, helper) {
        location.href = '/services/auth/sso/Google';
    },

    handleRegisterFacebook: function (cmp, event, helper) {
        location.href = '/services/auth/sso/Facebook';
    },

    resendVerificationEmail: function (cmp, event, helper) {
        cmp.set("v.popup", "");
        var action = cmp.get("c.resendRegistrationEmail");
       
        action.setParams({
            'userName': cmp.find('Email').get("v.value")
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state = ' + state);
            setTimeout(() => {
                cmp.set("v.popup", "registration_complete");
            }, 1500);
        });
        $A.enqueueAction(action);
    }, 

    doLogin: function(cmp, evt, helper) {
        var action = cmp.get("c.UserLogin");
        var params = helper.validateFormAndGetValues(cmp);
        console.log(params);
        if(params.length==0) {
            return;
        }
        
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseObj = response.getReturnValue();
                if ( responseObj.status == 'success' ) {
                }
                else {
                    cmp.set("v.Result", response.getReturnValue());
                    cmp.set("v.ServerErrors", [$A.get("$Label.c.Wrong_password")]);
                }
                    

            }else if (state === "ERROR") {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})