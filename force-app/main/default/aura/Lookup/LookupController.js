({
 
	getButtonComponent : function(component, event, helper) {
		console.log("~~~~~~getButtonComponent~~~~~~");
		var btnCmp = component.find("userinput");
		console.log("btnCmp: ", btnCmp);
		return btnCmp;
	},

	searchField : function(component, event, helper) {
		var currentText = event.getSource().get("v.value");
		console.log("currentText", currentText);
		console.log("sadf", component.get('v.selectRecordId'));
        var resultBox = component.find('resultBox');
        if(currentText.length > 0) {
            $A.util.addClass(resultBox, 'slds-is-open');
        }else {
            $A.util.removeClass(resultBox, 'slds-is-open');
        }
        var action = component.get("c.getUsers");
			action.setParams({ userPreffix : component.get('v.selectRecordName') });
			action.setCallback(this, function(response) {
            var STATE = response.getState();
            
			if(STATE === "SUCCESS") {
                var returnValue = JSON.parse(JSON.stringify(response.getReturnValue())); 
					component.set("v.usersList", returnValue);
            }
			
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    setSelectedRecord : function(component, event, helper) {
        var currentText = event.currentTarget.id;
        var resultBox = component.find('resultBox');
        $A.util.removeClass(resultBox, 'slds-is-open');
        component.set("v.selectRecordName", event.currentTarget.dataset.name);
        component.set("v.selectRecordId", currentText);
    },
})