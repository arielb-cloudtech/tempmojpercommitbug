({
    doInit: function (cmp, event, helper) {    
        helper.setTabContext(cmp, window.location.hash.substr(1, window.location.hash.length)); 

        window.addEventListener("hashchange", 
            function(){
                helper.setTabContext(cmp, window.location.hash.substr(1,100));
            }, false);
    },

    handleComponentEvent : function (cmp, event) {
        var currentUser = event.getParam('currentUser');
    
        if( ! currentUser.Name ) {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": '/s/moj-login?goto='
            });
            urlEvent.fire();
        }
        cmp.set('v.CurrentUser', currentUser);
    },

    gotoTab: function (cmp, event, helper) {
        window.location.hash = event.target.getAttribute('data-tab');
	},
})