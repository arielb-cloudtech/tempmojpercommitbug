({
	setTabContext : function(cmp, requiredTab) 
	{
        var currentTab = "comments";
        var pageTitle =  $A.get("$Label.c.My_Comment_Header_Personal");
        switch(requiredTab){
            case "drafts":
                currentTab = requiredTab;
                pageTitle =  $A.get("$Label.c.Draft_notes");

            break;
            case "subscriptionSettings":
                currentTab = requiredTab;
                pageTitle =  $A.get("$Label.c.Registration_for_mailing_alerts_Personal_Area");
            break;
            case "settings":
                currentTab = requiredTab;
                pageTitle = $A.get("$Label.c.Update_profile");
            break;
        }
        cmp.set("v.CurrentTab", currentTab);
        cmp.set("v.PageTitle", pageTitle);

        return currentTab;
	}
})