({
	changeFile: function (cmp,  fileType, uploadedFile, callbackAfterDelete) {
		var filesForEditMode = cmp.get("v.filesForEditMode");
		var lawItem = cmp.get("v.newLawItem");
		var action = cmp.get("c.updateFileTypes");
		var oldFileId = filesForEditMode[fileType] ? filesForEditMode[fileType].Id : null;
		action.setParams({ 
			"lawItemId": lawItem.Id ,
			"filesId": [ uploadedFile.documentId],
			"fileType": fileType,
			"oldFileId": oldFileId
		});
        
		action.setCallback(this, function (response) {
			var status = response.getState();
			if (cmp.isValid() && status === "SUCCESS") {
				var filesForEditMode = cmp.get("v.filesForEditMode");
				filesForEditMode[fileType] = {Name: uploadedFile.name, Id: uploadedFile.documentId};
				cmp.set("v.filesForEditMode", filesForEditMode);
				callbackAfterDelete();
			}
		});
		$A.enqueueAction(action);
	},

	resetFieldsAndFiles: function (cmp) {
		var filesToDelete = [];
		var filesForEditMode = cmp.get("v.filesForEditMode");
		['RIAFile','HomeshFile','NegligibleEffectFile','OtherFile'].forEach( function(fileSection) {
			if ( filesForEditMode[fileSection] ) {
				filesToDelete.push( filesForEditMode[fileSection].Id );
				filesForEditMode[fileSection] = null;
			}
		});
		
		if ( filesToDelete.length > 0 ) {
			var action = cmp.get("c.deleteFileServerBulk");
			action.setParams({ "fileIds": filesToDelete });
			$A.enqueueAction(action);
		}

		cmp.get("v.filesForEditMode", filesForEditMode);
		var lawItem = cmp.get("v.newLawItem");
		lawItem.RIA_File_URL__c = null;
		lawItem.No_RIA_Reason__c = null;
		lawItem.Five_Year_Plan_URL__c = null;
		lawItem.Reason_for_Gov_Decision__c = null;
		lawItem.No_RIA_Other_Reason__c = null;
		lawItem.RIA_File_Name__c = null;
		lawItem.Five_Year_Plan_File_Name__c = null;
		lawItem.Negligible_Impact_Name__c = null;
		lawItem.No_RIA_Other_Name__c = null;

		cmp.set("v.newLawItem", lawItem);
	}
})