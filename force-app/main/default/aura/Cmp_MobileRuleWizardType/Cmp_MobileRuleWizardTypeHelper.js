({
	saveRuleHelper : function(cmp, evt, helper) 
	{
		try 
		{
			var isEdit = cmp.get("v.isWizardEdit");
			var editIndx = cmp.get("v.editIndex");
			cmp.set("v.showWizard", "false");
			var process = cmp.get("v.chosenProcess");
			switch(process) 
			{
				case '1':
				var newRules = [];//cmp.get("v.ruleLst");
				var chosenSubs = cmp.get("v.chosenSubs");
				var typeSelection = cmp.get("v.typeSelection");
				for(const key in chosenSubs)
				{
					if(typeSelection[5] > 0)
					{
						for(const key2 in typeSelection)
						{
							var newRule = new Object();
							newRule.Classification__c = chosenSubs[key].Id;
							newRule.ClassificationName = chosenSubs[key].Name;
							if(typeSelection[key2] == true && key2 != '5')
							{
								switch(key2) 
								{
									case '0':
									newRule.Item_Type__c = 'Memorandum of Law';
									break;
									case '1':
									newRule.Item_Type__c = 'Secondary Legislation Draft';
									break;
									case '2':
									newRule.Item_Type__c = 'Support Test Draft';
									break;
									case '3':
									newRule.Item_Type__c = 'Ministry Directive or Procedure Draft';
									break;
									case '4':
									newRule.Items_With_RIA__c = true;
									break;
								}
								if(isEdit == "true")
								{
									newRule.Id = newRules[editIndx].Id;
									newRules.splice(editIndx, 1, newRule);
								}else
								{
									newRules.push(newRule);
								}
							}
						}
					}else if (typeSelection[5] == 0 || !typeSelection[5])
					{
						var newRule = new Object();
						newRule.Classification__c = chosenSubs[key].Id;
						newRule.ClassificationName = chosenSubs[key].Name;
						if(isEdit == "true")
						{
							newRule.Id = newRules[editIndx].Id;
							newRules.splice(editIndx, 1, newRule);
						}else
						{
							newRules.push(newRule);
						}
					}
				}
				break;
				case '2':
				var newRules = [];//cmp.get("v.ruleLst");
				var chosenSubs = cmp.get("v.selectedLaws");
				var typeSelection = cmp.get("v.typeSelection");
				for(const key in chosenSubs)
				{
					if(typeSelection[5] > 0)
					{
						for(const key2 in typeSelection)
						{
							var newRule = new Object();
							newRule.Law__c= chosenSubs[key].Id;
							newRule.LawName = chosenSubs[key].Name;
							if(typeSelection[key2] == true && key2 != '5')
							{
								switch(key2) 
								{
									case '0':
									newRule.Item_Type__c = 'Memorandum of Law';
									break;
									case '1':
									newRule.Item_Type__c = 'Secondary Legislation Draft';
									break;
									case '2':
									newRule.Item_Type__c = 'Support Test Draft';
									break;
									case '3':
									newRule.Item_Type__c = 'Ministry Directive or Procedure Draft';
									break;
									case '4':
									newRule.Items_With_RIA__c = true;
									break;
								}
								if(isEdit == "true")
								{
									newRule.Id = newRules[editIndx].Id;
									newRules.splice(editIndx, 1, newRule);
								}else
								{
									newRules.push(newRule);
								}
							}
						}
					}else if (typeSelection[5] == 0 || !typeSelection[5])
					{
						var newRule = new Object();
						newRule.Law__c = chosenSubs[key].Id;
						newRule.LawName = chosenSubs[key].Name;
						if(isEdit == "true")
						{
							newRule.Id = newRules[editIndx].Id;
							newRules.splice(editIndx, 1, newRule);
						}else
						{
							newRules.push(newRule);
						}
					}
					
				}
				break;
				case '3':
				var newRules = [];//cmp.get("v.ruleLst");
				var chosenSubs = cmp.get("v.chosenOffices");
				var typeSelection = cmp.get("v.typeSelection");
				for(const key in chosenSubs)
				{
					if(typeSelection[5] > 0)
					{
						for(const key2 in typeSelection)
						{
							var newRule = new Object();
							newRule.Ministry__c= chosenSubs[key].Id;
							newRule.MinistryName = chosenSubs[key].Name;
							if(typeSelection[key2] == true && key2 != '5')
							{
								switch(key2) 
								{
									// case '0':
									// newRule.Item_Type__c = 'תזכיר חוק';
									// break;
									// case '1':
									// newRule.Item_Type__c = 'חקיקת משנה';
									// break;
									// case '2':
									// newRule.Item_Type__c = 'טיוטת מבחן תמיכה';
									// break;
									// case '3':
									// newRule.Item_Type__c = 'טיוטת נוהל או הנחיה';
									// break;
									// case '4':
									// newRule.Items_With_RIA__c = true;
									// break;
									case '0':
									newRule.Item_Type__c = 'Memorandum of Law';
									break;
									case '1':
									newRule.Item_Type__c = 'Secondary Legislation Draft';
									break;
									case '2':
									newRule.Item_Type__c = 'Support Test Draft';
									break;
									case '3':
									newRule.Item_Type__c = 'Ministry Directive or Procedure Draft';
									break;
									case '4':
									newRule.Items_With_RIA__c = true;
									break;
								}
								if(isEdit == "true")
								{
									newRule.Id = newRules[editIndx].Id;
									newRules.splice(editIndx, 1, newRule);
								}else
								{
									newRules.push(newRule);
								}
							}
						}
					}else if (typeSelection[5] == 0 || !typeSelection[5])
					{
						var newRule = new Object();
						newRule.Ministry__c = chosenSubs[key].Id;
						newRule.MinistryName = chosenSubs[key].Name;
						if(isEdit == "true")
						{
							newRule.Id = newRules[editIndx].Id;
							newRules.splice(editIndx, 1, newRule);
						}else
						{
							newRules.push(newRule);
						}
					}
					
				}
				break;
				case '5':
				var newRules = [];//cmp.get("v.ruleLst");
				var typeSelection = cmp.get("v.typeSelection");
				var newRule = new Object();
				for(const key2 in typeSelection)
				{
					var newRule = new Object();
					if(typeSelection[key2] == true && key2 != '5')
					{
						switch(key2) 
						{
							// case '0':
									// newRule.Item_Type__c = 'תזכיר חוק';
									// break;
									// case '1':
									// newRule.Item_Type__c = 'חקיקת משנה';
									// break;
									// case '2':
									// newRule.Item_Type__c = 'טיוטת מבחן תמיכה';
									// break;
									// case '3':
									// newRule.Item_Type__c = 'טיוטת נוהל או הנחיה';
									// break;
									// case '4':
									// newRule.Items_With_RIA__c = true;
									// break;
									case '0':
									newRule.Item_Type__c = 'Memorandum of Law';
									break;
									case '1':
									newRule.Item_Type__c = 'Secondary Legislation Draft';
									break;
									case '2':
									newRule.Item_Type__c = 'Support Test Draft';
									break;
									case '3':
									newRule.Item_Type__c = 'Ministry Directive or Procedure Draft';
									break;
									case '4':
									newRule.Items_With_RIA__c = true;
									break;
						}
						if(isEdit == "true")
						{
							newRule.Id = newRules[editIndx].Id;
							newRules.splice(editIndx, 1, newRule);
						}else
						{
							newRules.push(newRule);
						}
						// newRules.push(newRule);
					}
				}
				break;
			}
			var sObjectEvent = $A.get("e.c:updateRulesMobile");
			sObjectEvent.setParams({
				"rules": newRules 
			});
			sObjectEvent.fire();

		} catch (err) 
		{
			console.log(err.message);
		}
	}
})