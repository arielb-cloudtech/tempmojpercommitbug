({
	doInit : function(cmp, evt, helper) 
	{
		var op = [];
			
			var item = {
				label: $A.get("$Label.c.Subject_e_g_selections"),
				value: '1'
			};
			
			op.push(item);
			var item = {
				label: $A.get("$Label.c.Related_legislation_Checkbox_Personal_area"),
				value: '2'
			};
			
			op.push(item);
			var item = {
				label: $A.get("$Label.c.Government_Office_Distributor_Checkbox_Personal_area"),
				value: '3'
			};
			
			op.push(item);
			
			var item = {
				label: $A.get("$Label.c.Document_Type_Search_Page"),
				value: '5'
			};
			
			op.push(item);

			cmp.set("v.choices", op);
	},

	next : function(cmp, evt, helper) 
	{
		var inMain = cmp.get("v.inMain");
		var choice = cmp.get("v.choice");
		cmp.set("v.chosenProcess", choice);
		console.log(choice);
		if(inMain == "false" && choice && choice != '4')
		{
			choice = '4';
			cmp.set("v.choice", choice);
		}else if(choice == '4')
		{
			console.log("in else");
			cmp.set("v.choice", '');
			cmp.set("v.inMain", "true");
		}else
		{
			cmp.set("v.inMain", "false");
		}
	},

	prev : function(cmp, evt, helper) 
	{
		var choice = cmp.get("v.choice");
		if(choice == '4')
		{
			cmp.set("v.choice", cmp.get("v.chosenProcess"));
		}else
		{
			cmp.set("v.choice", cmp.get("v.chosenProcess"));
			cmp.set("v.inMain", "true");
		}
	},

	saveRule : function(cmp, evt, helper) 
	{
		helper.saveRuleHelper(cmp, evt, helper);
		
	},
	// updateVarsAddRule : function(cmp, evt, helper) 
	// {
	// 	console.log("in add rule func from mobile");
	// 	var params = evt.getParams();
	// 	cmp.set("v.chosenSubs", params.subs);
	// 	cmp.set("v.selectedOffices", params.offices);
	// 	cmp.set("v.selectedLaws", params.laws);
	// 	cmp.set("v.typeSelection", params.types);
	// 	helper.saveRuleHelper(cmp, evt, helper);
		
	// },
})