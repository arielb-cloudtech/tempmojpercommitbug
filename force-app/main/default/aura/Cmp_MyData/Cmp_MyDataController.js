({
    doInit: function (cmp, event, helper) {
        // cmp.set("v.userId", '0051t0000028Nq4AAE');	
        var action = cmp.get("c.getUser");
        console.log(cmp.get("v.userId"));
        action.setParams({ userId: cmp.get("v.userId") });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                console.log("res user:");
                console.log(response.getReturnValue());
                var returnValue = JSON.parse(JSON.stringify(response.getReturnValue()))[0];
                var name = returnValue.FirstName + ' ' + returnValue.LastName;
                cmp.set("v.userName", name);
                if(returnValue.Contact)
                {
                    cmp.set("v.userRole", returnValue.Contact.Role__c);
                }
                var initial = returnValue.FirstName.substring(0, 1) + returnValue.LastName.substring(0, 1);
                cmp.set("v.userInitial", initial);
            }
        });
        $A.enqueueAction(action);
    },

    handleClick: function (cmp, event, helper) {
        var eve = $A.get("e.c:Cmp_TabEvent");
        eve.setParams({
            "tabId": '4'
        });
        eve.fire();
    }
})