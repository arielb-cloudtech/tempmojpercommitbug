({
    doInit: function(cmp, evt, helper) {
        var alertModeOptions = [];

        alertModeOptions.push({
            label: $A.get("$Label.c.All_documents_published_on_the_site_checkbox_Personal_area"),
            value: 'all'
        });
        alertModeOptions.push({
            label: $A.get("$Label.c.Set_rules_for_custom_alerts_Checkbox_Person_area"),
            value: 'costume'
        });
        cmp.set("v.alertModeOptions", alertModeOptions);

        // fetch filter values
        var filtersList = helper.getBaseFilterObject();

        var actionRegistrationData = cmp.get("c.getMailingRules"); 

        actionRegistrationData.setCallback(this, function(response) {
            var relatedLawIds = new Set([]);
            if(response.getState() == "SUCCESS") {
                var dataResponseObject = response.getReturnValue();
                if( dataResponseObject.mailingRules.length == 0 ) {
                   
                }
                cmp.set("v.dailyAlerts", dataResponseObject.dailySummery);
                cmp.set("v.weeklyAlerts", dataResponseObject.weeklySummery);
                cmp.set("v.alertMode", dataResponseObject.subscribeToAllMails ? 'all': 'costume' );

                dataResponseObject.mailingRules
                                    .filter(f=>f.type=="relatedLaws")
                                    .forEach(
                                        function(relatedLaw){
                                            relatedLaw.values.forEach(value => relatedLawIds.add(value));
                                        });
            } 
            
            var actionGetFilters = cmp.get("c.getFilters");
            
            actionGetFilters.setParams({relatedLaws:Array.from(relatedLawIds)});

            actionGetFilters.setCallback(this, function(response) {
                if(response.getState() == "SUCCESS") {
                    var responseObj = response.getReturnValue();
                    filtersList.filter(s=>s.name=='relatedLaws')[0].lawTopics = responseObj.related_law_topics;
                    filtersList.filter(s=>s.name=='documentType')[0].options = responseObj.lawTypes.map(s=>({ value:s.Id, label:s.Name }));
                    filtersList.filter(s=>s.name=='documentType')[0].options.push( { value:'RIA_only', label: $A.get("$Label.c.Only_Documents_that_Include_RIA_Checkbox_Search_Page")} );
                    filtersList.filter(s=>s.name=='relatedLaws')[0].options = responseObj.related_laws;

                    filtersList.filter(s=>['classifications','offices'].indexOf(s.name) > -1 )
                            .forEach( function(filterSection) {
                        var filterResponseItems = responseObj[filterSection.name];
                        filterSection.itemsByFirstChar = [];
                        if(! filterResponseItems ) return;
                        filterResponseItems.sort((a, b) => a.Name > b.Name ? 1 : -1);
                        var firstChar = "";
                        filterResponseItems.forEach(function(classificationItem) {
                            if ( firstChar!= classificationItem.Name[0] ) {
                                firstChar = classificationItem.Name[0];
                                filterSection.itemsByFirstChar.push({ 'firstChar': firstChar, 'items': [] });
                            }
                            filterSection.options.push(classificationItem);
                            classificationItem.checked = false;
                            filterSection.itemsByFirstChar.find(v=>v.firstChar == firstChar).items.push(classificationItem);
                        });
                    });
                    cmp.set('v.filtersList', filtersList );
                    var rules = dataResponseObject.mailingRules;
                    rules.forEach(function(rule){
                        helper.generateRuleLabel(cmp, rule);
                    });
                    cmp.set("v.Rules" , rules);
                }
            });

            $A.enqueueAction(actionGetFilters);
        });

        $A.enqueueAction(actionRegistrationData);

    },
    relatedLawFilterChange: function(cmp, event, helper) 
	{
		helper.getRelatedLaws(cmp);
    },
    checkRelatedLawItem: function(cmp, event, helper) 
	{
        var filters = cmp.get("v.filtersList");
        var relatedLawsFilter = filters.filter(s=>s.name=='relatedLaws')[0];
        relatedLawsFilter.currentValue = relatedLawsFilter.options.filter(item=>item.checked);
        cmp.set("v.filtersList", filters);
        
    },
    closeWizard: function(cmp, evt, helper) {
        cmp.set("v.wizardStep", "");
    },
    
    wizardNext: function (cmp, evt, helper) {
        var currentStep = cmp.get("v.wizardStep");
        var filter =cmp.get('v.filtersList');
        var nextStep = filter[0].currentValue;
        
        if( currentStep == nextStep ) {
            nextStep = 'documentType';
        }

        if ( currentStep == 'documentType' ) {
            helper.endWizard(cmp);
            nextStep = '';
        }

        if( nextStep == 'relatedLaws' ) {
            helper.getRelatedLaws(cmp);
        }
        cmp.set("v.wizardStep", nextStep);
    },

    savePage: function(cmp, evt, helper) {
        var action = cmp.get("c.saveMailingRules"); 

        var actionParams = {
            dailyAlerts: cmp.get("v.dailyAlerts"),
            weeklyAlerts: cmp.get("v.weeklyAlerts"),
            allImmediate: false,
            rules: cmp.get("v.Rules").filter(r=>!r.isDeleted)
        };

        if( cmp.get('v.alertMode') == 'all' ) {
            actionParams.allImmediate = true;
            actionParams.rules = [];
        }

        if ( cmp.get("v.stopAllNotification") ) {
            actionParams.dailyAlerts = false;
            actionParams.weeklyAlerts = false;
            actionParams.allImmediate = false;
            actionParams.rules = [];
        }

        action.setParams(actionParams);
        action.setCallback(this, function(response) {
            console.log(response);
        });

        $A.enqueueAction(action);
    },
    createNewRule: function(cmp, evt, helper) {
        cmp.set("v.editRuleIndex", -1);
        helper.openWizard(cmp);
    },

    deleteRule: function(cmp, evt, helper) {
        var ruleIndex = evt.target.getAttribute('data-ruleIndex');
        var rules = cmp.get("v.Rules");
        rules[ruleIndex].isDeleted = true;
         cmp.set("v.Rules", rules);
    },

    editRule: function(cmp, evt, helper) {
        var ruleIndex = evt.target.getAttribute('data-ruleIndex');
        cmp.set("v.editRuleIndex", ruleIndex);
        helper.openWizard(cmp);
    }
})