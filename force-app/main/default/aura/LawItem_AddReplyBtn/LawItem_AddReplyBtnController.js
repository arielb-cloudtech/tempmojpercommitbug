({
	addReply: function (component, event, helper) {

		if (component.get("v.replyId").length == 0)
			return;

		if (component.get("v.myReply").length == 0 && component.get("v.filesList").length == 0)
			return;

		var action = component.get("c.SaveNewReply");
		var comment = JSON.parse(JSON.stringify(component.get("v.comment")));

		console.log("body: ", component.get("v.myReply"));
		console.log("replyId: ", component.get("v.replyId"));
		console.log("deletedFiles: ", component.get("v.deletedFiles"));
		console.log("beforeAction ");
		action.setParams({
			"body": component.get("v.myReply"),
			"replyId": component.get("v.replyId"),
			"filesToDelete": component.get("v.deletedFiles"),
			"disp": component.get("v.pubChoice")
		});

		action.setCallback(this, function (response) {

			var state = response.getState();
			if (component.isValid() && state === "SUCCESS") {
				var returnValue = response.getReturnValue();

				comment["mCommentReplys"].push(returnValue);
				component.set("v.comment", comment);
				component.set("v.myReply", "");
				component.set("v.replyId", "");
				var eve = $A.get("e.c:LawItem_RemoveFileEv");
				eve.fire();
				location.reload();
			}
			else {
				alert("Custom ERROR!!!");
			}
		});
		$A.enqueueAction(action);
	}
})