({
	closeFilters : function(cmp, evt, helper) 
	{
		cmp.set("v.showWizard", "false");	
	},
	doInit : function(cmp, evt, helper) 
	{
		var typeSelection = [];
		
		var i;
		// for (i = 0; i <= 4; i++) 
		// { 
		// 	typeSelection[i] = false;
		// }
		for (i = 0; i <= 3; i++) 
		{ 
			typeSelection[i] = true;
		}

		typeSelection[4] = false;

		cmp.set("v.typeSelection", typeSelection);
	},
	checkAll : function(cmp, evt, helper) 
	{
		var typeSelection = cmp.get("v.typeSelection");

		var counter = 0;
		for(const key in typeSelection)
		{
			if(typeSelection[key])
			{
				counter++;
			}
		}
		typeSelection[5] = counter;

		cmp.set("v.typeSelection", typeSelection);
	},
})