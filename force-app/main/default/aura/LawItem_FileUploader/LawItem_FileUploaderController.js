({ 
	onUploadedFilesFinish: function (component, event, helper) {
		try {
			console.log("~~~Start");
			
			var uploadedFiles = event.getParam("files"); 
			console.log("uploadedFiles", uploadedFiles.length);
			var fList = component.get("v.filesList");
			console.log("~~~for");
			console.log("~~~component.get(v.filesList)", component.get("v.filesList"));
			for (var i = 0; i < uploadedFiles.length; i++) {
				console.log("~~~fList", fList);
				if(fList == null || fList == undefined){
					fList = [];
				}
				fList.push(uploadedFiles[i].documentId);
				console.log(uploadedFiles[i] + ' ' + uploadedFiles[i].documentId );
				//filesList
				console.log("~~~component");
				$A.createComponent(
					"c:LawItem_FileLoaded",
					{
						"aura:id": "section1",
						"file": uploadedFiles[i],
						"deleteFile": true,
						"fileId": uploadedFiles[i].documentId
					},
					function (newComponent, status, errorMessage) {
						console.log("~~~func");
						if (status === "SUCCESS") {
							console.log("~~~SUCCESS");
							var body = component.get("v.body");
							console.log("newComponent", newComponent);
							console.log("body", body);

							body.push(newComponent);
							component.set("v.body", body);
						}
						else if (status === "INCOMPLETE") {
							console.log("No response from server or client is offline.");
						}
						else if (status === "ERROR") {
							console.log("Error: " + errorMessage);
						}
					}
				);
			}
			component.set("v.filesList", fList);		
			
		} catch (error) {
			alert(error.message);
		}
	},

	removeFile: function (component, event, helper) {
		var del = component.get("v.deletedFiles");
		var iD = event.getParam("docId");
		console.log('iD = ' + iD);
		del.push(iD);		
		component.set("v.deletedFiles", del);
		console.log('fileUploadedController = ' + component.get("v.deletedFiles"));
	},

	clear:function(component, event, helper) {		
		var body = component.get("v.body");		
		console.log ('length = ' +  body.length);
		var b;
		if (body.length > 0){
			component.set("v.body", []);
		}
		component.set("v.deletedFiles", null);
		component.set("v.filesList", null);		
	}
})