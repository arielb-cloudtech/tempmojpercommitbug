({
	removeLaw : function(cmp, evt, helper) 
	{
		console.log(evt.target.classList[0]);
		var indxVar = evt.target.classList[0];
		
		var removeLawEvt = $A.get("e.c:removeLawEventChild");
		removeLawEvt.setParams({"indx" : indxVar});
		removeLawEvt.fire();

		helper.getResults(cmp, evt, helper);
	},
	removeSecLaw : function(cmp, evt, helper) 
	{
		console.log(evt.target.classList[0]);
		var indxVar = evt.target.classList[0];
		
		var removeLawEvt = $A.get("e.c:removeSecLawEventChild");
		removeLawEvt.setParams({"indx" : indxVar});
		removeLawEvt.fire();

		helper.getResults(cmp, evt, helper);
	},
	doInit : function(cmp, evt, helper) 
	{
		helper.initOpts(cmp, evt, helper);

		var choiceObj = new Object();
		var dates = new Object();
		dates.start = '';
		dates.end = '';
		cmp.set("v.dateChoicesObj", dates);

		var action = cmp.get("c.getSubs");
			action.setParams({ subSearch : "" });
			action.setCallback(this, function(response) {
            var STATE = response.getState();
            
			if(STATE === "SUCCESS") {
				//console.log(response.getReturnValue());
                var returnValue = JSON.parse(response.getReturnValue()); 
				cmp.set("v.subLst", returnValue);
            }
			
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);

		var rtLst;
		var action = cmp.get("c.getRecordTypes");
			//action.setParams({ subSearch : "" });
			action.setCallback(this, function(response) {
            var STATE = response.getState();
            
			if(STATE === "SUCCESS") {
				console.log(response.getReturnValue());
				rtLst = JSON.parse(response.getReturnValue()); 
				for(const key in rtLst)
				{
					switch(rtLst[key].Name) 
					{
						case 'Memorandum of Law':
							choiceObj.checkMemo = new Object();
							choiceObj.checkMemo.name = $A.get("$Label.c.MemorandumOfLaw");
							choiceObj.checkMemo.Id = rtLst[key].Id;
							choiceObj.checkMemo.isSelected = false;
						  break;
						case 'Ministry Directive or Procedure Draft':
							choiceObj.checkProt = new Object();
							choiceObj.checkProt.name = $A.get("$Label.c.Draft_procedure_or_guideline");
							choiceObj.checkProt.Id = rtLst[key].Id;
							choiceObj.checkProt.isSelected = false;
						  break;
						case 'Secondary Legislation Draft':
							choiceObj.checkSec = new Object();
							choiceObj.checkSec.name = $A.get("$Label.c.DraftLegislation");
							choiceObj.checkSec.Id = rtLst[key].Id;
							choiceObj.checkSec.isSelected = false;
						  break;
						case 'Support Test Draft':
							choiceObj.checkTest = new Object();
							choiceObj.checkTest.name = $A.get("$Label.c.Draft_test_support");
							choiceObj.checkTest.Id = rtLst[key].Id;
							choiceObj.checkTest.isSelected = false;
						  break;
					  }
				}
				try 
				{
					var indxRecType = window.location.search.indexOf("recType");
					console.log("indxRecType");
					console.log(indxRecType);
					var recType = decodeURIComponent(window.location.search.substring(indxRecType).substring(8,9));
					console.log("rectype");
					console.log(recType);
					switch (recType) 
					{
						case "1":
							choiceObj.checkMemo.isSelected = true;
							break;
			
						case "2":
							choiceObj.checkSec.isSelected = true;
							break;
			
						case "3":
							choiceObj.checkProt.isSelected = true;
							break;
			
						case "4":
							choiceObj.checkTest.isSelected = true;
							break;
					}
					cmp.set("v.itmTypeChoicesObj", choiceObj);
					if((indxRecType != -1 && recType) || recType == "=")
					{
						helper.getResults(cmp, evt, helper);
					}
				} catch (err) 
				{
					console.log("err.message2");
					console.log(err.message);
				}
				
            }
			
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
		$A.enqueueAction(action);
	},

	filterResults : function(cmp, evt, helper) 
	{
		setTimeout(() => {
			cmp.set("v.showSpinner", "true");
		}, 1);
		helper.getResults(cmp, evt, helper);
		
	},

	clearFilters : function(cmp, evt, helper) 
	{
		try
		{
			cmp.set("v.showSpinner", "true");
			var itmTypeChoices = cmp.get("v.itmTypeChoicesObj");
			console.log(JSON.stringify(itmTypeChoices));
			for(const key in itmTypeChoices)
			{
				if(key != "numOfChoices")
				{
					itmTypeChoices[key].isSelected = false;
				}
			}
			itmTypeChoices.numOfChoices = 0;
			cmp.set("v.itmTypeChoicesObj", itmTypeChoices);
			cmp.set("v.startDate", "");
			cmp.set("v.endDate", "");
			var empty = [];
			cmp.set("v.chosenSubs", empty);
			cmp.set("v.selectedLaws", empty);
			cmp.set("v.selectedSecLaws", empty);
			cmp.set("v.selectedOffices", empty);
			cmp.set("v.value", "הכל");
			cmp.set("v.RIAFilter", false);
			cmp.set("v.openForComm", false);
			cmp.set("v.closedForComm", false);
			cmp.set("v.orderBy", "Desc");
			cmp.set("v.allClassses", false);
			cmp.set("v.allOffices", false);
			helper.clearClassChoice(cmp, evt, helper);
			helper.clearOfficeChoice(cmp, evt, helper);
			helper.clearLawChoice(cmp, evt, helper);
			helper.getResults(cmp, evt, helper);
		}catch(err)
		{
			console.log(err.message);
		}
		// setTimeout(() => {
		// }, 1);
		
	},

	openClassFilter : function(cmp, evt, helper) 
	{
		cmp.set("v.isClassFilterOpen", "true");
	},
	openLawFilter : function(cmp, evt, helper) 
	{
		cmp.set("v.isLawFilterOpen", "true");
	},
	openSecLawFilter : function(cmp, evt, helper) 
	{
		cmp.set("v.isSecLawFilterOpen", "true");
	},
	openOfficeFilter : function(cmp, evt, helper) 
	{
		cmp.set("v.isOfficeFilterOpen", "true");
	},
	clearClassChoice : function(cmp, evt, helper) 
	{
		helper.clearClassChoice(cmp, evt, helper);
	},
	clearOfficeChoice : function(cmp, evt, helper) 
	{
		helper.clearOfficeChoice(cmp, evt, helper);
	},
	removeSub : function(cmp, evt, helper) 
	{
		console.log(cmp.get("v.chosenSubs"));
		console.log(evt.getParam("item").Name);
		var chosenSubs = cmp.get('v.chosenSubs');
        var item = evt.getParam("index");
        chosenSubs.splice(item, 1);
        cmp.set('v.chosenSubs', chosenSubs);
	},
	removeSubDesktop : function(cmp, evt, helper) 
	{
		try {
			console.log(cmp.get("v.chosenSubs"));
			console.log(evt.getParam("item").Name);
			var chosenSubs = cmp.get('v.chosenSubs');
			var item = evt.getParam("index");
			chosenSubs.splice(item, 1);
			cmp.set('v.chosenSubs', chosenSubs);
			helper.clearSpecificClassChoice(cmp, evt, helper);
			helper.getResults(cmp, evt, helper);
		} catch (err) {
			console.log("err");
			console.log(err.message);
		}
		
	},
	removeOffDesktop : function(cmp, evt, helper) 
	{
		try {
			console.log(cmp.get("v.selectedOffices"));
			console.log(evt.getParam("item").Name);
			var chosenSubs = cmp.get('v.selectedOffices');
			var item = evt.getParam("index");
			chosenSubs.splice(item, 1);
			cmp.set('v.selectedOffices', chosenSubs);
			helper.clearSpecificOfficeChoice(cmp, evt, helper);
			helper.getResults(cmp, evt, helper);
		} catch (err) {
			// console.log("err");
			// console.log(err.message);
		}
		
	},
	closeFilters : function(cmp, evt, helper) 
	{
		console.log("in close filters");
		cmp.set("v.isFiltersOpen", "false");
	},
	openFiltersOffice : function(cmp, evt, helper) 
	{
		console.log("in open office");
		cmp.set("v.isFiltersOpen", "false");
		cmp.set("v.isFiltersOpenRelaw", "false");
		cmp.set("v.isFiltersOpenSecRelaw", "false");
		cmp.set("v.isFiltersOpenOffice", "true");
	},
	openFiltersRelaw : function(cmp, evt, helper) 
	{
		console.log("in open relaw");
		cmp.set("v.isFiltersOpen", "false");
		cmp.set("v.isFiltersOpenOffice", "false");
		cmp.set("v.isFiltersOpenSecRelaw", "false");
		cmp.set("v.isFiltersOpenRelaw", "true");
		console.log(cmp.get("v.isFiltersOpen"));
		console.log(cmp.get("v.isFiltersOpenOffice"));
		console.log(cmp.get("v.isFiltersOpenRelaw"));
	},
	openFiltersSecRelaw : function(cmp, evt, helper) 
	{
		console.log("in open relaw");
		cmp.set("v.isFiltersOpen", "false");
		cmp.set("v.isFiltersOpenOffice", "false");
		cmp.set("v.isFiltersOpenRelaw", "false");
		cmp.set("v.isFiltersOpenSecRelaw", "true");
		console.log(cmp.get("v.isFiltersOpen"));
		console.log(cmp.get("v.isFiltersOpenOffice"));
		console.log(cmp.get("v.isFiltersOpenRelaw"));
	},

	openDocType : function(cmp, evt, helper) 
	{
		console.log("in open itm type");
		var showType = cmp.get("v.showItmType");
		cmp.set("v.showItmType", !showType);
	},
	
	openResType : function(cmp, evt, helper) 
	{
		console.log("in open res type");
		var showType = cmp.get("v.showResType");
		cmp.set("v.showResType", !showType);
	},

	openDateRng : function(cmp, evt, helper) 
	{
		console.log("in open res type");
		var showType = cmp.get("v.showDateRng");
		cmp.set("v.showDateRng", !showType);
	},
	openSub : function(cmp, evt, helper) 
	{
		console.log("in open res type");
		var showType = cmp.get("v.showSub");
		cmp.set("v.showSub", !showType);
		cmp.set("v.isFiltersOpen", "false");
		cmp.set("v.isFiltersOpenOffice", "false");
		cmp.set("v.isFiltersOpenSecRelaw", "false");
		cmp.set("v.isFiltersOpenRelaw", "false");
	},
	RIAChange : function(cmp, evt, helper) 
	{
		try 
		{
			console.log("in ria change func");
			var pills = cmp.get("v.fltPills");
			var exists = false;
			var riaFLT = cmp.get("v.RIAFilter");
			if(riaFLT)
			{
				for(const key in pills)
				{
					if(pills[key].fltType == 'ria')
					{
						pills[key].label =  'RIA'; 
						exists = true;
					}
				}
				if(!exists)
				{
					var pill = new Object();
					pill.type = 'basic';
					pill.fltType = 'ria';
					pill.label = 'RIA'; 
					pills.push(pill);
				}
	
				console.log("in ria change func2");	
			}else
			{
				for(const key in pills)
				{
					if(pills[key].fltType == 'ria')
					{
						pills.splice(key,1);
					}
				}
			}
			cmp.set("v.fltPills", pills);
		} catch (err) 
		{
			console.log("Err:");
			console.log(err.message);
		}
	},
	changeDocType : function(cmp, evt, helper) 
	{
		try 
		{
			var pills = cmp.get("v.fltPills");
			var itmTypeChoicesObj = cmp.get("v.itmTypeChoicesObj");
			var exists = false;

			if(itmTypeChoicesObj.checkMemo.isSelected)
			{
				for(const key in pills)
				{
					if(pills[key].fltType == itmTypeChoicesObj.checkMemo.name)
					{
						pills[key].label =  itmTypeChoicesObj.checkMemo.name 
						exists = true;
					}
				}
				if(!exists)
				{
					var pill = new Object();
					pill.type = 'basic';
					pill.fltType = itmTypeChoicesObj.checkMemo.name;
					pill.label = itmTypeChoicesObj.checkMemo.name;
					pills.push(pill);
				}
			}else
			{
				for(const key in pills)
				{
					if(pills[key].fltType == itmTypeChoicesObj.checkMemo.name)
					{
						pills.splice(key,1);
					}
				}
			}

		
			
			cmp.set("v.fltPills", pills);
		} catch (err) 
		{
			console.log("Err:");
			console.log(err.message);
		}
	},
	changeDocType2 : function(cmp, evt, helper) 
	{
		try 
		{
			var pills = cmp.get("v.fltPills");
			var itmTypeChoicesObj = cmp.get("v.itmTypeChoicesObj");
			var exists = false;

			if(itmTypeChoicesObj.checkSec.isSelected)
			{
				for(const key in pills)
				{
					if(pills[key].fltType == itmTypeChoicesObj.checkSec.name)
					{
						pills[key].label =  itmTypeChoicesObj.checkSec.name 
						exists = true;
					}
				}
				if(!exists)
				{
					var pill = new Object();
					pill.type = 'basic';
					pill.fltType = itmTypeChoicesObj.checkSec.name;
					pill.label = itmTypeChoicesObj.checkSec.name;
					pills.push(pill);
				}
			}else
			{
				for(const key in pills)
				{
					if(pills[key].fltType == itmTypeChoicesObj.checkSec.name)
					{
						pills.splice(key,1);
					}
				}
			}

		
			
			cmp.set("v.fltPills", pills);
		} catch (err) 
		{
			console.log("Err:");
			console.log(err.message);
		}
	},
	changeDocType3 : function(cmp, evt, helper) 
	{
		try 
		{
			var pills = cmp.get("v.fltPills");
			var itmTypeChoicesObj = cmp.get("v.itmTypeChoicesObj");
			var exists = false;

			if(itmTypeChoicesObj.checkTest.isSelected)
			{
				for(const key in pills)
				{
					if(pills[key].fltType == itmTypeChoicesObj.checkTest.name)
					{
						pills[key].label =  itmTypeChoicesObj.checkTest.name 
						exists = true;
					}
				}
				if(!exists)
				{
					var pill = new Object();
					pill.type = 'basic';
					pill.fltType = itmTypeChoicesObj.checkTest.name;
					pill.label = itmTypeChoicesObj.checkTest.name;
					pills.push(pill);
				}
			}else
			{
				for(const key in pills)
				{
					if(pills[key].fltType == itmTypeChoicesObj.checkTest.name)
					{
						pills.splice(key,1);
					}
				}
			}

		
			
			cmp.set("v.fltPills", pills);
		} catch (err) 
		{
			console.log("Err:");
			console.log(err.message);
		}
	},
	changeDocType4 : function(cmp, evt, helper) 
	{
		try 
		{
			var pills = cmp.get("v.fltPills");
			var itmTypeChoicesObj = cmp.get("v.itmTypeChoicesObj");
			var exists = false;

			if(itmTypeChoicesObj.checkProt.isSelected)
			{
				for(const key in pills)
				{
					if(pills[key].fltType == itmTypeChoicesObj.checkProt.name)
					{
						pills[key].label =  itmTypeChoicesObj.checkProt.name 
						exists = true;
					}
				}
				if(!exists)
				{
					var pill = new Object();
					pill.type = 'basic';
					pill.fltType = itmTypeChoicesObj.checkProt.name;
					pill.label = itmTypeChoicesObj.checkProt.name;
					pills.push(pill);
				}
			}else
			{
				for(const key in pills)
				{
					if(pills[key].fltType == itmTypeChoicesObj.checkProt.name)
					{
						pills.splice(key,1);
					}
				}
			}

		
			
			cmp.set("v.fltPills", pills);
		} catch (err) 
		{
			console.log("Err:");
			console.log(err.message);
		}
	},
	changeDates : function(cmp, evt, helper) 
	{
		var startDt = cmp.get("v.startDate");
		var endDt = cmp.get("v.endDate");
		
		if(startDt != "" && startDt != "NaN/NaN/NaN")
		{
			console.log("startdate");
			console.log(startDt);
			var start = new Date(startDt);
			console.log("startdateparsed");
			var startStr = start.getDate() + "/" + (parseInt(start.getMonth())+1) + "/" + start.getFullYear();
			console.log(startStr);
			//startDt = start.getDate() + "/" + (parseInt(start.getMonth())+1) + "/" + start.getFullYear();
			cmp.set("v.startDateDisp", startStr);
		}
		if(endDt != "" && endDt != "NaN/NaN/NaN")
		{
			console.log(endDt);
			var end = new Date(endDt);
			console.log(end);
			var endStr = end.getDate() + "/" + (parseInt(end.getMonth())+1) + "/" + end.getFullYear();
			console.log(endStr);
			//endDt = end.getDate() + "/" + (parseInt(end.getMonth())+1) + "/" + end.getFullYear();
			cmp.set("v.endDateDisp", endStr);
		}

		try 
		{
			console.log("in date change func");
			var pills = cmp.get("v.fltPills");
			var exists = false;
			for(const key in pills)
			{
				if(pills[key].fltType == 'date')
				{
					pills[key].label =  endStr + ' - ' + startStr; 
					exists = true;
				}
			}
			if(!exists)
			{
				var pill = new Object();
				pill.type = 'basic';
				pill.fltType = 'date';
				pill.label = endStr + ' - ' + startStr; 
				pills.push(pill);
			}

			cmp.set("v.fltPills", pills);
			console.log("in date change func2");	
		} catch (err) 
		{
			console.log("Err:");
			console.log(err.message);
		}
	},
	checkAll : function(cmp, evt, helper) 
	{
		// setTimeout(() => {
		// 	cmp.set("v.showSpinner", "true");
		// }, 10);
		var choices = [];//cmp.get("v.itmTypeChoices");
		var choicesObj = cmp.get("v.itmTypeChoicesObj");
		console.log(choices);
		console.log(JSON.stringify(choicesObj));
		for(const key in choicesObj)
		{
			console.log(choicesObj[key].name)
			if(choicesObj[key].isSelected)
			{
				var str = '';
				if(choices.length == 0)
				{
					str += choicesObj[key].name;
				}
				if(choices.length == 1)
				{
					str += ', ' + choicesObj[key].name + ', ';
				}
				if(choices.length == 2)
				{
					str += choicesObj[key].name;
				}
				choices.push(str);
			}
		}
		cmp.set("v.itmTypeChoices", choices);
		// var filter = $A.get("e.c:runFilterDesktop");	
		// filter.setParams({itmTypeChoices : choicesObj});
		// console.log(filter);
		// filter.fire();
		console.log("selectedOffices221: ");
		console.log(cmp.get("v.selectedOffices"));
		helper.getResults(cmp, evt, helper);
	},
	checkComm : function(cmp, evt, helper) 
	{
		var openforComm = cmp.get("v.openForComm");
		var closedforComm = cmp.get("v.closedForComm");
		var choice = "";

		if(openforComm == true && closedforComm == false)
		{
			// cmp.set("v.value", "פתוח להערות ציבור");
			choice = $A.get("$Label.c.Open_For_Public_Comments_Checkbox_Search_Page");
		}
		else if(openforComm == false && closedforComm == true)
		{
			// cmp.set("v.value", "סגור להערות ציבור");
			choice = $A.get("$Label.c.Closed_for_Public_Comments_Checkbox_Search_Page");
		}else
		{
			// cmp.set("v.value", "הכל");
			choice = $A.get("$Label.c.All");
		}
		console.log(choice);
		cmp.set("v.value", choice);
		helper.getResults(cmp, evt, helper);
	},
	downloadResults : function(cmp, evt, helper) 
	{
		try 
		{
			var laws = cmp.get("v.currentLawsResult");
			console.log("laws");
			console.log(laws);
			var rows = [];
			var headersRow = [];
			headersRow.push($A.get("$Label.c.Item_name") + ": ");
			headersRow.push($A.get("$Label.c.Document_Type_Search_Page") + ": ");
			headersRow.push($A.get("$Label.c.Subjects") + ": ");
			headersRow.push($A.get("$Label.c.Distributor_Office") + ": ");
			headersRow.push($A.get("$Label.c.Legal_Counsel_Sign") + ": ");
			headersRow.push($A.get("$Label.c.Date_of_distribution_Search_page") + ": ");
			headersRow.push($A.get("$Label.c.Last_comment_date") + ": ");
			headersRow.push($A.get("$Label.c.Summary_of_item") + ": ");
			headersRow.push($A.get("$Label.c.Related_Files") + ": ");
			rows.push(headersRow);
			var LawItmIds = [];
			for(const key in laws)
			{
				LawItmIds.push(laws[key].Itemcard.Id);
				// var row = [];
				// row.push(laws[key].Itemcard.Law_Item_Name__c);
				// row.push(laws[key].Itemcard.Id);
				// rows.push(row);
			}

			var action = cmp.get("c.getLawsForExport");
				action.setParams({ lawItmIds : LawItmIds });
				action.setCallback(this, function(response) {
				var STATE = response.getState();
				
				if(STATE === "SUCCESS") {
					//console.log(response.getReturnValue());
					var returnValue = JSON.parse(response.getReturnValue()); 
					returnValue.forEach(function (item) 
					{
						console.log("law item:");
						console.log(item);
						var row = [];
						row.push(item.Law_Item_Name__c);
						row.push(item.RecordType.Name);
						row.push(''); //subjects
						row.push(''); //Pub
						row.push(''); //Counsel
						// row.push(item.Publisher_Account__r.Name);
						// row.push(item.Legal_Counsel_Name__r.FirstName + ' ' + item.Legal_Counsel_Name__r.LastName);
						row.push(item.Publish_Date__c);
						row.push(item.Last_Date_for_Comments__c);
						// row.push(item.Document_Brief__c.replace(/\,/g, ""));
						if(item.ContentDocumentLinks)
						{
							var fileCellStr = '';
							item.ContentDocumentLinks.records.forEach(function (item2) 
							{
								console.log("content:");
								console.log(item2);
								fileCellStr += location.origin + '/sfc/servlet.shepherd/version/download/' + item2.ContentDocument.LatestPublishedVersionId + ';';
							});
							row.push(fileCellStr.substring(0,fileCellStr.length-1));
						}
						console.log("row:");
						console.log(row);
						rows.push(row);
					});
					
					// for(const key in returnValue)
					// {
					// 	console.log("law item:");
					// 	console.log(returnValue[key]);
					// 	var row = [];
					// 	row.push(returnValue[key].Law_Item_Name__c);
					// 	if(returnValue[key].ContentDocumentLinks.records)
					// 	{
					// 		for(const key2 in returnValue[key].ContentDocumentLinks.records)
					// 		{
					// 			console.log("content:");
					// 			console.log(returnValue[key].ContentDocumentLinks.records[0]);
					// 			row.push(location.origin + '/sfc/servlet.shepherd/version/download/' + returnValue[key].ContentDocumentLinks.records[key2].ContentDocument.LatestPublishedVersionId);
					// 		}
					// 	}
					// 	rows.push(row);
					// }
					console.log("rows");
					console.log(rows);
					/// actual export of csv, taken from google
					let csvContent = "data:text/csv;charset=utf-8,\uFEFF" 
						+ rows.map(e => e.join(",")).join("\n");

					var encodedUri = encodeURI(csvContent);
					var link = document.createElement("a");
					link.setAttribute("href", encodedUri);
					// link.setAttribute("href", "data:text/csv;charset=utf-8,\uFEFF" + encodedUri);
					link.setAttribute("download", "Search_Results_Export.csv");
					document.body.appendChild(link); // Required for FF

					link.click(); 
				}
				
				else if (state === "ERROR") {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.log("Error message: " + 
										errors[0].message);
						}
					} else {
						console.log("Unknown error");
					}
				}
			});
			$A.enqueueAction(action);
		} catch (err) 
		{
			console.log("err: ");	
			console.log(err.message);	
		}
		
	},

	getButtonComponent : function(component, event, helper) {
		console.log("~~~~~~getButtonComponent~~~~~~");
		var btnCmp = component.find("userinput");
		console.log("btnCmp: ", btnCmp);
		return btnCmp;
	},
	changeOpen : function(cmp, evt, helper) {
		try 
		{
			console.log('in open type change');
			var chosenVal = cmp.get("v.value");
			var pills = cmp.get("v.fltPills");
			var exists = false;
			if(chosenVal != 'הכל')
			{
				for(const key in pills)
				{
					if(pills[key].fltType == 'openType')
					{
						if(chosenVal == 'פתוח')
						{
							pills[key].label =  'פתוח להערות ציבור';
						}else if(chosenVal == 'סגור')
						{
							pills[key].label =  'סגור להערות ציבור';
						}
						exists = true;
					}
				}
				if(!exists)
				{
					var pill = new Object();
					pill.type = 'basic';
					pill.fltType = 'openType';
					if(chosenVal == 'פתוח')
					{
						pill.label =  'פתוח להערות ציבור';
					}else if(chosenVal == 'סגור')
					{
						pill.label =  'סגור להערות ציבור';
					}
					pills.push(pill);
				}
			}else
			{
				for(const key in pills)
				{
					if(pills[key].fltType == 'openType')
					{
						pills.splice(key, 1);
					}
				}
			}
			cmp.set("v.fltPills", pills);
		} catch (err) 
		{
			console.log("Err:");
			console.log(err.message);
		}
	},

	searchField : function(component, event, helper) {
		var currentText = event.getSource().get("v.value");
		console.log("currentText", currentText);
		console.log("sadf", component.get('v.selectRecordId'));
        var resultBox = component.find('resultBox');
        if(currentText.length > 0) {
            $A.util.addClass(resultBox, 'slds-is-open');
        }else {
            $A.util.removeClass(resultBox, 'slds-is-open');
        }
        var action = component.get("c.getSubs");
			action.setParams({ subSearch : component.get('v.selectRecordName') });
			action.setCallback(this, function(response) {
            var STATE = response.getState();
            
			if(STATE === "SUCCESS") {
				//console.log(response.getReturnValue());
                var returnValue = JSON.parse(response.getReturnValue()); 
				component.set("v.subLst", returnValue);
            }
			
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    setSelectedRecord : function(component, event, helper) {
		console.log("in click");
        var currentText = event.currentTarget.id;
        var resultBox = component.find('resultBox');
		$A.util.removeClass(resultBox, 'slds-is-open');
		console.log(event.currentTarget.dataset.name);
		console.log(currentText);
        // component.set("v.selectRecordName", event.currentTarget.dataset.name);
		// component.set("v.selectRecordId", currentText);
		var chosenSubs = component.get("v.chosenSubs");
		var selectedSub = new Object();
		selectedSub.Name = event.currentTarget.dataset.name;
		selectedSub.Id = currentText;
		selectedSub.label = event.currentTarget.dataset.name;
		selectedSub.type = 'basic';
		chosenSubs.push(selectedSub);
		component.set("v.chosenSubs", chosenSubs);
		component.set("v.selectRecordName", "");

		try 
		{
			console.log("in sub change func");
			var pills = component.get("v.fltPills");
			var exists = false;
			for(const key in pills)
			{
				if(pills[key].fltType == 'sub')
				{
					pills[key].label =  '(נושאים ' +  '(' + chosenSubs.length; 
					exists = true;
				}
			}
			if(!exists)
			{
				var pill = new Object();
				pill.type = 'basic';
				pill.fltType = 'sub';
				pill.label = '(נושאים ' +  '(' + chosenSubs.length; 
				pills.push(pill);
			}

			component.set("v.fltPills", pills);
			console.log("in sub change func2");	
		} catch (err) 
		{
			console.log("Err:");
			console.log(err.message);
		}
		
    },
})