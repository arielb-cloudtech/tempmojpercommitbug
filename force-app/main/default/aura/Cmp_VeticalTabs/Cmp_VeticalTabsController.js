({
    Tab1: function (component, event, helper) {
        console.log('Tab1TTTTTTT');
        var sTab1 = component.find('sTab1');
        $A.util.addClass(sTab1, 'lineClass');
        var sTab2 = component.find('sTab2');
        $A.util.removeClass(sTab2, 'lineClass');
        var sTab3 = component.find('sTab3');
        $A.util.removeClass(sTab3, 'lineClass');

        var spTab1 = component.find('spTab1');
        $A.util.removeClass(spTab1, 'textClass');
        $A.util.addClass(spTab1, 'textChoose');
        var spTab2 = component.find('spTab2');
        $A.util.addClass(spTab2, 'textClass');
        $A.util.removeClass(spTab2, 'textChoose');
        var spTab3 = component.find('spTab3');
        $A.util.addClass(spTab3, 'textClass');
        $A.util.removeClass(spTab3, 'textChoose');


        var eve = $A.get("e.c:Cmp_TabEvent");
        eve.setParams({
            "tabId": '1'
        });
        eve.fire();
    },

    Tab2: function (component, event, helper) {
        var sTab1 = component.find('sTab1');
        $A.util.removeClass(sTab1, 'lineClass');
        var sTab2 = component.find('sTab2');
        $A.util.addClass(sTab2, 'lineClass');
        var sTab3 = component.find('sTab3');
        $A.util.removeClass(sTab3, 'lineClass');

        var spTab2 = component.find('spTab2');
        $A.util.removeClass(spTab2, 'textClass');
        $A.util.addClass(spTab2, 'textChoose');
        var spTab1 = component.find('spTab1');
        $A.util.addClass(spTab1, 'textClass');
        $A.util.removeClass(spTab1, 'textChoose');
        var spTab3 = component.find('spTab3');
        $A.util.addClass(spTab3, 'textClass');
        $A.util.removeClass(spTab3, 'textChoose');

        console.log('Tab2');
        var eve = $A.get("e.c:Cmp_TabEvent");
        eve.setParams({
            "tabId": '2'
        });
        eve.fire();
    },
    Tab3: function (component, event, helper) {
        var sTab1 = component.find('sTab1');
        $A.util.removeClass(sTab1, 'lineClass');
        var sTab2 = component.find('sTab2');
        $A.util.removeClass(sTab2, 'lineClass');
        var sTab3 = component.find('sTab3');
        $A.util.addClass(sTab3, 'lineClass');

        var spTab3 = component.find('spTab3');
        $A.util.removeClass(spTab3, 'textClass');
        $A.util.addClass(spTab3, 'textChoose');
        var spTab1 = component.find('spTab1');
        $A.util.addClass(spTab1, 'textClass');
        $A.util.removeClass(spTab1, 'textChoose');
        var spTab2 = component.find('spTab2');
        $A.util.addClass(spTab2, 'textClass');
        $A.util.removeClass(spTab2, 'textChoose');

        console.log('Tab3');
        var eve = $A.get("e.c:Cmp_TabEvent");
        eve.setParams({
            "tabId": '3'
        });
        eve.fire();
    },
    putNotReaded: function (component, event, helper) {
        var r = event.getParam("notRecieved");
        component.set("v.notReadedComments", r);
        if (r > 0)
            component.set("v.bNotReaded", true);
    },

    showTabs: function (component, event, helper) {
        var r = event.getParam("tabId");
        console.log('Tab showTabs = ' + r);
        var sTab1 = component.find('sTab1');
        var sTab2 = component.find('sTab2');
        var sTab3 = component.find('sTab3');
        var spTab1 = component.find('spTab1');
        var spTab2 = component.find('spTab2');
        var spTab3 = component.find('spTab3');
        switch (r) {
            // case "":
            //     {
            //         $A.util.addClass(sTab1, 'lineClass');
            //         $A.util.removeClass(sTab2, 'lineClass');
            //         $A.util.removeClass(sTab3, 'lineClass');
            //         $A.util.removeClass(spTab1, 'textClass');
            //         $A.util.addClass(spTab1, 'textChoose');
            //         $A.util.addClass(spTab2, 'textClass');
            //         $A.util.removeClass(spTab2, 'textChoose');
            //         $A.util.addClass(spTab3, 'textClass');
            //         $A.util.removeClass(spTab3, 'textChoose');
            //     }
            //     break;
            // case "2":
            //     {
            //         $A.util.removeClass(sTab1, 'lineClass');
            //         $A.util.addClass(sTab2, 'lineClass');
            //         $A.util.removeClass(sTab3, 'lineClass');
            //         $A.util.removeClass(spTab2, 'textClass');
            //         $A.util.addClass(spTab2, 'textChoose');
            //         $A.util.addClass(spTab1, 'textClass');
            //         $A.util.removeClass(spTab1, 'textChoose');
            //         $A.util.addClass(spTab3, 'textClass');
            //         $A.util.removeClass(spTab3, 'textChoose');
            //     }
            //     break;
            case "3": {
                $A.util.removeClass(sTab1, 'lineClass');
                $A.util.removeClass(sTab2, 'lineClass');                
                $A.util.addClass(sTab3, 'lineClass');                
                $A.util.removeClass(spTab3, 'textClass');
                $A.util.addClass(spTab3, 'textChoose');                
                $A.util.addClass(spTab1, 'textClass');
                $A.util.removeClass(spTab1, 'textChoose');                
                $A.util.addClass(spTab2, 'textClass');
                $A.util.removeClass(spTab2, 'textChoose');
            }
                break;
            case "4":
                $A.util.removeClass(sTab1, 'lineClass');
                $A.util.removeClass(sTab2, 'lineClass');                
                $A.util.removeClass(sTab3, 'lineClass');                
                $A.util.addClass(spTab3, 'textClass');
                $A.util.removeClass(spTab3, 'textChoose');                
                $A.util.addClass(spTab1, 'textClass');
                $A.util.removeClass(spTab1, 'textChoose');                
                $A.util.addClass(spTab2, 'textClass');
                $A.util.removeClass(spTab2, 'textChoose');
                break;
        }
    },
})