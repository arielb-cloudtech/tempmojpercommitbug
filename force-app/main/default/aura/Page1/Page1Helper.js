({
	selectLaw : function(cmp, lawId, lawText) { 
		cmp.set("v.showLawError", false );
		cmp.set("v.showResults",  false );
		cmp.set("v.searchVar", lawText);

		var lawItem = cmp.get("v.newLawItem");
		lawItem.Law__c = lawId;
		lawItem.Law__r = {"Id": lawId, "Law_Name__c": lawText };
		cmp.set("v.newLawItem", lawItem);
	}, 
	
	clearSelectedLaw: function(cmp){
		var lawItem = cmp.get("v.newLawItem");
		lawItem.Law__c = null;
		cmp.set("v.newLawItem", lawItem);
	}
})