({
	removeFile:function(component){
		component.set("v.bRemove", false);
		var fileInput;		
        if (component.get("v.comment") == null)
            fileInput = document.getElementById("fileUpload-");
        else
			fileInput = document.getElementById("fileUpload-" + component.get("v.comment.mId"));

		console.log('fileInput.files = ' + fileInput.value);
			fileInput.value = '';	
			console.log('fileInput.files 1 = ' + fileInput.value);	
		
		console.log("remove file 2");	 
		component.set("v.fileName", null);
		component.set("v.fileExtetion", null);
		component.set("v.filebody", null);
		console.log(component.get("v.fileName"));	
		var fileInputLabel;
		if (component.get("v.comment") == null)
			fileInputLabel = document.getElementById("fileInputLabel");
		else	
			fileInputLabel = document.getElementById("fileInputLabel" + component.get("v.comment.mId"));
		fileInputLabel.innerText = "לא נבחר קובץ";	
		
	},

	doSave: function (component, event, helper) {		

		component.set("v.bRemove", true);
		var fileInput;		
        if (component.get("v.comment") == null)
            fileInput = document.getElementById("fileUpload-");
        else
			fileInput = document.getElementById("fileUpload-" + component.get("v.comment.mId"));

		var file = fileInput.files[0];		
        console.log('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
		console.log(file.size);
        var MAX_FILE_SIZE = 6000000;
        if (file.size > MAX_FILE_SIZE) {
            component.set("v.fileName", 'Alert : File size cannot exceed ' + MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
            alert("לא ניתן להעלות קובץ שמשקלו יותר מ- 6mb אנא בחר קובץ אחר.")
            return;
		}
		
        if (component.get("v.comment") == null){
			document.getElementById("fileInputLabel").innerText = file.name;
		}
		else{
			document.getElementById("fileInputLabel" + component.get("v.comment.mId")).innerText = file.name;			
		}
			
		component.set("v.fileName", file.name);
		component.set("v.fileExtetion", file.type);
		console.log("fileName upload file = " + component.get("v.fileName"));

        var objFileReader = new FileReader();
        objFileReader.onload = $A.getCallback(function () {
            var fileContents = objFileReader.result;
            var base64 = String(fileContents);
            fileContents = base64.substr(base64.indexOf(',') + 1);
            component.set("v.filebody", fileContents);
        });

		objFileReader.readAsDataURL(file);		
	},
	
})