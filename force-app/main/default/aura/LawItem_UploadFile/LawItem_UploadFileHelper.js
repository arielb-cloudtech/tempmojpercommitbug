({
	MAX_FILE_SIZE: 6000000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb 
    
    uploadHelper: function(component, event) {
		 
        var fileInput = component.find("fileId").get("v.files");
        
		var file = fileInput[0];
		console.log(file);
		
		
        var self = this;
        
        if (file.size > self.MAX_FILE_SIZE) {
		
			

			component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
			alert("לא ניתן להעלות קובץ שמשקלו יותר מ- 6mb אנא בחר קובץ אחר.")
            return;
        }
 
        var objFileReader = new FileReader();
        
        objFileReader.onload = $A.getCallback(function() {
			var fileContents = objFileReader.result;
			
			


			var base64 = String(fileContents);
			fileContents = base64.substr(base64.indexOf(',') + 1);
            
			component.set("v.fileName", file.name);
			component.set("v.fileExtetion", file.type);
			component.set("v.filebody", fileContents);
			console.log(file.name);
			console.log(file.type);
			console.log(fileContents);
			
            
        });
  
        objFileReader.readAsDataURL(file);
    }
 
})