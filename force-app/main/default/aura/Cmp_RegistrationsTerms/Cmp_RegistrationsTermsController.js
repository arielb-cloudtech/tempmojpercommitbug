({
	doInit : function(cmp, evt, helper) 
	{
		try 
		{
			var lang = location.search.substring(location.search.indexOf("language=")+9, location.search.indexOf("language=") +11);
			var action = cmp.get("c.getRegTermsData");
			action.setCallback(this, function(response) {
				console.log('calling init getPrivacyPolicy');
				var status = response.getState();
				console.log("status");
				console.log(status);
				console.log("res");
				console.log(response.getError());
				if (cmp.isValid() && status === "SUCCESS") {
					// var res = JSON.parse(response.getReturnValue());
					var res = response.getReturnValue();
					console.log(response.getReturnValue());
					var txtLst = [];
					console.log("txtLst");
					console.log(res.Page_Childs__r);
					var i = 0;
					for(const key in res.Page_Childs__r)
					{
						var q = new Object();
						if(lang == 'iw' || !lang)
						{
							q.header = res.Page_Childs__r[key].Header_Hebrew__c;
							q.body = res.Page_Childs__r[key].Body_Hebrew__c;
							q.open = false;
						}else if(lang == "ar")
						{
							q.header = res.Page_Childs__r[key].Header_Arabic__c;
							q.body = res.Page_Childs__r[key].Body_Arabic__c;
							q.open = false;
						}
						txtLst.push(q);
						// cmp.find(moshe).innerHTML = q.question;
						// console.log(cmp.find(moshe));
						i++;
					}
					console.log("txtLst");
					console.log(txtLst);
					cmp.set("v.txtLst", txtLst);
				}

			});
			$A.enqueueAction(action);
		} catch (err) 
		{
			console.log("Err:");	
			console.log(err.messsage);	
		}
		
	},
})
