({ 
	showFiles : function(component, event, helper) {

		var reply = JSON.parse(JSON.stringify(component.get("v.reply")));
		//var element = document.getElementById(reply.mId);
		var element = document.querySelector('div#' + reply.mId);
		console.log('showFiles: lookup for id=' + reply.mId);
		if (element)
		{
			console.log('showFiles: found');
			if (element.classList.contains('groupHidden'))
			{
				console.log('showFiles: id hidden');
				element.classList.remove('groupHidden');
				element.classList.add('groupVisible');
			}
			else
			{
				console.log('showFiles: id visible');
				element.classList.remove('groupVisible');
				element.classList.add('groupHidden');
			}
		}

	}
})