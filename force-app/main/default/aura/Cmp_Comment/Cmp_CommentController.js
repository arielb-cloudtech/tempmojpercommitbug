({
    doInit: function(cmp, event, helper) {
        // var tempComm = cmp.get("v.comment");
        // cmp.set("v.Temp", JSON.stringify(tempComm));
        var r = cmp.get("v.comment.mCommentReplys");        
        if (r.length == 0) {
            cmp.set("v.mojMessage", $A.get("$Label.c.Your_comment_was_successful"));
            cmp.set("v.bReplyList", false);
        } else {
            cmp.set("v.mojMessage", $A.get("$Label.c.The_office_responded_to_you"));
            cmp.set("v.bReplyList", true);
        }
    },

    showText: function(component, event, helper) {
        var comment = JSON.parse(JSON.stringify(component.get("v.comment")));
        var o = $A.get("$Label.c.Expand");
        var c = $A.get("$Label.c.shutter");
        var f = component.get("v.formId");
        var element = document.getElementById('textPart_1' + comment.mId + f);
        var elFile = document.getElementById('fileDiv' + comment.mId + f);
        
        if (element.classList.contains('slds-truncate')) {
            element.classList.remove('slds-truncate');
            element.classList.add('preStyle');
            component.set('v.btnText', c);

        } else {
            element.classList.add('slds-truncate');
            element.classList.remove('preStyle');
            component.set('v.btnText', o);

        }
    },

    showReply: function(cmp, event, helper) {
        var r = event.getParam("bRep");
        cmp.set('v.bReply', r);
        var l = cmp.get("v.comment.mCommentReplys");
        var bComment = true;
        if (cmp.get("v.bReply") && l.length == 0)
            bComment = false;
        cmp.set("v.bCommentVisible", bComment);
    },

    Go2Lawitem: function(cmp, event, helper) {       
        window.location.assign("/s/law-item/" + cmp.get("v.comment.mLawItemId"));
    }
})