({
	doInit: function (cmp, event, helper) { 
		console.log('doInit statusValue');   
		var action = cmp.get("c.getStatusList");		
		var inputsel = cmp.find("commentStatus");
		var opts = [];
		console.log('doInit statusValue 5'); 
		
		action.setCallback(this, function(a) {
			var myStatus = JSON.parse(JSON.stringify(a.getReturnValue()));
			for (var key in myStatus) {
				if (myStatus[key] == cmp.get("v.statusValue"))
			 	 	  opts.push({"class": "optionClass", "value": myStatus[key], "label": key, selected: "true"});
				else
			  	  	opts.push({"class": "optionClass", "value": myStatus[key], "label": key});
			}
			
             inputsel.set("v.options", opts);
             cmp.set('v.commentStatus', opts);            
		});
		$A.enqueueAction(action);  
	},

	getValue: function(cmp, event, helper){
		var fieldValue = cmp.find("commentStatus");        
		var inputValue = fieldValue.get("v.value");
		if (inputValue != 'Published') {
			cmp.set('v.isDisp', false);
		}
		cmp.set("v.statusValue",inputValue);		
        var vx = cmp.get("v.method");
        //fire event from child and capture in parent
        $A.enqueueAction(vx);
	}
})