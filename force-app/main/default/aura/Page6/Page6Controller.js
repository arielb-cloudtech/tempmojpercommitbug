({
	doInit : function(cmp, evt, helper) {
		var lawItem = cmp.get("v.newLawItem");
		if ( lawItem.Main_File_URL__c ) {
			cmp.set("v.mainFileId", lawItem.Main_File_URL__c.substring(1));
		}
	},

	updateVarsFromP21 : function(cmp, evt, helper) {
		var params = evt.getParams();
		cmp.set("v.draftFix", params.fixValue);
		cmp.set("v.laws", params.authValues);
	},

	updateVarsFromP41 : function(cmp, evt, helper) {
		var params = evt.getParams();
		cmp.set("v.laws", params.authValues);
	},

	updateVarsFromP3 : function(cmp, evt, helper) {
		console.log("~~~ Page6 cmp function: updateVarsFromP3 ~~~");
		var lawItem = cmp.get("v.newLawItem");
		var pubDate = new Date(lawItem.Publish_Date__c);
		var lastDate = new Date(lawItem.Last_Date_for_Comments__c);
		cmp.set("v.pubDate", pubDate.getDate() + "/" + (parseInt(pubDate.getMonth())+1).toString() + "/" + pubDate.getFullYear());
		cmp.set("v.lastDate", lastDate.getDate()-1 + "/" + (parseInt(lastDate.getMonth())+1).toString() + "/" + lastDate.getFullYear());
		lawItem.Document_Brief__c = lawItem.Document_Brief__c;
		cmp.set("v.newLawItem", lawItem);
		cmp.set("v.showSpinner", "false");
	}
})