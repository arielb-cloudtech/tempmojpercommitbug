({
    init: function(cmp, evt, helper) {
        var mainPageHeaderTexts = $A.get("$Label.c.Main_Page_Text").split(/[\r\n]/g);
        mainPageHeaderTexts = mainPageHeaderTexts.filter(v=>v!="")
        cmp.set("v.mainPageHeaderTexts", mainPageHeaderTexts);
        var action = cmp.get("c.getLawItems");
        action.setParams({
            StartRec:0,
            NumberOfResultToFetch: 10
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('ariel', response.getReturnValue().items);
                cmp.set('v.LawItems', response.getReturnValue().items);
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        cmp.set('v.guiMsg', errors[0].message);
                        cmp.set('v.guiMsgType', 'slds-notify slds-notify_alert slds-theme_alert-texture slds-theme_error');
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    Move2PersonalDashy: function(cmp, event, helper) {
        window.parent.location = "/s/personaldashboard";

    },
    
    toggleSubMenu: function(cmp, event, helper) {
        document.getElementById('crembo').classList.toggle("slds-is-open");
        document.getElementById('combobox-id-3').classList.toggle("slds-has-focus");
        document.getElementById('combobox-id-3').value = '';


    },

    changeSubMenu: function(cmp, event, helper) {
        var selectedOption = event.currentTarget.getAttribute('title');
        document.getElementById('crembo').classList.toggle("slds-is-open");
        document.getElementById('combobox-id-3').classList.toggle("slds-has-focus");

        document.getElementById('combobox-id-3').value = selectedOption;
    },

    searchLawItems: function(cmp, event, helper) {
       
        var currentCards = cmp.get('v.LawItems');

        var action = cmp.get("c.getLawItems");
        action.setParams({
            StartRec:currentCards.length,
            NumberOfResultToFetch:2
        });
        var el = cmp.find('lawItmes');
        action.setCallback( this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                currentCards = currentCards.concat(response.getReturnValue().items);
                cmp.set('v.LawItems', currentCards);  
            } 
        });
        $A.enqueueAction(action);
        setTimeout(function(){
            cmp.find("loadMoreButton").getElement().blur();
            var childNodes = cmp.find('lawItmes').getElement().childNodes;
            var aTag = childNodes[childNodes.length-3].querySelector('a');
            aTag.focus();
        },1000);
    },
    openMenu: function(cmp, event, helper) {
        var isOpen = cmp.get('v.isMenuOpen');
        if (isOpen == 'block') {
            isOpen = 'none';
            cmp.set('v.Modal', '');
        } else {
            cmp.set('v.Modal', 'slds-backdrop slds-backdrop_open');
            isOpen = 'block';
        }
        cmp.set('v.isMenuOpen', isOpen);
    },
    itemsChange: function(cmp, event, helper) {
        var currentCards = cmp.get('v.LawItems');
        var currentCardCount = currentCards.length;
        if (currentCardCount == 0) {
            cmp.set('v.CantLoadMore', 'true');
        } else if (currentCardCount > 0 && currentCardCount <= 3) {
            cmp.set('v.CantLoadMore', 'false');
        }
    },
    getUserFromChild: function(cmp, event, helper) {
        var objChild = cmp.find('compB');
        cmp.set("v.userId", objChild.get('v.SessionUser.Id'));
    }


})