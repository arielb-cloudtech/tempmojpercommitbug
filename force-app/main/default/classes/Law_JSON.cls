public with sharing class Law_JSON {
	public Integer LawID;	//2001673
	//public String TypeDesc;	//תיקון טעות
	public String SubTypeDesc;	//תיקון לפי ס' 10א לפשס"מ – אחרי פרסום החוק
	public String Name;	//תיקון טעות בחוק יישום ההסכם בדבר רצועת עזה ואזור יריחו (סמכויות שיפוט והוראות אחרות) (תיקוני חקיקה), התשנ"ה-1994
	public String PublicationDate;
	
	/* public static Law_JSON parse(String json){
		return (Law_JSON) System.JSON.deserialize(json, Law_JSON.class);
	} */
}