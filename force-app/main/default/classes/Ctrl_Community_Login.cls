public  with sharing class Ctrl_Community_Login {

    
        
    public Ctrl_Community_Login() {
  
    }
    @AuraEnabled
    public static List<User> getUser(String userId){
        List<User> u = [SELECT Id, firstName, lastName  FROM user where id = :userId LIMIT 1];
        System.debug('~~~ in getUser method' + u);
        return u;
    }
    
    @AuraEnabled
    public static String LoginUser(String username,String password,String Url){        
        if(Url == null || String.isEmpty(Url)){
            Url = 'platform-ministry-of-justice-sb.cs101.force.com';
            // Url = 'https://partial-moj.cs100.force.com';
        }      
    
        PageReference ref;
        ref = Site.login(username,password,Url);        
        if(ref != null){
            aura.redirect(ref);
            return UserInfo.getUserId() ;
         }else{
            return 'nay';
        }
        
     }
     

    @AuraEnabled
    public static Boolean LostPassword(String username){        
        List<User> RecUser = [SELECT Id,Username FROM User WHERE Username=:username LIMIT 1];
        if(RecUser.isEmpty()){
            return false;
        }
        Boolean rec = false;
        rec = Site.forgotPassword(username);
        return rec;
     }
}