@isTest
public with sharing class Test_DailyMailing {

    // public class PeriiodicMailingMock implements HttpCalloutMock {
	// 	public HTTPResponse respond(HTTPRequest req) {
	// 		HTTPResponse res = new HTTPResponse();
	// 		res.setStatusCode(200);
	// 		if (req.getEndpoint().containsIgnoreCase('api/login/version/4') || req.getEndpoint().containsIgnoreCase('https://pi.pardot.com/api/login/version/4')){
	// 			system.debug('get autjhentication from pardot ');
	// 			res.setBody('<api_key>asdd1651315</api_key>');
	// 		} else {
	// 			res.setBody('');
	// 		}

	// 		return res;

	// 	}
	// }

    @isTest(seeAllData=true)
    public static void testDailyScheduler(){

        Test.setMock(HttpCalloutMock.class, new Test_LawItemTriggerHandler.Mock());

		Test.StartTest();

		// String hour = String.valueOf(Datetime.now().hour());
		// String min = String.valueOf(Datetime.now().minute()); 
		// String ss = String.valueOf(Datetime.now().second());
		// String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';

		// Id jobId = System.schedule('Daily Summary', nextFireTime, new Sch_EmailUpdateDaily() );
		//Database.executeBatch(new Batch_SendPeriodicalyUpdateEmails('1','Hebrew'), 200);
		// Database.executeBatch(new Batch_SendPeriodicalyUpdateEmails('1','Arabic'), 200);

		Id jobId = System.schedule('Daily Summary', '0 0 15 ? * SUN,MON,TUE,WED,THU,FRI *', new Sch_EmailUpdateDaily() );

		Test.stopTest();
    }
}
