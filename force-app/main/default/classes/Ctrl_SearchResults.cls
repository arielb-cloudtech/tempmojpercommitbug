public without sharing class Ctrl_SearchResults 
{
    
    static final Integer ITEMS_PER_PAGE = 10;

    /** 
        Return csv string for exporting search result page

        returns CSV string of search results
    **/
    @AuraEnabled
    public static string getCSVSearchResults (
                                        String search, 
                                        Boolean RIA_included,
                                        String since, 
                                        String until, 
                                        String sortBy,
                                        String sortOrder, 
                                        List<String> lawTypes, 
                                        List<String> commentsStatus, 
                                        List<String> classifications, 
                                        List<String> relatedLaws, 
                                        List<String> offices ) {
    
        
        if ( null == search ){ search = ''; }
        if ( null == RIA_included ){ RIA_included = false; }        
        Integer page = -1; // no pagination

        String query = getSearchQuery( search, RIA_included, since, until, page, sortBy, sortOrder,
                                         new Set<String>(lawTypes),
                                         new Set<String>(commentsStatus),
                                         new Set<String>(classifications),
                                         new Set<String>(relatedLaws),
                                         new Set<String>(offices) );

        List<Law_Item__c> lawItems = Database.query(query);

        String response = '';
        /** CSV TITLE **/
        response+= Label.csv_title_documentType + ',';
        response+= Label.csv_title_Title + ',';
        response+= Label.csv_title_distributionDate + ',';
        response+= Label.csv_title_ministry + ',';
        response+= Label.csv_title_topics + ',';
        response+= Label.csv_title_ria + ',';
        response+= Label.csv_title_openForComments + ',';
        response+= '\n';
        /** CSV ROWS **/
        for (Law_Item__c item : lawItems) {
            ReturnObjects.ItemCard law = new ReturnObjects.ItemCard(item);
            response+= law.DocumentReadableType+',';
            response+= '"'+law.Title+'",';
            response+= law.FixedCreatedDate+',';
            response+= law.PublisherName+',';
            response+= '"'+String.join( law.Classifications, ',')+'",';
            response+= (law.IsOpenForComments? Label.yes : Label.no)+',';
            response+= law.IsOpenForComments? Label.yes : Label.no;
            response+= '\n';
        }
        return response;

    }

    public static Map<Id, Integer> getCommentCount(Set<Id> relatedLawItemsId ){
		List<AggregateResult> counterAggregation =  [ 
			SELECT Related_to_Law_Item__c, count(Id) 
			FROM Comment__c 
			WHERE Related_to_Law_Item__c IN :relatedLawItemsId
				AND Sub_Comment__c = null 
				AND Is_Private_Comment__c = false 
				AND Display_Comment__c = true 
				AND Comment_Status__c != 'Draft' 
			GROUP BY Related_to_Law_Item__c 
		];
		Map<Id, Integer> countByLawIds = new Map<Id, Integer>();
		for (AggregateResult countAggregation : counterAggregation)  {
			countByLawIds.put((String)countAggregation.get('Related_to_Law_Item__c'), (Integer)countAggregation.get('expr0'));
		}
		return countByLawIds;
	}

    @AuraEnabled
    public static searchResponse getLawItems(
                                        String search, 
                                        Boolean RIA_included,
                                        String since, 
                                        String until, 
                                        String sortBy,
                                        String sortOrder,
                                        Integer page, 
                                        List<String> lawTypes, 
                                        List<String> commentsStatus, 
                                        List<String> classifications, 
                                        List<String> relatedLaws, 
                                        List<String> offices ) {

        searchResponse response = new searchResponse();
        
        if ( null == search ){ search = ''; }
        if ( null == RIA_included ){ RIA_included = false; }        
        if ( page == null ){ page = 1; }

        String query = getSearchQuery( search, RIA_included, since, until, page, sortBy, sortOrder,
                                         new Set<String>(lawTypes),
                                         new Set<String>(commentsStatus),
                                         new Set<String>(classifications),
                                         new Set<String>(relatedLaws),
                                         new Set<String>(offices) );

        List<String> filters = getBasicQueryFilters(search);

        List<String> allFilters = getQueryFilters(search, RIA_included, since, until,new Set<String>(lawTypes), new Set<String>(commentsStatus), new Set<String>(classifications), new Set<String>(relatedLaws), new Set<String>(offices));
        List<String> counterFilters = getQueryFilters(search, RIA_included, since, until,new Set<String>(), new Set<String>(commentsStatus), new Set<String>(classifications), new Set<String>(relatedLaws), new Set<String>(offices));

        Integer total = database.countQuery('select count() From Law_Item__c Where '+String.join( allFilters , ' AND '));
    
        response.counters.put('total',  total);
       
        List<Schema.RecordTypeInfo> lawItemRecordTypes = Law_Item__c.SObjectType.getDescribe().getRecordTypeInfos();
        for(Schema.RecordTypeInfo lawItemRecordType: lawItemRecordTypes) {
            Id recordTypeId = lawItemRecordType.getRecordTypeId();
            if( !lawItemRecordType.isMaster() ) {
                response.counters.put(recordTypeId,  database.countQuery('select count() From Law_Item__c Where RecordType.Id = :recordTypeId AND '+String.join( counterFilters , ' AND ')));
            }
        }
        
        response.pager.put('page', page);
        response.pager.put('of', (Integer)(Decimal.valueOf( total ).divide( ITEMS_PER_PAGE, 1)).round(System.RoundingMode.CEILING));

        List<Law_Item__c> liLst = Database.query(query);

        response.laws = loadCommentsAndFiles(liLst);
        response.setSuccessStatus();
        response.message = JSON.serialize(allFilters);
        return response;
    }

    @AuraEnabled
    public static List<ReturnObjects.ItemCard> loadCommentsAndFiles(List<Law_Item__c> LawItems)
    {
        
        List<ReturnObjects.ItemCard> CardList = new List<ReturnObjects.ItemCard>(); 
        try
        {
        
            AggregateResult[] LawItemsCom2 = [  
                                                SELECT Related_to_Law_Item__r.Id, COUNT(Related_to_Law_Item__c) 
                                                FROM Comment__c 
                                                WHERE Related_to_Law_Item__r.Id in:LawItems AND Display_Comment__c = True AND  Comment_Status__c = 'Published' 
                                                GROUP BY Related_to_Law_Item__r.Id
                                            ];
            
            Set<Id> lawItemIds = new Set<Id>((new Map<Id, Law_Item__c>(LawItems)).keySet());

            Map<Id, ContentDocumentLink> cdlList = new Map<Id, ContentDocumentLink>([
                                                SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId,
                                                ContentDocument.LatestPublishedVersion.Title,ContentDocument.LatestPublishedVersion.File_Type__c,LinkedEntityId,
                                                ContentDocument.FileExtension
                                                FROM ContentDocumentLink 
                                                WHERE LinkedEntityId IN: lawItemIds AND ContentDocument.LatestPublishedVersion.File_Type__c = 'Main File']);
            
            
            
            Map<Id, Integer> commentCount = Ctrl_SearchResults.getCommentCount(lawItemIds);

            String debug = '';
            for (Law_Item__c item : LawItems) 
            {
                ReturnObjects.ItemCard yay = new ReturnObjects.ItemCard(item, commentCount);
            
                for (Id key : cdlList.KeySet()) {
                    ContentDocumentLink Cdl =  cdlList.get(key);
                    if(Cdl.LinkedEntityId == item.Id){
                        yay.File = Cdl.ContentDocument.LatestPublishedVersionId;
                        yay.FileType = Cdl.ContentDocument.FileExtension;
                        yay.HasFile = true;
                        break;
                    }
                }
                CardList.add(yay);
            }
        } catch(Exception ex) {
            System.debug('!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber());
        }
        return  CardList;
    }

    private static List<String> getQueryFilters(
                            String searchVar, 
                            Boolean RIA_included,
                            String since, 
                            String until, 
                            Set<String> lawTypes, 
                            Set<String> commentsStatus, 
                            Set<String> classifications, 
                            Set<String> relatedLaws, 
                            Set<String> offices) {
        
        List<String> filters = getBasicQueryFilters(searchVar);

         // Law Types
        if( lawTypes.size() > 0 ) {
            filters.add('RecordType.Id IN :lawTypes');
        }

        // Comment Statuas
        if( commentsStatus.size() == 1) {
            if(commentsStatus.contains('Open')) {
                filters.add('Last_Date_for_Comments__c > ' + String.valueOf(Date.today()) + 'T' + '00:00:00.000Z');
            }
            else {
                filters.add('Last_Date_for_Comments__c < ' + String.valueOf(Date.today()) + 'T' + '00:00:00.000Z');
            }
        }

        if( since != null ) {
            filters.add('Publish_Date__c >= ' + since + 'T' + '00:00:00.000Z');
        }
        if( until != null ) {
            filters.add('Publish_Date__c <= ' + until + 'T' + '00:00:00.000Z');
        }

        if( classifications.size() > 0 ) {
            filters.add('Id IN (SELECT Law_Item__c FROM Law_Item_Classification__c WHERE Classification__c IN :classifications)');
        }

        if( relatedLaws.size() > 0 ) {
            filters.add('Id IN (SELECT Law_Item__c FROM Related_to_Law__c WHERE Law__c IN :relatedLaws)');
        }

        if( offices.size() > 0 ) {
            filters.add('Publisher_Account__c IN :offices');
        }

        if( RIA_included ) {
            filters.add('RIA_File_Name__c != NULL');
        }
        return filters;
    }

    private static List<String> getBasicQueryFilters(String searchVar){
        return new List<String> {
                'Status__c = \'Distributed\'',
                'Name != \'placeholder\'',
                'Law_Item_Name__c != null',
                '( Law_Item_Name__c Like \'%' + searchVar + '%\' OR Document_Brief_Short_1__c Like \'%' + searchVar + '%\' OR Document_Brief_Short_2__c Like \'%' + searchVar + '%\')'
            };
    }

    public static String getSearchQuery(
                            String searchVar,
                            Boolean RIA_included, 
                            String since, 
                            String until, 
                            Integer page,
                            String sortBy,
                            String sortOrder,
                            Set<String> lawTypes, 
                            Set<String> commentsStatus, 
                            Set<String> classifications, 
                            Set<String> relatedLaws, 
                            Set<String> offices)
        //FiltersWrapper filters, String searchVar, String searchType)
    {
        try
        {
            List<String> queryFields = new List<String> {
                'Id',
                'CreatedDate',
                'Publisher_Account_Additional__c',
                'Publisher_Account__c',
                'Law__c',
                'Document_Brief__c',
                'Law_Item_Name__c',
                'Status__c',
                'Publisher_Account__r.Name',
                'Last_Date_for_Comments__c',
                'Publish_Date__c',
                'Type__c',
                'RIA_File_Name__c',
                'RecordTypeId', 
                '(SELECT Classification_Name__c FROM Law_Item_Classifications__r)'
            };

            List<String> queryWhere = getQueryFilters( searchVar, RIA_included, since, until, lawTypes, commentsStatus, classifications, relatedLaws, offices);

            String orderBy = '';

            switch on sortBy {
                when  'title' {
                    orderBy = 'Name';
                }
                when else {
                     orderBy = 'Publish_Date__c';
                }
            }

            if ( sortOrder == 'DESC' ) {
                orderBy += ' DESC';
            }
            else {
                orderBy += ' ASC';
            }

            String query = 'Select '+ String.join( queryFields , ', ')+' From Law_Item__c Where '+String.join( queryWhere , ' AND ');
            query+= ' ORDER BY '+orderBy;
            if ( page > -1 ) {
                query+= ' LIMIT '+ITEMS_PER_PAGE;
                if ( page != null && page>1 ) {
                    query+= ' OFFSET '+ ( (page-1) * ITEMS_PER_PAGE);
                }
            }
            return query;

        }catch(Exception ex)
        {
            System.debug('!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber());
            return '!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber();
        }
        
    }

    @AuraEnabled
    public static String getSubs(String subSearch)
    {
        try
        {
            List<Classification__c> clsLst = [Select Id, Name From Classification__c];
            List<Classification__c> clsLsttoSend = new List<Classification__c>();
            for(Classification__c cls : clsLst)
            {
                if(cls.Name.contains(subSearch))
                {
                    clsLsttoSend.add(cls);
                }
            }
            System.debug('!!!clsLsttoSend: ' + clsLsttoSend);
            clsLsttoSend.sort();
            return JSON.serialize(clsLsttoSend);
        }catch(Exception ex)
        {
            System.debug('!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber());
            return '!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber();
        }
        
    }

    @AuraEnabled
	public static String getLawsForExport(List<String> lawItmIds)
	{
		List<Law_Item__c> lawItms = [Select Id, Law_Item_Name__c, Publish_Date__c, Document_Brief__c, Last_Date_for_Comments__c, RecordType.Name,
									Publisher_Account__r.Name, Legal_Counsel_Name__r.FirstName, Legal_Counsel_Name__r.LastName, 
									(Select Id, Classification__r.Name From Law_Item_Classifications__r),
									(Select ContentDocument.LatestPublishedVersionId From ContentDocumentLinks) 
									From Law_Item__c Where Id In: lawItmIds];
		return JSON.serialize(lawItms);
	}

    @AuraEnabled
    public static String getLaws(String subSearch, String subName)
    {
        try
        {
            //String rtId = Schema.SObjectType.Law__c.getRecordTypeInfosByName().get('Parent Law').getRecordTypeId();
           Map<Id, RecordType> rtIdMap = new Map<Id, RecordType>([Select Id From RecordType Where SObjectType =: 'Law__c' And (DeveloperName =: 'Parent_Law' Or DeveloperName =: 'Law')]);
            List<Law__c> clsLst = new List<Law__c>();
            subName = '%' + subName + '%';
            subSearch = '%' + subSearch + '%';
            if(subName != null && !String.isBlank(subName))
            {
                clsLst = [Select Id, Law_Name__c, Name,
                                                (Select Id, ClassificiationID__r.Name From LawClassifications__r Where ClassificiationID__r.Name =: subName Limit 200)
                                                From Law__c Where RecordTypeId In: rtIdMap.keySet() And Classifications__c Like: subName And Law_Name__c Like: subSearch];

                //for(Law__c l : clsLstunFiltered)
                //{
                //    if(l.LawClassifications__r.size() > 0)
                //    {
                //        clsLst.add(l);
                //    }
                //}

            }else
            {
                clsLst = [Select Id, Law_Name__c, Name, (Select Id, ClassificiationID__r.Name From LawClassifications__r Limit 200) From Law__c Where RecordTypeId In: rtIdMap.keySet() And Law_Name__c Like: subSearch];
            }
                                    
            List<lawContainer> clsLsttoSend = new List<lawContainer>();
            for(Law__c cls : clsLst)
            {
                //if(cls.Name.contains(subSearch))
                //{
                //}
                lawContainer lc = new lawContainer();
                lc.Name = cls.Law_Name__c;
                lc.Id = cls.Id;
                clsLsttoSend.add(lc);
            }
            System.debug('!!!clsLsttoSend: ' + clsLsttoSend);
            //clsLsttoSend.sort();
            return JSON.serialize(clsLsttoSend);
        }catch(Exception ex)
        {
            System.debug('!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber());
            return '!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber();
        }
        
    }

    @AuraEnabled
    public static String getSecLaws(String subSearch)
    {
        try
        {
            //String rtId = Schema.SObjectType.Law__c.getRecordTypeInfosByName().get('Parent Law').getRecordTypeId();
            Map<Id, RecordType> rtIdMap = new Map<Id, RecordType>([Select Id From RecordType Where SObjectType =: 'Law__c' And (DeveloperName =: 'Secondary_Law_Mother')]);
            List<Law__c> clsLst = new List<Law__c>();
            subSearch = '%' + subSearch + '%';
            
             clsLst = [Select Id, Name, Law_Name__c, (Select Id, ClassificiationID__r.Name From LawClassifications__r Limit 10) From Law__c Where Law_Name__c Like: subSearch And RecordTypeId In: rtIdMap.keySet() Limit 50];
                                    
            List<lawContainer> clsLsttoSend = new List<lawContainer>();
            for(Law__c cls : clsLst)
            {
                lawContainer lc = new lawContainer();
                lc.Name = cls.Law_Name__c;
                lc.Id = cls.Id;
                clsLsttoSend.add(lc);
            }
            System.debug('!!!clsLsttoSend: ' + clsLsttoSend);
            //clsLsttoSend.sort();
            return JSON.serialize(clsLsttoSend);
        }catch(Exception ex)
        {
            System.debug('!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber());
            return '!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber();
        }
        
    }

    @AuraEnabled
    public static String getData(String SearchValue, String searchType)
    {
        try
        {
        	List<Law_Item__c> LawItems = new List<Law_Item__c>();
        	if(searchType != null && searchType == '1')
        	{
	            LawItems = [SELECT CreatedDate, Id,Document_Brief__c,Law_Item_Name__c,Status__c,Publisher_Account__r.Name,Last_Date_for_Comments__c,Publish_Date__c,Type__c,RecordTypeId FROM Law_Item__c WHERE Law_Item_Name__c Like '% :SearchValue %' And Publish_Date__c != null Order By Publish_Date__c Desc];
        	}
        	else if(searchType != null && searchType == '2')
        	{
	            LawItems = [Select CreatedDate, Id,Document_Brief__c,Law_Item_Name__c,Status__c,Publisher_Account__r.Name,Last_Date_for_Comments__c,Publish_Date__c,Type__c,
	            			RecordTypeId From Law_Item__c 
	            			Where Law_Item_Name__c Like: SearchValue
	            			Or Document_Brief_Short_1__c Like: SearchValue
	            			Or Document_Brief_Short_2__c Like: SearchValue Order By Publish_Date__c Desc];
        	}
            if(String.isBlank(SearchValue))
            {
                LawItems = [SELECT CreatedDate, Id,Document_Brief__c,Law_Item_Name__c,Status__c,Publisher_Account__r.Name,Last_Date_for_Comments__c,Publish_Date__c,Type__c,RecordTypeId FROM Law_Item__c Where Publish_Date__c != null Order By Publish_Date__c Desc];
            }
            AggregateResult[] LawItemsCom2 = [SELECT Related_to_Law_Item__r.Id, COUNT(Related_to_Law_Item__c) FROM Comment__c WHERE Display_Comment__c = true And Related_to_Law_Item__r.Id in:LawItems GROUP BY Related_to_Law_Item__r.Id];
            
            Set<Id> lawItemIds = new Set<Id>((new Map<Id, Law_Item__c>(lawItems)).keySet());
            Map<Id, Integer> commentCount = Ctrl_SearchResults.getCommentCount(lawItemIds);

            if(lawItemIds != null && !lawItemIds.isEmpty())
            {
                Map<Id,ContentDocumentLink> cdlList = new Map<Id,ContentDocumentLink>([SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId,
                                                    ContentDocument.LatestPublishedVersion.Title,LinkedEntityId,ContentDocument.FileExtension
                                                FROM ContentDocumentLink WHERE LinkedEntityId IN: lawItemIds]);
                
                List<ReturnObjects.ItemCard> CardList = new List<ReturnObjects.ItemCard>(); 
               
                for (Law_Item__c item : LawItems) 
                {
                    ReturnObjects.ItemCard yay = new ReturnObjects.ItemCard(item, commentCount);
                    
                    for (Id key : cdlList.KeySet()) {
                      ContentDocumentLink Cdl =  cdlList.get(key);
                      if(Cdl.LinkedEntityId == item.Id){
                        yay.File = Cdl.ContentDocument.LatestPublishedVersionId;
                        yay.FileType = Cdl.ContentDocument.FileExtension;
                        yay.HasFile = true;
                        break;
                      }
                    }
                    CardList.add(yay);
                }
                return  json.serialize(CardList);
            }else
            {
                return JSON.serialize(new List<ReturnObjects.ItemCard>());
            }
            
        }catch(Exception ex)
        {
            System.debug('!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber());
            return '!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber();
        }
        
    }
    
    /**
    Return list of laws for related laws filter 
    **/
    @AuraEnabled
    public static relatedLawsResponse getRelatedLawsItems (String search, List<String> excludeIds, List<String> lawClassifaction) {

        Map<Id, RecordType> lawRecordTypes = new Map<Id, RecordType>([Select Id From RecordType Where SObjectType =: 'Law__c' And (DeveloperName =: 'Parent_Law' Or DeveloperName =: 'Law')]);
        Set<Id> lawRecordTypeIds = lawRecordTypes.keySet();
        relatedLawsResponse response = new relatedLawsResponse();
        
        String searchLike = '%'+search+'%';
        
        string query = 'SELECT Id, Name FROM Law__c WHERE RecordTypeId IN :lawRecordTypeIds And Name LIKE :searchLike ';
        if( null != lawClassifaction && !lawClassifaction.isEmpty() ) {
            query+='AND Id IN ( SELECT IsraelLawID__c FROM LawClassification__c WHERE ClassificiationID__c IN :lawClassifaction ) ';
        }
        if( null != excludeIds && !excludeIds.isEmpty() ) {
            query+='AND ID NOT IN :excludeIds ';
        }
         query+='LIMIT 10';
        response.message = query;
        List<Law__c> laws = Database.query(query);

        for (Law__c item : laws) {
            response.relatedLaws.add(new filterSelectItem(item.Id, item.Name));
        }

        response.setSuccessStatus();

        return response;
    }


    @AuraEnabled
    public static filtersResponse getFilters( List<Id> relatedLaws )
    {
        filtersResponse response = new filtersResponse();
        // Law types
        List<Schema.RecordTypeInfo> lawItemRecordTypes = Law_Item__c.SObjectType.getDescribe().getRecordTypeInfos();
        for(Schema.RecordTypeInfo lawItemRecordType: lawItemRecordTypes) {
            if( !lawItemRecordType.isMaster() ) {
                response.lawTypes.add(new filterSelectItem(lawItemRecordType.getRecordTypeId(), lawItemRecordType.getName()));
            }
        }

        // Offices
        List<Account> offLst1 = [Select Id, Name From Account Where IsUnit__c = true And IsActive__c = true];
        for(Account off : offLst1)
        {
            response.offices.add(new filterSelectItem(off.Id, off.Name));
        }

        // classification
        List<Classification__c> classifications = [Select Id, Name From Classification__c];
        for(Classification__c cls : classifications)
        {
            response.classifications.add(new filterSelectItem(cls.Id, cls.Name));
        }

        // related_law_topics
        List<LawClassification__c> lawClassifications = [Select Id,  Classification_Name__c From LawClassification__c];
        for(LawClassification__c lawClassification : lawClassifications)
        {
            response.related_law_topics.add(new filterSelectItem(lawClassification.Id, lawClassification.Classification_Name__c));
        }

        List<Law__c> relatedLawsNamed = [Select Id,  Name From Law__c WHERE Id IN :relatedLaws];
        for( Law__c relatedLawNamed : relatedLawsNamed ) {
            response.related_laws.add(new filterSelectItem(relatedLawNamed.Id, relatedLawNamed.Name));
        }

        // Ministory Prefix filter 
        response.ministerIngnoredPrefix = Label.Search_ministory_pres.split(',');

        response.setSuccessStatus();
        return response;
    }

    public class searchResponse extends ReturnObjects.AjaxMessage {
         @AuraEnabled public List<ReturnObjects.ItemCard> laws = new  List<ReturnObjects.ItemCard>();
         @AuraEnabled public Map<String, Integer > counters = new  Map<String, Integer >();
         @AuraEnabled public Map<String, Integer > pager = new  Map<String, Integer >();
    }

    public class relatedLawsResponse extends ReturnObjects.AjaxMessage {
        @AuraEnabled public List<filterSelectItem> relatedLaws = new List<filterSelectItem>();
    }

    public class filtersResponse extends ReturnObjects.AjaxMessage {
        @AuraEnabled public List<filterSelectItem> offices = new List<filterSelectItem>();
        @AuraEnabled public List<filterSelectItem> classifications = new List<filterSelectItem>();
        @AuraEnabled public List<filterSelectItem> lawTypes = new List<filterSelectItem>();
        @AuraEnabled public List<filterSelectItem> related_law_topics = new List<filterSelectItem>();
        @AuraEnabled public List<filterSelectItem> related_laws = new List<filterSelectItem>();

        @AuraEnabled public List<String> ministerIngnoredPrefix = new List<String>();
    }

    public class filterSelectItem {
        @AuraEnabled public String Id;
        @AuraEnabled public String Name;

        public filterSelectItem(String Id, String Name){
            this.Id = Id;
            this.Name = Name;
        }
    }


    @AuraEnabled
    public static String getOffices()
    {
        try
        {
            List<Account> offLst1 = [Select Id, Name From Account Where IsUnit__c = true And IsActive__c = true];

            List<Account> offLst = new List<Account>();
            for(Account off : offLst1)
            {
                System.debug('!!!off: ' + off);
                System.debug('!!!condition: ' + off.Name.contains('משרד'));
                if(off.Name.contains('משרד'))
                {
                    offLst.add(off);
                }
            }
            Map<String, List<Account>> offMap = new Map<String, List<Account>>();
            Map<String, String> letterMap = new Map<String, String>();

            List<String> abc = new List<String>();
            abc.add('A');
            abc.add('B');
            abc.add('C');
            abc.add('D');
            abc.add('E');
            abc.add('F');
            abc.add('G');
            abc.add('H');
            abc.add('I');
            abc.add('J');
            abc.add('K');
            abc.add('L');
            abc.add('M');
            abc.add('N');
            abc.add('O');
            abc.add('P');
            abc.add('Q');
            abc.add('R');
            abc.add('S');
            abc.add('T');
            abc.add('U');
            abc.add('V');

            List<String> hebabc = new List<String>();
            hebabc.add('א');
            hebabc.add('ב');
            hebabc.add('ג');
            hebabc.add('ד');
            hebabc.add('ה');
            hebabc.add('ו');
            hebabc.add('ז');
            hebabc.add('ח');
            hebabc.add('ט');
            hebabc.add('י');
            hebabc.add('כ');
            hebabc.add('ל');
            hebabc.add('מ');
            hebabc.add('נ');
            hebabc.add('ס');
            hebabc.add('ע');
            hebabc.add('פ');
            hebabc.add('צ');
            hebabc.add('ק');
            hebabc.add('ר');
            hebabc.add('ש');
            hebabc.add('ת');

            for(Integer i = 0; i < hebabc.size(); i++)
            {
                letterMap.put(hebabc[i],abc[i]);
            }

            /*for(Account off : offLst)
            {
                if(off.Name.startsWith('משרד ה'))
                {
                    String startLetter = off.Name.subString(6,off.Name.length()).subString(0,1);
                    if(!offMap.containsKey(letterMap.get(startLetter)))
                    {
                        offMap.put(letterMap.get(startLetter), new List<Account>());
                    }
                    offMap.get(letterMap.get(startLetter)).add(off);
                }else if(off.Name.startsWith('המשרד ל'))
                {
                    String startLetter = off.Name.subString(7,off.Name.length()).subString(0,1);
                    if(!offMap.containsKey(letterMap.get(startLetter)))
                    {
                        offMap.put(letterMap.get(startLetter), new List<Account>());
                    }
                    offMap.get(letterMap.get(startLetter)).add(off);
                }
            }*/
            for(Account off : offLst)
            {
                if(off.Name.startsWith('משרד ה'))
                {
                    System.debug('!!!offinIf: ' + off);
                    String startLetter = off.Name.subString(6,off.Name.length()).subString(0,1);
                    if(!offMap.containsKey(startLetter))
                    {
                        offMap.put(startLetter, new List<Account>());
                    }
                    offMap.get(startLetter).add(off);
                }else if(off.Name.startsWith('המשרד ל'))
                {
                    String startLetter = off.Name.subString(7,off.Name.length()).subString(0,1);
                    if(!offMap.containsKey(startLetter))
                    {
                        offMap.put(startLetter, new List<Account>());
                    }
                    offMap.get(startLetter).add(off);
                }
            }
            System.debug('!!!offMap: ' + offMap);
            List<String> sortedKeys = new List<String>(offMap.keySet());
            sortedKeys.sort();
            List<OfficeLetter> accLst = new List<OfficeLetter>();
            for(String key : sortedKeys)
            {
                System.debug(key);
                OfficeLetter ol = new OfficeLetter();
                ol.letter = key;
                ol.Name = key;
                for(Account acc : offMap.get(key))
                {
                    OfficeLetterAcc olAc = new OfficeLetterAcc();
                    olAc.office = acc;
                    ol.offices.add(olAc); 
                }
                accLst.add(ol);
            }
            //accLst.sort();
            System.debug('!!!accLst:' + accLst);
            return JSON.serialize(accLst);
        }catch(Exception ex)
        {
            System.debug('!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber());
            return '!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber();
        }
        
    }

    @AuraEnabled
    public static String getClassesLetter()
    {
        try
        {
            List<Classification__c> offLst = [Select Id, Name From Classification__c];
            System.debug('!!!offLst: ' + offLst);
            Map<String, List<Classification__c>> offMap = new Map<String, List<Classification__c>>();
            Map<String, String> letterMap = new Map<String, String>();

            List<String> abc = new List<String>();
            abc.add('A');
            abc.add('B');
            abc.add('C');
            abc.add('D');
            abc.add('E');
            abc.add('F');
            abc.add('G');
            abc.add('H');
            abc.add('I');
            abc.add('J');
            abc.add('K');
            abc.add('L');
            abc.add('M');
            abc.add('N');
            abc.add('O');
            abc.add('P');
            abc.add('Q');
            abc.add('R');
            abc.add('S');
            abc.add('T');
            abc.add('U');
            abc.add('V');

            List<String> hebabc = new List<String>();
            hebabc.add('א');
            hebabc.add('ב');
            hebabc.add('ג');
            hebabc.add('ד');
            hebabc.add('ה');
            hebabc.add('ו');
            hebabc.add('ז');
            hebabc.add('ח');
            hebabc.add('ט');
            hebabc.add('י');
            hebabc.add('כ');
            hebabc.add('ל');
            hebabc.add('מ');
            hebabc.add('נ');
            hebabc.add('ס');
            hebabc.add('ע');
            hebabc.add('פ');
            hebabc.add('צ');
            hebabc.add('ק');
            hebabc.add('ר');
            hebabc.add('ש');
            hebabc.add('ת');

            for(Integer i = 0; i < hebabc.size(); i++)
            {
                letterMap.put(hebabc[i],abc[i]);
            }

            for(Classification__c off : offLst)
            {
                String startLetter = off.Name.subString(0,1);
                if(!offMap.containsKey(startLetter))
                {
                    offMap.put(startLetter, new List<Classification__c>());
                }
                offMap.get(startLetter).add(off);
            }
            System.debug('!!!offMap: ' + offMap);
            List<String> sortedKeys = new List<String>(offMap.keySet());
            sortedKeys.sort();
            List<ClassLetter> accLst = new List<ClassLetter>();
            for(String key : sortedKeys)
            {
                System.debug(key);
                ClassLetter ol = new ClassLetter();
                ol.letter = key;
                ol.Name = key;
                for(Classification__c acc : offMap.get(key))
                {
                    ClassLetterAcc olAc = new ClassLetterAcc();
                    olAc.office = acc;
                    ol.offices.add(olAc); 
                }
                accLst.add(ol);
            }

            System.debug('!!!accLst:' + accLst);
            return JSON.serialize(accLst);
        }catch(Exception ex)
        {
            System.debug('!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber());
            return '!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber();
        }
    }

    public class lawContainer
    {
        @AuraEnabled public String Name;
        @AuraEnabled public String Id;
        @AuraEnabled public Boolean isSelected;

        public lawContainer()
        {
            isSelected = false;
        }
    }

    public class OfficeLetter
    {

         @AuraEnabled public String letter;
         @AuraEnabled public String Name;
         @AuraEnabled public Boolean isSelected;
         @AuraEnabled public List<OfficeLetterAcc> offices;
       
        public OfficeLetter()
        {
           offices = new List<OfficeLetterAcc>();
           isSelected = false;
        }
    }

    public class OfficeLetterAcc
    {
         @AuraEnabled public Boolean isSelected;         
         @AuraEnabled public Account office;
       
        public OfficeLetterAcc()
        {
           isSelected = false;
        }
    }

    public class ClassLetter
    {

         @AuraEnabled public String letter;
         @AuraEnabled public String Name;
         @AuraEnabled public Boolean isSelected;
         @AuraEnabled public List<ClassLetterAcc> offices;
       
        public ClassLetter()
        {
           offices = new List<ClassLetterAcc>();
           isSelected = false;
        }
    }

    public class ClassLetterAcc
    {
         @AuraEnabled public Boolean isSelected;         
         @AuraEnabled public Classification__c office;
       
        public ClassLetterAcc()
        {
           isSelected = false;
        }
    }

    public Ctrl_SearchResults() 
    {
        
    }
}