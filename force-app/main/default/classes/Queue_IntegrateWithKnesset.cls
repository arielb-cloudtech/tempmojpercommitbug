public with sharing class Queue_IntegrateWithKnesset implements Queueable, Database.allowsCallouts{
    
    public Queue_IntegrateWithKnesset() {
    }

    public void execute(QueueableContext context){
        KNS_Integration_Utils.getKnessetIsraelLaws();
        //KNS_Integration_Utils.getKnessetLaws();
        KNS_Integration_Utils.getKnessetBills();
        KNS_Integration_Utils.getKnessetLawBindings();

        KNS_Integration_Utils.upsertKnessetIsraelLaws();
        //KNS_Integration_Utils.upsertKnessetLaws();
        KNS_Integration_Utils.upsertKnessetBills();
        KNS_Integration_Utils.upsertKnessetLawBindings();
    }
}
