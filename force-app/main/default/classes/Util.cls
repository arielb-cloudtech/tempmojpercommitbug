public class Util {
		//@future
		//public static void InsertPermissionSet(SET<id> userIds) {
			//Set<String> assigneeIdSet = new Set<String>();
			//id permission_SetId;
			//for (PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'F_Secure_Cloud_Protection_User'  Limit 1]) {
			//	permission_SetId = ps.Id;
			//	for (PermissionSetAssignment psa : [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :ps.Id]) { //query for the permission set Users that are already assigned
			//		assigneeIdSet.add(psa.AssigneeId); //add the Id of each assigned User to our set
			//	}
			//}
			//if (permission_SetId == null) {
			//	system.debug('*** No Permission Set Id found by Name');
			//	return;
			//}

			//List<PermissionSetAssignment> permissionSetAssignmentList = new List<PermissionSetAssignment>();	
			//if (userIds != null || !userIds.isEmpty()){
			//	for ( Id usr : userIds ){
			//		if (permission_SetId != null && !assigneeIdSet.contains(usr) ){
			//			permissionSetAssignmentList.add( new PermissionSetAssignment (PermissionSetId = permission_SetId , AssigneeId = usr) );						
			//		}
			//		else{
			//			system.debug('*** permission Set Assignee exist ');
			//		}	
			//	}
			//	try{
			//		insert permissionSetAssignmentList;
			//	}
			//	catch(exception ex){
			//		system.debug('*** Error in insert to Permission Set Assignment: ' + ex);
			//	}				
			//}				
		//}

		@future
		public static void updateNoneActiveUsers (Set<id> noneActiveUserIds) {
			list <User> userToUpdate = new list <User>();
			for (User usr : [ select id,isActive from User where id IN : noneActiveUserIds] ){
				userToUpdate.add( new User ( id = usr.id, isActive = false  )	);
			}		
			try {
	    		update userToUpdate;
	    	}
	    	catch(exception ex){
	    		system.debug('** Error in none Active Users Update: ' + ex);
	    	}	
		}

		@future
		public static void upsertActiveUsers (string userListSerialized, Set<id> notToUpdateContactList) {
			Boolean isUpsertUsersSuccess = false;
			Map<id,User> contactUserMap = new Map<id,User>();
			contactUserMap = (Map<id,User>)JSON.deserialize(userListSerialized, Map<id,User>.class );
			try {
				//system.debug('*** 2- contactUserMap: ' + contactUserMap);
				database.upsert (contactUserMap.values()); 
				isUpsertUsersSuccess = true;
			}
			catch(exception ex){
				system.debug('*** Error in upserting users: ' + ex);
			}
			if (isUpsertUsersSuccess){
	    		List<id> userIds = new List<id>();
				List <Contact> contactToUpdateList = new List <Contact>();
				for ( Id conId : contactUserMap.keyset()){					
					System.resetPassword(contactUserMap.get(conId).id,true);
					userIds.add(contactUserMap.get(conId).id);
					if (!notToUpdateContactList.contains(conId)){
						contactToUpdateList.add(new Contact ( id = conId, user__c = contactUserMap.get(conId).id));	    								
					}
				}
				//try {
				//	if (!contactToUpdateList.isEmpty()){
				//		//update contactToUpdateList;
				//		//System.enqueueJob(new contactToUpdateQueueable(contactToUpdateList));
				//	}
				//}
				//catch(exception ex){
				//	system.debug('*** Error in updating Contact: ' + ex);
				//}
				// Add new Users to the F_Secure_Cloud_Protection_User permission set:
				//InsertPermissionSet(userIds);
				System.enqueueJob(new UserQueueable(userIds,contactToUpdateList));
	    	}	    	
		}
	}