public with sharing class FiltersWrapper 
{
	public rts rtChoices;
	public String distDateStart;
	public String distDateEnd;
	public List<classification> chosenClassifications;
	public List<Law__c> relatedLaws;
	public List<Law__c> relatedSecLaws;
	public List<Account> selectedOffices;
	public String openForComments;
	public Boolean RIAFilter;
	public String orderBy;
	public String searchVar;

	public FiltersWrapper() 
	{
		relatedLaws = new List<Law__c>();
		relatedSecLaws = new List<Law__c>();
		chosenClassifications = new List<classification>();
		selectedOffices = new List<Account>();
		rtChoices = new rts();
	}


	public class rts
	{
		public Integer numOfChoices;
		public rtChoice checkMemo;
		public rtChoice checkProt;
		public rtChoice checkSec;
		public rtChoice checkTest;

		public rts()
		{
			
		}
	}

	public class rtChoice
	{
		public String name;
		public String Id;
		public Boolean isSelected;

		public rtChoice()
		{

		}
	}

	public class classification
	{
		public String Name;
		public String Id;

		public classification()
		{

		}
	}
}