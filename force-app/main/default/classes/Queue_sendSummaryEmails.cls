public with sharing class Queue_sendSummaryEmails implements Queueable, Database.allowsCallouts{
   public String thisList;
   public Map<String,object> BodyParams;

   public Queue_sendSummaryEmails(Map<String,object> params, String pardotList) {
       thisList = pardotList;
       BodyParams = params;
   }

    public void execute(QueueableContext context){
        if(bodyParams != null && !bodyParams.isEmpty()){
            String thisApiKey = '';
            Utils.sendEmailsToLists(thisApiKey, thisList, BodyParams);
        }
    }
}
