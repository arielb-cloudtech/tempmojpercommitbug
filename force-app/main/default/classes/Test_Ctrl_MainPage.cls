@isTest
public with sharing class Test_Ctrl_MainPage {

    @TestSetup
    static void createData() {

        Account acc = new Account(Name = 'Main');

        Datetime baseDT =  Datetime.now();
        List<Law_Item__c> lawItems = new List<Law_Item__c> {
            new Law_Item__c(Status__c = 'Canceled', Publish_Date__c= baseDT.addDays(-1), Law_Item_Name__c = 'UnDistributed Law Item - 3 comments', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-3), Law_Item_Name__c = 'Distributed Low Item with no comments', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Ready for Distribution', Publish_Date__c= baseDT.addDays(-21), Law_Item_Name__c = 'UnDistributed Law Item', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-11), Name='placeholder', Law_Item_Name__c = 'UnDistributed - Name placeholder', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-1), Name='No Law_Item_Name__c', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT, Law_Item_Name__c = 'Distributed Low Item with 5 comments', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Draft', Publish_Date__c= baseDT.addDays(-1), Law_Item_Name__c = 'UnDistributed Law Item ', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-1), Law_Item_Name__c = 'Distributed Low Item with no comments2', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-2), Law_Item_Name__c = 'Distributed Low Item with no comments3', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-3), Law_Item_Name__c = 'Distributed Low Item with no comments4', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-41), Law_Item_Name__c = 'Distributed Low Item with no comments5', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-5), Law_Item_Name__c = 'Distributed Low Item with no comments6', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-6), Law_Item_Name__c = 'Distributed Low Item with no comments7', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-7), Law_Item_Name__c = 'Distributed Low Item with no comments8', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-8), Law_Item_Name__c = 'Distributed Low Item with no comments9', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-9), Law_Item_Name__c = 'Distributed Low Item with no comments10', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-10), Law_Item_Name__c = 'Distributed Low Item with no comments11', Account_Responsibility__r = acc),
            new Law_Item__c(Status__c = 'Distributed', Publish_Date__c= baseDT.addDays(-11), Law_Item_Name__c = 'Distributed Low Item with no comments12', Account_Responsibility__r = acc)
        };
        insert lawItems;
        Test.setCreatedDate(lawItems[5].Id, Datetime.now().addDays(-1));

        lawItems = [SELECT Id, Law_Item_Name__c FROM Law_Item__c];

        Law_Item__c Distributed5Comments;
        Law_Item__c DistributedNoComments;

        for ( Law_Item__c li : lawItems ) {
            if ( 'Distributed Low Item with 5 comments' == li.Law_Item_Name__c ) {
                Distributed5Comments = li;
            }
            if ( 'Distributed Low Item with no comments' == li.Law_Item_Name__c ) {
                DistributedNoComments = li;
            }
        }

        System.assertNotEquals(null, Distributed5Comments);
        System.assertNotEquals(null, DistributedNoComments);

        //Comments
        List<Comment__c> comments = new List<Comment__c>{
            // Law Item 1
            new Comment__c(Comment_Text__c='visivle Comment1', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Published', Display_Comment__c = true),
            new Comment__c(Comment_Text__c='visivle Comment2', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Published', Display_Comment__c = true),
            new Comment__c(Comment_Text__c='Unvisivle Comment1', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Draft', Display_Comment__c = false),
            new Comment__c(Comment_Text__c='Unvisivle display = false', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Published', Display_Comment__c = false),
            new Comment__c(Comment_Text__c='Unvisivle Draft', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Draft', Display_Comment__c = true),
            new Comment__c(Comment_Text__c='Unvisivle Draft', Related_to_Law_Item__c=DistributedNoComments.Id, Comment_Status__c = 'Draft', Display_Comment__c = true),
            new Comment__c(Comment_Text__c='Unvisivle Deleted', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Deleted', Display_Comment__c = true),
            new Comment__c(Comment_Text__c='Visivle Comment3', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Published', Display_Comment__c = true),
            new Comment__c(Comment_Text__c='Visivle Comment4', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Published', Display_Comment__c = true),
            new Comment__c(Comment_Text__c='Visivle Comment5', Related_to_Law_Item__c=Distributed5Comments.Id, Comment_Status__c = 'Published', Display_Comment__c = true)
        };

        Id t = comments[0].Related_to_Law_Item__c;
        insert comments;

        List<Comment__c> savedComments = [SELECT Id ,Comment_Text__c, Display_Comment__c, Related_to_Law_Item__c , Comment_Status__c FROM Comment__c ];
        System.assertEquals(10, savedComments.size());
        for (Comment__c x : savedComments ) {
            System.debug(x);

        }

        ContentVersion cv = new ContentVersion(Title = 'wow', PathOnClient = 'wow.txt', versionData = Blob.valueOf('vd'));
		insert cv; 
        Id cdId = [select ContentDocumentId From ContentVersion Limit 1].ContentDocumentId;
        ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = cdId, LinkedEntityId = lawItems[0].Id, Visibility = 'ALLUsers', ShareType = 'I');
        Id lpvid = [select LatestPublishedVersionId From ContentDocument Order By CreatedDate Desc Limit 1].LatestPublishedVersionId;
        cv = [select File_Type__c From ContentVersion Where Id = :lpvid];
        cv.File_Type__c = 'Main File';
        update cv;
        insert cdl;    
    }
    
    @isTest
    public static void testGetLawItems(){
        Ctrl_MainPage.lawItemFetchResult lawItemFetchResult = Ctrl_MainPage.getLawItems(0, 10);
        System.assertEquals(10, lawItemFetchResult.items.size());

        Set<Id> lawItemsIds = new Set<Id>();

        //Validate Sorting
        System.assertEquals('Distributed Low Item with 5 comments', lawItemFetchResult.items[0].Title);
       
       

        //Validte right fetch
        for( ReturnObjects.ItemCard lawItem :  lawItemFetchResult.items) {
            lawItemsIds.add(lawItem.Id);
            if ( lawItem.Title.contains('Distributed Low Item with no comments')) {
                // System.assertEquals(0, lawItem.CommentsCount, lawItem);
            }
            else if (lawItem.Title == 'Distributed Low Item with 5 comments') {
               // System.assertEquals(5, lawItemFetchResult.items[0].CommentsCount);
            }
            else {
                System.assert(false, 'Unrecognised Title');
            }
        }

        lawItemFetchResult = Ctrl_MainPage.getLawItems(lawItemsIds.size() , 1);
        System.assertEquals(1, lawItemFetchResult.items.size());
        for( ReturnObjects.ItemCard lawItem :  lawItemFetchResult.items) {
            lawItemsIds.add(lawItem.Id);
        }

        lawItemFetchResult = Ctrl_MainPage.getLawItems(lawItemsIds.size() , 10);
        System.assertEquals(2, lawItemFetchResult.items.size());
        for( ReturnObjects.ItemCard lawItem :  lawItemFetchResult.items) {
            lawItemsIds.add(lawItem.Id);
        }
    }
}