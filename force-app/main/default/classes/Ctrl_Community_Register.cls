/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
public without sharing class Ctrl_Community_Register {

    public Ctrl_Community_Register() {
        String expid = ApexPages.currentPage().getParameters().get('expid');
        if (expId != null) {
            Site.setExperienceId(expId);
        }
    }


    @AuraEnabled
    public static List<User> getUser(String userId){
        return null;
    }

    @AuraEnabled
    public static ReturnObjects.AjaxMessage sendForgottenPassword(String username){
        ReturnObjects.AjaxMessage res = new ReturnObjects.AjaxMessage();
        List<User> RecUser = [SELECT Id,Username FROM User WHERE Username=:username OR Email=:username LIMIT 1];
        if(RecUser.isEmpty()){
            res.message = Label.Email_does_not_exist_on_the_system;
        }
        else {
            Site.forgotPassword(RecUser[0].Username);
            res.message = Label.You_may_have_accessed_your_spam_box_Subtitle;
            res.setSuccessStatus();
        }

        return res;
    }

     @AuraEnabled
    public static ReturnObjects.AjaxMessage UserLogin(String username,String password){
        System.debug('username: '+ username+' password '+password);
        ReturnObjects.AjaxMessage res = new ReturnObjects.AjaxMessage();
        PageReference ref;
        try {
            ref = Site.login(username,password, '');

            if (ref != null) {
                aura.redirect(ref);
                res.message = UserInfo.getUserId() ;
                res.setSuccessStatus();
            }
        }
        catch(Exception ex) {
            res.message = ex.getMessage();
        }
        return res;
     }

    @AuraEnabled
    public static String forgotMyPassword(String userId){
        try{
            Boolean bSuccess = Site.forgotPassword(userId);
            if(bSuccess)
                return 'true';
            else
                return 'false';
        }
        catch(Exception ex) {
            // This message is used for debugging. Do not display this in the UI to the end user.
            // It has the information around why the user creation failed.
            System.debug(ex.getMessage());
            return 'כלומר שגיאה לא ידועה: ' + ex.getMessage() + ex.getCause();
        }
    }

    /***
        get own user data -
        allow user to edit his details
    ***/
    @AuraEnabled
    public static ReturnObjects.MojUser getCurrentUser(){
        Id userId = UserInfo.getUserId();
       List<User> u = [SELECT  Username, MobilePhone, Email, FirstName, LastName, Company__c, Role__c  FROM User WHERE Id = :userId LIMIT 1];
       if (u == null || u.size() == 0)
        return null; // should not happen...
       return  new ReturnObjects.MojUser(u[0]);
    }

    @AuraEnabled
    public static ReturnObjects.AjaxMessage saveUserData(String FirstName, String LastName, String Phone , String Company, String Role, String currentPassword, String newPassword){
        ReturnObjects.AjaxMessage response = new ReturnObjects.AjaxMessage();
        Id userId = UserInfo.getUserId();

        User u = [SELECT Id, ContactId, Username, Email, FirstName,LastName, MobilePhone FROM USER WHERE Id = :userId LIMIT 1];

        u.FirstName = FirstName;
        u.LastName = LastName;
        u.MobilePhone = Phone;
        u.Company__c = Company;
        u.Role__c = Role;
        update u;

        Contact c = new Contact(Id = u.ContactId,
                                Email = u.Email,
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                MobilePhone = u.MobilePhone,
                                Company__c = Company);
        upsert(c);
        response.message= System.Label.personalDetailsHasBeenSaved;
        
        if( !String.isBlank(currentPassword) && !String.isBlank(newPassword) ) {
            try {
                PageReference pageRef = Site.changePassword(newPassword, newPassword, currentPassword);
                response.message= System.Label.passwordChangedSuccessfully;
                response.setSuccessStatus();
            }
            catch(exception ex){
                response.message = ex.getMessage(); 
            }
        }
        else {
            response.setSuccessStatus();
        }
        
        
        return response;
    }


    @AuraEnabled
    public static String changePassword(String newPassword, String verifyNewPassword, String oldPassword){
        String res ='';
        PageReference ref;
        try{
        ref = Site.changePassword(newPassword, verifyNewPassword, oldpassword);
            if(ref == null){
                res = 'ERROR';
                for (ApexPages.Message m : ApexPages.getMessages()) {
                    res = res + ' ' +  m;
                }
            }
        }
        catch(Exception ex) {
            // This message is used for debugging. Do not display this in the UI to the end user.
            // It has the information around why the user creation failed.
            System.debug(ex.getMessage());
            // return ex.getMessage() + ex.getCause();
            return ex.getMessage();

        }

        return res;


     }

     @AuraEnabled
	public static ReturnObjects.AjaxMessage resendRegistrationEmail(String userName){
        ReturnObjects.AjaxMessage res = new ReturnObjects.AjaxMessage();
        
        User u = [Select Id, Email, ContactId, userName From User Where userName = :userName Limit 1];

        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setTargetObjectId(u.id);

        message.setSenderDisplayName('MOJ');
        message.setReplyTo('no-reply@company.com');
        message.setUseSignature(false);
        message.setBccSender(false);
        message.setSaveAsActivity(false);
        EmailTemplate emailTemplate = [Select Id,Subject,Description,HtmlValue,DeveloperName,Body from EmailTemplate where developername = 'Communities_New_Member_Welcome_Email_Resend_HEB'];
        message.setTemplateID(emailTemplate.Id);
        //message.setWhatId(u.contactid); //This is important for the merge fields in template to work
        message.toAddresses = new String[] { u.Email};
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        res.setSuccessStatus();
        res.message = 'email sent';
        return res;

    }

    /***
        Register new user to the site


    ***/
	@AuraEnabled
	public static registrationResponse AddPortalUser(String phone, String firstName, String lastName, String email,  String password, String title, String role ){

        registrationResponse response = new registrationResponse();
        response.message = 'Unknown Error has occured';

        Map<String, Set<String>> validationErrors = registrationValidation( new  Map<String, String>{
            'firstName'=> firstName,
            'lastName'=> lastName,
            'phone'=> phone,
            'role'=> role,
            'email'=> email,
            'password'=> password
        });

        if ( validationErrors.size() > 0 ) {
            for ( String invalidField : validationErrors.keySet() ){
                for ( String fieldValidationError : validationErrors.get( invalidField ) ) {
                    response.errors.add(invalidField + ' - '+ invalidField);
                }
            }
            return response;
        }

        // Get the coomuinty account
        String accountId = [Select Id From Account Where Name = 'Community Account'].Id;
        if( null == accountId ) {
            throw new registrationException('An error has occured please contact site administrator');
        }

        User u = new User();
        u.Username = email;
        u.Email = email;
        u.FirstName = firstName;
        u.LastName = lastName;
        u.MobilePhone = phone;
        u.CommunityNickname = email;
        //TODO : Set language to current language
        u.LanguageLocaleKey = 'iw';

        List<Profile> pr = [SELECT Id from Profile where Name='Customer Community MOJ'];
        if (pr.size() > 0){
            u.ProfileId = pr[0].Id;
        }

        try {
            System.debug('before createPortalUser:');
            Id userId = Site.createPortalUser(u, accountId, password);
            System.debug('after createPortalUser:');
            linkContactToUser(userId);
            response.message = 'success';//Label.c.New_user_created;
            response.setSuccessStatus();

        } catch ( Exception ex ) {
            System.debug('Exception: ' + ex.getMessage() + ex.getStackTraceString());
            response.errors.add( ex.getMessage() );
        }
        return response;
	}

    private static void linkContactToUser(Id userId){
        User portalUser = [SELECT Id, ContactId FROM User WHERE Id = :userId LIMIT 1];
        Contact portalContact = [SELECT Id, Active_User__c, User__c, Role__c FROM Contact WHERE Id = :portalUser.ContactId LIMIT 1];
        portalContact.Active_User__c = true;
        portalContact.User__c = userId;
        portalContact.Role__c = 'Customer Community MOJ';
        update portalContact;
    }

    public class registrationResponse extends ReturnObjects.AjaxMessage {
        @AuraEnabled public List<String> errors = new List<String>();
    }

    public class registrationException extends Exception {
        Map<String,List<String>> validationErrors;

        registrationException(String errorMessage, Map<String,List<String>> validationErrors) {
            this(errorMessage);
            this.validationErrors = validationErrors;
        }
    }

    /**
    get Filters Data
    **/
    @AuraEnabled
    public static Ctrl_SearchResults.filtersResponse getFilters(List<Id> relatedLaws) {
        Ctrl_SearchResults.filtersResponse  response = Ctrl_SearchResults.getFilters(relatedLaws);
        response.message = 'a'+ relatedLaws.size();
        return response;
    }

    /**
    get Selectable options for related laws
    **/
    @AuraEnabled
    public static Ctrl_SearchResults.relatedLawsResponse getRelatedLawsItems(String search, List<String> excludeIds, List<String> lawClassifaction){

        Ctrl_SearchResults.relatedLawsResponse response = Ctrl_SearchResults.getRelatedLawsItems( search, excludeIds, lawClassifaction );
        return response;
    }


    /**
    Save Subscribtion rules.
    @weeklyAlerts - Assign user to weekly digest mail
    @dailyAlerts - Assign user to daily digest mail
    @rules - List of MailingSubscriptionRule
    **/
    @AuraEnabled
    public static ReturnObjects.AjaxMessage saveMailingRules(Boolean dailyAlerts, Boolean weeklyAlerts, Boolean allImmediate,  List<Object> rules)
    {
        Id userId = UserInfo.getUserId();
        // Start fresh
        delete [SELECT Id FROM Mailing_Subscription__c  WHERE User__c= :userId ];

        //Get Contact and prospect id to populate MS fields
        Contact contact = [SELECT Id, User__c, Pardot_Prospect_Id__c FROM Contact WHERE User__c = :userId LIMIT 1]; 
        String contactId = contact.Id;
        if(String.isBlank(contactId)){
            System.debug('>>>>>Contact id is blank!!!!!!!!!!!!!!!');
        }
        String pardotProspectId = contact.Pardot_Prospect_Id__c;
        if(String.isBlank(pardotProspectId)){
            System.debug('>>>>>pardotProspectId is blank!!!!!!!!!!!!!!!');
        }

        String userLanguage = UserInfo.getLanguage();

        // TODO: Set Mailing_Subscription__c language to language code
        if (userLanguage == 'iw') {
            userLanguage = 'Hebrew';
        }
        else {
            userLanguage = 'Arabic';
        }

        ReturnObjects.AjaxMessage response = new ReturnObjects.AjaxMessage();

        List<Mailing_Subscription__c> subscriptionRules = new List<Mailing_Subscription__c>();

        if( dailyAlerts ) {
            subscriptionRules.add(new Mailing_Subscription__c(Mailing_Frequency__c = 'Daily', User__c=userId, Registration_Type__c = 'All Updates', Mailing_Language__c= userLanguage, Contact__c = contactId));
        }
        
        if( weeklyAlerts ) {
            subscriptionRules.add(new Mailing_Subscription__c(Mailing_Frequency__c = 'Weekly', User__c=userId, Registration_Type__c = 'All Updates', Mailing_Language__c= userLanguage, Contact__c = contactId));
        }
        if( allImmediate ) {
             subscriptionRules.add(new Mailing_Subscription__c( Mailing_Frequency__c = 'Immediate', User__c=userId, Registration_Type__c = 'All Updates', Mailing_Language__c= userLanguage, Contact__c = contactId));
        }
        Map<String, String> mapRecordIdToTypeName = new Map<String, String>();
        List<Schema.RecordTypeInfo> lawItemRecordTypes = Law_Item__c.SObjectType.getDescribe().getRecordTypeInfos();

        for(Schema.RecordTypeInfo lawItemRecordType: lawItemRecordTypes ) {
            if( !lawItemRecordType.isMaster() ) {
                mapRecordIdToTypeName.put( lawItemRecordType.getRecordTypeId(), fixRecordTypeForMailingSubscription(lawItemRecordType.getDeveloperName()));
            }
        }

        for ( Object subscriptionRule : rules ) {
            Map<Object, Object> subscriptionRuleProperties = (Map<Object, Object>)subscriptionRule;
            List<Object> filterValues = (List<Object>)subscriptionRuleProperties.get('values');
            if( filterValues.size() == 0 ) {
                filterValues.add( null );
            }

            for( Object valueId : filterValues ) {

                List<Object> docTypes = (List<Object>)subscriptionRuleProperties.get('docTypes');

                if ( docTypes.size() == 0 ) {
                    for( String recordType : mapRecordIdToTypeName.keySet()) {
                        docTypes.add((Object)recordType);
                    }
                }

                for( Object documentTypeId : docTypes ) {
                    Mailing_Subscription__c newRule = new Mailing_Subscription__c(  User__c=userId,
                                                                                    Registration_Type__c = 'Personal Preferences',
                                                                                    Items_With_RIA__c = ((Boolean) subscriptionRuleProperties.get('onlyRIA') )? true : false,
                                                                                    Mailing_Language__c= userLanguage,
                                                                                    Item_Type__c = mapRecordIdToTypeName.containsKey((String)documentTypeId)? mapRecordIdToTypeName.get((String)documentTypeId): null,
                                                                                    Contact__c = contactId );
                    if ( null != valueId ) {
                        if ( ((String)subscriptionRuleProperties.get('type') ) == 'offices' ) {
                            newRule.Ministry__c = (Id)valueId;
                        }
                        if ( ((String)subscriptionRuleProperties.get('type') ) == 'classifications' ) {
                            newRule.Classification__c = (Id)valueId;
                        }
                        if ( ((String)subscriptionRuleProperties.get('type') ) == 'relatedLaws' ) {
                            newRule.Law__c = (Id)valueId;
                        }
                    }
                    subscriptionRules.add( newRule );
                }
            }
        }

        if ( ! subscriptionRules.isEmpty() ) {
            upsert subscriptionRules;
        }
        response.setSuccessStatus();
        return response;
    }

    @AuraEnabled
    public static mailingSubscriptionResponse getMailingRules()
    {
        String userId = UserInfo.getUserId();

        List<Mailing_Subscription__c> ruleLst = [Select Id, User__c, Contact__c, Items_With_RIA__c, Classification__c, Ministry__c, Law__c,
                                                ClassificationName__c, LawName__c, OfficeName__c, Item_Type__c, Mailing_Frequency__c, Registration_Type__c
                                                From Mailing_Subscription__c Where User__c =: userId];


        mailingSubscriptionResponse response = new mailingSubscriptionResponse(ruleLst);
        response.setSuccessStatus();
        return response;
    }

    /**
    Fix for getting the right selection in  Mailing_Subscription__c:
    TODO: Rebuild Item_Type__c to be auto synchronize with developer name
    **/
    private static string fixRecordTypeForMailingSubscription(string developerName){
        return developerName.replace('_', ' ');
    }

    public class mailingSubscriptionResponse extends ReturnObjects.AjaxMessage {
		@AuraEnabled public Boolean weeklySummery = false;
		@AuraEnabled public Boolean dailySummery = false;
		@AuraEnabled public Boolean subscribeToAllMails = false;
		@AuraEnabled public list<MailingSubscriptionRule> mailingRules = new list<MailingSubscriptionRule>();

        public mailingSubscriptionResponse(List<Mailing_Subscription__c> dbRules) {
            super();
            // Law types
            Map<String, String> mapRecordTypeNameToId = new Map<String, String>();
            List<Schema.RecordTypeInfo> lawItemRecordTypes = Law_Item__c.SObjectType.getDescribe().getRecordTypeInfos();
            mapRecordTypeNameToId.put( '', null );
            for(Schema.RecordTypeInfo lawItemRecordType: lawItemRecordTypes ) {
                if ( !lawItemRecordType.isMaster() ) {
                    mapRecordTypeNameToId.put( fixRecordTypeForMailingSubscription(lawItemRecordType.getDeveloperName()), lawItemRecordType.getRecordTypeId() );
                }
            }

            /***

                Need to aggrgate all documentTypes and FilterIds foreEach FilterType and RIA included

                Aggregation proccess logic is as follows:
                1. Create two keyHolders for RIA/WithoutRIA
                2. For Each filterId save all documentTypes this item is filtered at (for and without RIA)
                3. Rubuild the rules from aggregation data

            ***/
            Map<Id , Map<String, Boolean>> keyHolder = new Map<Id , Map<String, Boolean>>();
            Map<Id , Map<String, Boolean>> RIAkeyHolder = new Map<Id , Map<String, Boolean>>();

            Map<String, Set<String>> ItemIdToTypeMap = new  Map<String, Set<String>>{
                'documentType'=> new Set<String>(),
                'classifications'=> new Set<String>(),
                'offices'=> new Set<String>(),
                'relatedLaws'=> new Set<String>()
            };

            for ( Mailing_Subscription__c dbRule: dbRules ) {
                 switch on dbRule.Registration_Type__c {
                    // Phase 1: Prepare
                    // aggregate to singles rules base on : documentType & IsRia
                    // first create key of aggregation for each filtered selection
                    when 'Personal Preferences' {
                        // Default to documentType filter
                        String itemId = mapRecordTypeNameToId.get( dbRule.Item_Type__c );
                        String filterType = 'documentType';

                        if( dbRule.Classification__c != null ) {
                            itemId = dbRule.Classification__c;
                            filterType = 'classifications';
                        }

                        if( dbRule.Ministry__c != null ) {
                            itemId = dbRule.Ministry__c;
                            filterType = 'offices';
                        }

                        if( dbRule.Law__c != null ) {
                            itemId = dbRule.Law__c;
                            filterType = 'relatedLaws';
                        }

                        if( filterType == 'documentType') {
                            itemId = null;
                        }

                        Map<Id , Map<String, Boolean>> keyHolderRefference = keyHolder;
                        if( dbRule.Items_With_RIA__c ){
                            keyHolderRefference = RIAkeyHolder;
                        }

                        ItemIdToTypeMap.get(filterType).add(itemId);

                        Map<String, Boolean> keyBuilder = keyHolderRefference.get(itemId);
                        if( keyBuilder == null ) {
                            keyBuilder = new Map<String, Boolean>{
                                'RIA_Only'=> dbRule.Items_With_RIA__c
                            };
                            for(String DevloperDocumentTypeName : mapRecordTypeNameToId.keySet() ) {
                                keyBuilder.put(DevloperDocumentTypeName, false);
                            }
                        }
                        keyBuilder.put(dbRule.Item_Type__c, true);
                        keyHolderRefference.put(itemId, keyBuilder);
                    }
                    when else {
                        if ( dbRule.Mailing_Frequency__c == 'Daily' ){
                            this.dailySummery = true;
                            continue;
                        }
                         if ( dbRule.Mailing_Frequency__c == 'Immediate' ){
                            this.subscribeToAllMails = true;
                            continue;
                        }
                        if ( dbRule.Mailing_Frequency__c == 'Weekly' ){
                            this.weeklySummery = true;
                            continue;
                        }
                    }
                }
            }

            // After aggregation is prepared build aggregate rule items
            // map: filterType=> keys ( documentFilter ) => filtered Values ( Ids )
            List<Map<Id , Map<String, Boolean>>> keyHolders = new List<Map<Id , Map<String, Boolean>>>{
                keyHolder, RIAkeyHolder
            };


            Map<String , Map<Map<String, Boolean>, Set<String>> > mailigRulesList = new Map<String , Map<Map<String, Boolean>, Set<String>> >();

            // Phase 2: Aggregate
            for ( String filterType : ItemIdToTypeMap.keySet() ){
                // search both array - with and without RIA
                Map<Map<String, Boolean>, Set<String>> IdByKeyForTypeMap = new  Map<Map<String, Boolean>, Set<String>>();
                for (Map<Id , Map<String, Boolean>> keyHolderRefference : keyHolders ) {
                    for ( String filterId: ItemIdToTypeMap.get(filterType) ){
                        Map<String, Boolean> key = keyHolderRefference.get(filterId);
                        if ( null != key ){
                            if ( null == IdByKeyForTypeMap.get(key)) {
                                IdByKeyForTypeMap.put(key, new Set<String>());
                            }
                            IdByKeyForTypeMap.get(key).add(filterId);
                        }
                    }
                }
                mailigRulesList.put(filterType, IdByKeyForTypeMap);
            }

            // phase 3: create MailingSubscriptionRules
            for ( String filterType : mailigRulesList.keySet() ) {
                for(  Map<String, Boolean>  filterKeys : mailigRulesList.get(filterType).keySet() ) {
                    MailingSubscriptionRule rule = new MailingSubscriptionRule();
                    rule.type = filterType;
                    if ( filterType != 'documentType' ){
                        rule.values = new List<String>(mailigRulesList.get(filterType).get(filterKeys) );
                    }
                    else {
                        rule.values = new List<String>();
                    }

                    for( String filterKey : filterKeys.keySet() ) {
                        if ( filterKeys.get( filterKey )) {
                            if ( filterKey != 'RIA_Only' ) {
                                if ( filterType == 'documentType' ) {
                                    rule.values.add(  mapRecordTypeNameToId.get( filterKey ) );
                                }
                                else {
                                    rule.docTypes.add( mapRecordTypeNameToId.get( filterKey ) );
                                }
                            }
                            else {
                                rule.onlyRIA = true;
                            }
                        }
                    }

                    this.mailingRules.add( rule );
                }
            }
        }
	}

    /**
        Single mailing rule for immidiate mailing subscribtion
    **/
    public class MailingSubscriptionRule {
        @AuraEnabled public String type;
        @AuraEnabled public Boolean onlyRIA = false;
        @AuraEnabled public List<String> docTypes = new List<String>();
        @AuraEnabled public List<String> values;
    }

    /**
     * method to check the values that were entered in registration page
     * returns  - empty string Map if everything is ok
     *          - string List of field names with validation errors
     */
    public static Map<String, Set<String>> registrationValidation(Map<String,String> fieldValues){

        Map<String, Set<String>> result = new Map<String,Set<String>>();


        Map<String, Set<String>> validations = new Map<String, Set<String>> {
            'lastName'  => new Set<String>{ 'required', 'min2chars'},
            'firstName' => new Set<String>{ 'required', 'min2chars'},
            'phone'     => new Set<String>{ 'phone' },
            'email'     => new Set<String>{ 'email' },
            'password'  => new Set<String>{ 'required', 'min8chars' }
        };

        for( String checkedField : validations.keySet() ) {
            Set<String> validationRulesForField = validations.get(checkedField);
            String fieldValue = fieldValues.get(checkedField);

            if( fieldValue == '' || fieldValue== null) {

                if( validationRulesForField.contains('required') ) {
                    result.put(checkedField, new Set<String>{System.Label.Field_is_empty_error.replace('%s',checkedField)});
                }

                continue;
            }

            Set<String> fieldValidationErrors = new Set<String>();

            if( validationRulesForField.contains('min2chars') ) {
                if ( fieldValue.length() < 2 ) {
                    fieldValidationErrors.add(System.Label.Name_length_error.replace('%s',checkedField));
                }
            }
            if( validationRulesForField.contains('min8chars') ) {
                if ( fieldValue.length() < 8 ) {
                    fieldValidationErrors.add(System.Label.Password_Field_Invalid_length.replace('%s',checkedField));
                }
            }

            if( validationRulesForField.contains('email') ) {
                if(! pregMetched('([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})', fieldValue)) {
                    fieldValidationErrors.add(System.Label.Invalid_field_pattern.replace('%s',checkedField));
                }
            }

            if( validationRulesForField.contains('phone') ) {
                if(!pregMetched('(.*)[0-9](.*)', fieldValue)) {
                    fieldValidationErrors.add(System.Label.Invalid_field_pattern.replace('%s',checkedField));
               }
            }

            if( validationRulesForField.contains('hasNumber') ) {
                if(! pregMetched('(.*)[0-9](.*)', fieldValue)) {
                    fieldValidationErrors.add(System.Label.Password_Field_Invalid_Digit.replace('%s',checkedField));
                }
            }
            if( validationRulesForField.contains('hasSpecialChar') ) {
                if(! pregMetched('(.*)[!@#$%^&*(),.?":{}|<>](.*)', fieldValue)) {
                    fieldValidationErrors.add(System.Label.Password_Field_Invalid_SpecialChar.replace('%s',checkedField));
                }
            }

            if( validationRulesForField.contains('hasCap') ) {
                 if(! pregMetched('(.*)[A-Z](.*)', fieldValue)) {
                    fieldValidationErrors.add(System.Label.Password_Field_Invalid_Cap.replace('%s',checkedField));
                }
            }

            if( validationRulesForField.contains('hasChar') ) {
                if(! pregMetched('(.*)[a-z](.*)', fieldValue)) {
                    fieldValidationErrors.add(System.Label.Password_Field_Invalid_lowercase.replace('%s',checkedField));
                }
            }

            if ( fieldValidationErrors.size() > 0 ) {
                result.put(checkedField, fieldValidationErrors);
            }
        }
        return result;
    }

    private static Boolean pregMetched(String regex, String fieldValue){
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(fieldValue);
        return matcher.matches();
    }
}