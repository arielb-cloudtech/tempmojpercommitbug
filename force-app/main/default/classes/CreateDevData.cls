public with sharing class CreateDevData {
     public User thisUser;

	public CreateDevData () {
		 thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() limit 1];       

        
		
	}

    public void CreateNewData(){

        Savepoint sp = Database.setSavepoint();
        try{
        //        Create Account
        Account acc = new Account(Name = 'CloudTech1');
		insert acc;		

        // //Create Law
        Law__c l = new Law__c(LawID__c = '568879', Law_Name__c ='test');
        insert l;


        // //create contact
        String randomstr = string.valueof( (Math.floor(Math.random() * 999999) + 1).intValue() ) ;
 		Contact con = new Contact ( User__c = null, ID_Card__c = '111111118' ,  FirstName= 'Testing123'+randomstr ,LastName='Testing123'+randomstr,Email = 'Test'+randomstr+'@gmail.com'+'.'+randomstr , Role__c = 'Referent'  ,  Active_User__c = true, AccountId= acc.id);
		insert con;
		

        // //PardotList
        List<DynamicList__c> dls = new List<DynamicList__c>();
		dls.add(new DynamicList__c(Name	='Immediate Hebrew', Frequency__c='Immediate', Language__c='Hebrew', Pardot_List_Id__c='1111'));
		dls.add(new DynamicList__c(Name	='Immediate Arabic', Frequency__c='Immediate', Language__c='Arabic', Pardot_List_Id__c='2222'));
		dls.add(new DynamicList__c(Name	='Daily Hebrew', Frequency__c='Daily', Language__c='Hebrew', Pardot_List_Id__c='3333'));
		dls.add(new DynamicList__c(Name	='Daily Arabic', Frequency__c='Daily', Language__c='Arabic', Pardot_List_Id__c='4444'));
		dls.add(new DynamicList__c(Name	='Weekly Hebrew', Frequency__c='Weekly', Language__c='Hebrew', Pardot_List_Id__c='5555'));
		dls.add(new DynamicList__c(Name	='Weekly Arabic', Frequency__c='Weekly', Language__c='Arabic', Pardot_List_Id__c='6666'));
		insert dls;

         //pardotCampaign
         insert new Pardot_Campaign__c(Name='Email Distribution',PardotId__c='0000');

        // //Mailing_Subscription__c
        Mailing_Subscription__c lms = new Mailing_Subscription__c(Contact__c = con.Id,
																  Registration_Type__c = 'Personal Preferences', 
																  Mailing_Frequency__c = null, 
																  Ministry__c = null, 
																  Mailing_Language__c = 'Hebrew',
																  Law_Item__c = null, 
																  Law__c = null, 
																  Include_Comments_Updates__c =  true,
																  Classification__c =  null, 
																  Items_With_RIA__c = false,
																  Personal_Preference_Type__c = null, 
																  Item_Type__c = null);
        Insert(lms);

        //LawItem
         Law_Item__c li = new Law_Item__c(Status__c = 'Distributed', Law_Item_Name__c = 'wow1', Document_Brief__c ='Law Item Wow 1', 
          Account_Responsibility__c = acc.Id);
		 insert li;

         li = new Law_Item__c(Status__c = 'Distributed', Law_Item_Name__c = 'wow4', Document_Brief__c ='Law Item Wow 4', 
                                   Last_Date_for_Comments__c = Date.parse('10/01/2019'),  Account_Responsibility__c = acc.Id);
		 insert li;

       
        }
        catch(Exception e)
        {
            Database.RollBack(sp);
        }
    }
}