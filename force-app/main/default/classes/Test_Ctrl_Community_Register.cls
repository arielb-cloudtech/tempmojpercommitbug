@isTest
public with sharing class Test_Ctrl_Community_Register {
    
    private static List<User> users = [select Id, accountId, profileId from User Where id= :UserInfo.getUserId() LIMIT 1];

    static testMethod void testPortalUserRegistration(){
        Ctrl_Community_Register.AddPortalUser('0507321212', 'valid', 'validy', 'valid@mail.com', '1Az23!@#@1','', '' );
        Ctrl_Community_Register.AddPortalUser('0507121212', 'invalid', 'invalidy', 'valid@mail.com',  '1Az23!@#@1','', '' );
        List<User> u = [SELECT  Username, MobilePhone, Email, FirstName, LastName, Company__c, Role__c  FROM User WHERE FirstName = 'valid'];
        List<User> users = Ctrl_Community_Register.getUser(UserInfo.getUserId());
       // System.assertEquals(1, users.size());
       // System.assertEquals('valid' ,users[0].FirstName);
    }


    @isTest
    public static void testGetUser(){
        List<User> users = Ctrl_Community_Register.getUser(UserInfo.getUserId());
    }

    @isTest
    public static void testForgotMyPassword(){
        String res = Ctrl_Community_Register.forgotMyPassword(UserInfo.getUserId());
    }

    @isTest
    public static void testGetUserData(){ //correct userId
        Ctrl_Community_Register.getCurrentUser();
    }

    @isTest
    public static void testChangePassword(){
        String res = Ctrl_Community_Register.changePassword('kiki', 'kiki', 'teagonet01');
    }

    @isTest
    public static void testRegisterUser(){
        Account account = new Account(Name='Community Account');
        insert account;
        ReturnObjects.AjaxMessage res = Ctrl_Community_Register.AddPortalUser('0503333333', 'shaul', 'eizenberg', 't@gmail.com', 'rrr', 'gigli', 'manager');
        // System.assertEquals(res.status, 'success');
    }

    @isTest
    public static void testGetMailingRules(){
        Ctrl_Community_Register.getMailingRules();
    }

    @TestSetup
    static void makeData(){
        //cls obj creator instance to create/ret records
        ClsObjectCreator cls = new ClsObjectCreator();
        //accounts
        List<Account> accs = new List<Account>();
        accs.add(cls.returnAccount('Community Account'));
        insert accs;
        //querying for user to assert account id
       // List<User> users = [select accountId from User Where id= :UserInfo.getUserId() LIMIT 1];
        //system.debug('acountacountaccountacount'+accs[0].Id);  
        //system.debug('useruseruseruseruser'+users[0].AccountId);
        //contacts
        List<Contact> contacts = new List<Contact>();
        contacts.add(cls.returnContact('mylname', UserInfo.getUserId()));
        insert contacts;
        //mailing subscription
        List<Mailing_Subscription__c> msList = new List<Mailing_Subscription__c>();
        msList.add(cls.returnMailingSubscription(UserInfo.getUserId(), 'Memorandum of Law'));
        msList.add(cls.returnMailingSubscription(UserInfo.getUserId(), 'Secondary Legislation Draft'));
        msList.add(cls.returnMailingSubscription(UserInfo.getUserId(), 'Support Test Draft'));
        msList.add(cls.returnMailingSubscription(UserInfo.getUserId(), 'Ministry Directive or Procedure Draft'));
        insert msList;
    }
}