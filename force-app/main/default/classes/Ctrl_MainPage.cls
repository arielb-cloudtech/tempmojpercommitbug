public without sharing class Ctrl_MainPage {
    
    @AuraEnabled
    public static lawItemFetchResult getLawItems(Integer StartRec, Integer NumberOfResultToFetch) {

        lawItemFetchResult response = new lawItemFetchResult();

        List<Law_Item__c> LawItems =    [   SELECT Id,CreatedDate, Document_Brief__c,Law_Item_Name__c,Status__c,Publisher_Account__r.Name,Last_Date_for_Comments__c,Publish_Date__c,Type__c,RecordTypeId
                                            FROM Law_Item__c 
                                            WHERE Status__c = 'Distributed' And Name != 'placeholder' And Law_Item_Name__c != null 
                                            ORDER BY Publish_Date__c DESC 
                                            LIMIT :NumberOfResultToFetch
                                            OFFSET :StartRec ];
        
        response.total = [SELECT COUNT() FROM Law_Item__c WHERE Status__c = 'Distributed' And Name != 'placeholder' And Law_Item_Name__c != null ];
        response.items = Ctrl_SearchResults.loadCommentsAndFiles(LawItems);

        return  response;
    }

    @AuraEnabled
    public static filterTypesResponse getfilterOptions(){
        filterTypesResponse response = new filterTypesResponse();

        List<RecordType> rtLst = [Select Id, DeveloperName From RecordType Where SObjectType = 'Law_Item__c'];
        Map<String, Boolean> rtMap = new Map<String, Boolean>();
        Map<String, String> rtIdMap = new Map<String, String>();
        
        for(RecordType rt : rtLst)
        {
            Integer liCnt = database.countQuery('Select count() From Law_Item__c Where RecordTypeId = \''+rt.Id+'\' And Status__c = \'Distributed\'');
            rtMap.put(rt.DeveloperName, liCnt == 0 ? true : false);
            rtIdMap.put(rt.DeveloperName, rt.Id);
        }

        response.filters.add(new filterOption(rtIdMap.get('Memorandum_of_Law'), System.Label.Memorandums_Of_Law,System.Label.Memorandums_Of_Law, 'Icon-tzkir', rtMap.get('Memorandum_of_Law')));
        response.filters.add(new filterOption(rtIdMap.get('Secondary_Legislation_Draft'), System.Label.Drafts_Legislation,System.Label.Drafts_Legislation, 'Icon-mshn', rtMap.get('Secondary_Legislation_Draft')));         
        response.filters.add(new filterOption(rtIdMap.get('Ministry_Directive_or_Procedure_Draft'), System.Label.Draft_procedures_and_guidelines,System.Label.Draft_procedures_and_guidelines, 'Icon-nhlm', rtMap.get('Ministry_Directive_or_Procedure_Draft')));
        response.filters.add(new filterOption(rtIdMap.get('Support_Test_Draft'), System.Label.Draft_support_tests,System.Label.Draft_support_tests,'Icon-mvhn', rtMap.get('Support_Test_Draft')));
        response.filters.add(new filterOption('', System.Label.All_Data,System.Label.All_Data,'Icon-filter-all', false));

        response.setSuccessStatus();
        return response;
    }

    public class filterTypesResponse extends ReturnObjects.AjaxMessage {
         @AuraEnabled public List<filterOption> filters = new List<filterOption>();
    }

    /**
    Global Item to wrap request for law items for MainPage
    */
    public class lawItemFetchResult {
        @AuraEnabled public Integer total = 0;
        @AuraEnabled public List<ReturnObjects.ItemCard> items;
    }

    public class filterOption{

        @AuraEnabled public String id;
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public String image;
        @AuraEnabled public Boolean soon;

        public filterOption(String id, String cKey,String cValue, String cVersion, Boolean sn){
            this.label = cKey;
            this.value = cValue;
            this.image = cVersion;
            this.soon = sn;
            this.id = id;
        }
    }
}