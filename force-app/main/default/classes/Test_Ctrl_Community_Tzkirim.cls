@isTest
public with sharing class Test_Ctrl_Community_Tzkirim {

    @isTest
    public static void testInsertCommentWithSections() {
         // create Account
        Account account = new Account(Name = 'The ministry of something');
        insert account;

        // Create Users
        Profile profile = [SELECT Id from Profile where Name='Customer Community MOJ']; 

        List<Contact> contacts = new List<Contact> {
            new Contact(LastName ='tester1', FirstName ='tester1', Email='mail1@mojmail.test', AccountId = account.Id),
            new Contact(LastName ='tester2', FirstName ='tester2', Email='mail2@mojmail.test', AccountId = account.Id)
        };
        insert contacts;  

        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        List<User> users = new List<User> {
            new User(LastName='one', UserName= uniqueName+'1@mojmail.test', ContactId=contacts[0].Id, LocaleSidKey='iw_IL', Alias = 'standt',  ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', LanguageLocaleKey='en_US',  EmailEncodingKey='UTF-8', Email='testuser1@moj.mail' , FirstName = 'user'),
            new User(LastName='two', UserName= uniqueName+'2@mojmail.test', ContactId=contacts[1].Id, LocaleSidKey='iw_IL', Alias = 'standt',  ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', LanguageLocaleKey='en_US',  EmailEncodingKey='UTF-8', Email='testuser2@moj.mail' , FirstName = 'user')
        };
        insert users;

        List<ContentVersion> cvs = new List<ContentVersion>();
        cvs.add(new ContentVersion(Title = 'myCv', PathOnClient = 'myCv.txt', versionData = Blob.valueOf('kiki')));
        insert cvs;

        List<Law_Item__c> liList = new List<Law_Item__c>();
        liList.add(new Law_Item__c(Law_Item_Name__c = 'Parent Comment', Account_Responsibility__c = account.Id));
        insert liList;
        ReturnObjects.commentSection aaa;

        List<Map<String, String>> commentSections = new List<Map<String, String>> {
            new Map<String, String>{'Section'=>'1a', 'Comment'=>'This is comment Section 1'},
            new Map<String, String>{'Section'=>'2' , 'Comment'=>'This is comment Section 2'},
            new Map<String, String>{'Section'=>'3b', 'Comment'=>'This is comment Section 3'},
            new Map<String, String>{'Section'=>'55', 'Comment'=>'This is comment Section 4'}
        };

        ReturnObjects.AjaxMessage response;
        // String lawItemId, String commentId, String commentBody, String isPrivate, 
		// String commentStatus, List<ReturnObjects.commentSection> commentSections
        response = Ctrl_Community_Tzkirim.updateComment(liList[0].Id, '', 'test parent comment', false, '', commentSections);
        Ctrl_Community_Tzkirim.commentsResponse commentsResponse =  Ctrl_Community_Tzkirim.getComments(liList[0].Id, new List<String>(),'', ''); 
        system.assertEquals( 1, commentsResponse.total );
        system.assertEquals( 1, commentsResponse.comments.size() );
        system.assertEquals( 4, commentsResponse.comments[0].commentSections.size() );
    }


    
}