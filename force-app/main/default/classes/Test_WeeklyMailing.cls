@isTest
public with sharing class Test_WeeklyMailing {

    @isTest(seeAllData=true)
    public static void testWeeklyScheduler(){
        Test.setMock(HttpCalloutMock.class, new Test_LawItemTriggerHandler.Mock());

		Test.StartTest();
		System.schedule('Weekly Summary', '0 0 15 ? * SUN,MON,TUE,WED,THU,FRI *', new Sch_EmailUpdateWeekly() );
		Test.stopTest();
    }

}