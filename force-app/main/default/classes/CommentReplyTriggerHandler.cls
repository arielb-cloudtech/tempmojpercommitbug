public with sharing class CommentReplyTriggerHandler {
    public CommentReplyTriggerHandler() {
        
    }

    public void ProfanityHandler(List<Comment_Reply__c> newList) {
        List<Case> caseList = new List<Case>();
        Map<Id,Id> lawItem2Owner = new Map<Id,Id>();
        Set<Id> relatedItems = new Set<Id>();
        for(Comment_Reply__c com : newList){
            if(com.Related_to_Item__c!=null){
                relatedItems.add(com.Related_to_Item__c);
            }
        }
            system.debug('^^^ relatedItems '+ relatedItems);

        for(Law_Item__c l : [Select Id, OwnerId FROM Law_Item__c WHERE Id IN: relatedItems]){
            lawItem2Owner.put(l.Id, l.OwnerId);
        }
        
            system.debug('^^^ lawItem2Owner '+ lawItem2Owner);

        for(Comment_Reply__c com : newList){
            
            system.debug('com '+com);
            Case cs = Util_ProfanityCheck.ProfanityCheck(com.Reply__c, com.Id, false, com.Reply_to_Comment__c);
            system.debug('^^^ cs '+ cs);
            com.Reply__c = cs.ReplacementText__c;
            cs.OwnerId = com.Related_to_Item__c!=null && lawItem2Owner.containsKey(com.Related_to_Item__c)? lawItem2Owner.get(com.Related_to_Item__c) : UserInfo.getUserId();
            caseList.add(cs);
        }
        system.debug('^^^^ caseList '+caseList);
        upsert caseList;
    }
}