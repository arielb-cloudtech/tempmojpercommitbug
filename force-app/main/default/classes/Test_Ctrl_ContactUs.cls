@isTest
public without sharing class Test_Ctrl_ContactUs {
    public Test_Ctrl_ContactUs() {

    }

    @TestSetup
    static void makeData(){
        ClsObjectCreator cls = new ClsObjectCreator(); 
        //cases
        List<Case> cases = new List<Case>();
        cases.add(cls.returnCase());
        insert cases;
        //comment
        Comment__c com = cls.createComment('Published');
        //content version & content document automatically & cdls
        List<ContentVersion> cvList = new List<ContentVersion>();
        cvList.add(cls.returnContentVersion('myT', 'myT.txt', 'vd'));
        cvList.add(cls.returnContentVersion('myT2', 'myT2.txt', 'vd2'));
        insert cvList;
        List<ContentDocumentLink> cdls = new List<ContentDocumentLink>();
        Id cdId1 = [select ContentDocumentId From ContentVersion Where Title = 'myT' LIMIT 1].ContentDocumentId; 
        cdls.add(cls.returnCDL(cdId1, cases[0].Id));
        Id cdId2 = [select ContentDocumentId From ContentVersion Where Title = 'myT2' LIMIT 1].ContentDocumentId; 
        cdls.add(cls.returnCDL(cdId2, com.Id));
        insert cdls;
        
    }

    @isTest
    public static void testDeleteDocument(){
        Ctrl_ContactUs.deleteDocument([select ContentDocumentId from ContentVersion LIMIT 1].ContentDocumentId);
    }

    @isTest
    public static void testCancelCase(){
        List<String> lst = new List<String>();
        lst.add([select ContentDocumentId from ContentVersion LIMIT 1].ContentDocumentId); 
        Ctrl_ContactUs.cancelCase([select id from case LIMIT 1].Id, lst);
    }

    @isTest
    public static void test1SaveFeedback(){//null id - ok
       Ctrl_ContactUs.SaveFeedback(null , 't', 's', 't@gmail.com', '0567893526', 'national geographic', 'beast', 'holly shit', 'the sun goes down');
    }
}