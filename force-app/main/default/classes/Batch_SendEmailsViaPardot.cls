global class Batch_SendEmailsViaPardot implements Database.Batchable<String>, Database.Stateful, Database.AllowsCallouts{
	
	public String query;
	public Set<String> prospects;
	public Set<String> pardotLists;
	//public Map<String, Set<String>> pardotListsByLanguage;
	public String opperation;
	public String ApiKey;
	public String newList;
	public Set<String> contentDistributions;
	public Set<Id> LawItemIds;
	public String lang;
	//public String pardotLists;
	global Batch_SendEmailsViaPardot(String op, String l, Set<String> prospectsSet, Set<Id> lawItemsIdsSet){
		opperation = op;
		lang = l;
		prospects = prospectsSet;
		LawItemIds = lawItemsIdsSet;
	}

/*	global Batch_SendEmailsViaPardot(String op, String ApK, Set<Id> LII, String l, Set<String>prspcts) {
		this.opperation=op;
		this.ApiKey=ApK;
		//newList=lst;
		this.prospects = prspcts;
		this.LawItemIds = LII;
		this.lang=l;
	}*/
	
	global Iterable<String> start(Database.BatchableContext BC) {
		//if(ApiKey == null){
		String ThisApiKey = Utils.CalloutBuilder('authentication');
		ApiKey = ThisApiKey;
		//}

		system.debug('>>>>>opperation: ' + opperation + ',lang: ' + lang + 'prospects: ' + prospects );

		///allways immediate
		if(opperation.equalsIgnoreCase('Immediate')){
			//// add the list for all update immediate
			pardotLists = new Set<String>();

			// for(DynamicList__c dl : DynamicList__c.getAll().values()){
			// 	if(dl.Frequency__C.equalsIgnoreCase(opperation) && dl.Language__c.equalsIgnoreCase(lang)){
			// 		pardotLists.add(dl.Pardot_List_Id__c);
			// 		System.debug('>>>>>Added LIst: ' + dl.Pardot_List_Id__c  +', '+ opperation +', '+ lang);
			// 	}
			// }

/*			for(DynamicList__c dl : DynamicList__c.getAll().values()){
				if(dl.Frequency__C.equalsIgnoreCase(opperation)){
					pardotLists.add(dl.Pardot_List_Id__c);
				}


				//if(!pardotListsByLanguage.containsIgnoreCase(dl.Language__c)){
				//	pardotListsByLanguage.put(dl.Language__c, new Set<String>());
				//}
				//pardotListsByLanguage.get(dl.Language__c,dl.Padot_List_Id__c);

			}*/
			String ThisList = Utils.createList(ApiKey);
			newList = ThisList;
			system.debug('^^^ ThisList '+ ThisList);
			pardotLists.add(ThisList);



			return new List<String>(prospects);
		}
		return new List<String>();
	}

	/// to create Lists with callouts max scope 10 - 8=safty by key
   	global void execute(Database.BatchableContext BC, List<String> scope) {
   		system.debug('^^^ (Batch_SendEmailsViaPardot execute)  newList '+ newList);
   		//system.debug('^^^ (Batch_SendEmailsViaPardot execute)  ProspectId '+ ProspectId);
   		system.debug('^^^ (Batch_SendEmailsViaPardot execute)  scope '+ scope);
		for(String ProspectId : scope){
			Utils.assignMembersToList(ApiKey, newList , new List<String>{ProspectId}, true);
		}
	}
	
	global void finish(Database.BatchableContext BC)  {
		Set<Id> docSet = new Set<Id>();
   		Map<Id, Map<String,String>> docs2Links = new Map<Id, Map<String,String>>();
   		Map<Id,Map<String,String>> LawItem2Links = new Map<Id,Map<String,String>>();
   		Map<Id,Map<String,String>> LawItem2MainLinks = new Map<Id,Map<String,String>>();
   		Map<Id,Map<String,String>> LawItem2RiaLinks = new Map<Id,Map<String,String>>();
   		Map<Id,Map<String,String>> LawItem2CombLinks = new Map<Id,Map<String,String>>();
   		Map<Id,Set<Id>> docsByLawItem = new Map<Id,Set<Id>>();
   		Set<Id> latestVers = new Set<Id>();

		Map<Id, Map<String,String>> mainFilesLawItemToURLAndFileExtensions = new Map<Id, Map<String,String>>();
		Map<Id, Map<String,String>> riaFilesLawItemToURLAndFileExtensions = new Map<Id, Map<String,String>>();
		Map<Id, Map<String,String>> combFilesLawItemToURLAndFileExtensions = new Map<Id, Map<String,String>>();

		Map<Id, Map<String,String>> mainFilesCDIDToURLAndFileExtensions = new Map<Id, Map<String,String>>();
		Map<Id, Map<String,String>> riaFilesCDIDToURLAndFileExtensions = new Map<Id, Map<String,String>>();
		Map<Id, Map<String,String>> combFilesCDIDToURLAndFileExtensions = new Map<Id, Map<String,String>>();

   		for(Law_Item__c li : [Select Id, (Select Id, ContentDocumentId, LinkedEntity.Name, ContentDocument.LatestPublishedVersionId, ContentDocument.LatestPublishedVersion.IsRia__c, ContentDocument.LatestPublishedVersion.File_Type__c	 , ContentDocument.LatestPublishedVersion.Main__c ,ContentDocument.Title, LinkedEntityId FROM ContentDocumentLinks) From Law_Item__c WHere Id IN: LawItemIds]){
			//for(ContentDocumentLink cdl : [SELECT ContentDocumentId, LinkedEntity.Name, ContentDocument.LatestPublishedVersionId ,ContentDocument.Title, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN : LawItemIds]){
	            //String typeEntity = String.valueOf((cdl.LinkedEntityId).getsObjectType());
	            //if( String.valueOf(typeEntity).equalsIgnoreCase('Law_Item__c') ){
	        system.debug('li.ContentDocumentLinks.size() '+ li.ContentDocumentLinks.size());
	        for(ContentDocumentLink cdl : li.ContentDocumentLinks){
	        	String typeEntity = String.valueOf((cdl.LinkedEntityId).getsObjectType());
	        	if(!docSet.contains(cdl.ContentDocumentId) && typeEntity.equalsIgnoreCase('Law_Item__c')){
	        		latestVers.add(cdl.ContentDocument.LatestPublishedVersionId);
	        	}
            	docSet.add(cdl.ContentDocumentId);
            	if(!docsByLawItem.containsKey(cdl.LinkedEntityId)){
            		docsByLawItem.put(cdl.LinkedEntityId, new Set<Id>());
            	}
                docsByLawItem.get(cdl.LinkedEntityId).add(cdl.ContentDocumentId);
	            //}
	        }
	    }
	    system.debug('docsByLawItem '+ docsByLawItem);
	    system.debug('docSet '+ docSet);

	    Map<Id, Map<String,String>> RiaFile = new Map<Id, Map<String,String>>();
	    Map<Id, Map<String,String>> MainFile = new Map<Id, Map<String,String>>();
	    Map<Id, Map<String,String>> CombFile = new Map<Id, Map<String,String>>();
	    List<ContentDistribution> cdlist = [Select Id, Name, DistributionPublicURL, ContentDocumentId, ContentVersion.File_Type__c, ContentVersion.FileExtension, RelatedRecordId, ContentVersionId, ContentVersion.ContentDocumentId, ContentVersion.IsRia__c, ContentVersion.Main__c  
	    											From ContentDistribution 
	    												where ContentVersionId  In : latestVers];
	    
		system.debug('cdlist.size() '+ cdlist.size());
	    Set<Id> docsCheck = new Set<Id>();

        for(ContentDistribution cd : cdlist){
        	system.debug('cd.DistributionPublicURL '+cd.DistributionPublicURL);
        	system.debug('cd.ContentDocumentId '+cd.ContentDocumentId);
        	system.debug('cd.ContentVersion.ContentDocumentId '+cd.ContentVersion.ContentDocumentId);
        	system.debug('cd.RelatedRecordId '+cd.RelatedRecordId);
        	system.debug('cd.ContentVersion.IsRia__c '+cd.ContentVersion.IsRia__c);
        	system.debug('cd.ContentVersion.Main__c '+cd.ContentVersion.Main__c);
        	system.debug('cd.ContentVersion.Main__c '+cd.ContentVersion.File_Type__c);
        	docsCheck.add(cd.ContentVersion.ContentDocumentId);
        	if(cd.ContentVersion.File_Type__c != null && cd.ContentVersion.File_Type__c.equalsIgnoreCase('Main File')){
	        	if(!MainFile.containsKey(cd.ContentVersion.ContentDocumentId)){
	        		MainFile.put(cd.ContentVersion.ContentDocumentId,  new Map<String,String>());
	        	}
				if(!mainFilesCDIDToURLAndFileExtensions.containsKey(cd.ContentVersion.ContentDocumentId)){
	        		mainFilesCDIDToURLAndFileExtensions.put(cd.ContentVersion.ContentDocumentId,  new Map<String,String>());
	        	}
        		MainFile.get(cd.ContentVersion.ContentDocumentId).put(cd.DistributionPublicURL, cd.Name);
				mainFilesCDIDToURLAndFileExtensions.get(cd.ContentVersion.ContentDocumentId).put(cd.DistributionPublicURL, cd.ContentVersion.FileExtension);
        		continue;
        	}else if(cd.ContentVersion.File_Type__c != null && cd.ContentVersion.File_Type__c.equalsIgnoreCase('RIA File')){
	        	if(!RiaFile.containsKey(cd.ContentVersion.ContentDocumentId)){
	        		RiaFile.put(cd.ContentVersion.ContentDocumentId,  new Map<String,String>());
	        	}
				if(!riaFilesCDIDToURLAndFileExtensions.containsKey(cd.ContentVersion.ContentDocumentId)){
	        		riaFilesCDIDToURLAndFileExtensions.put(cd.ContentVersion.ContentDocumentId,  new Map<String,String>());
	        	}
	        	RiaFile.get(cd.ContentVersion.ContentDocumentId).put(cd.DistributionPublicURL, cd.Name);
				riaFilesCDIDToURLAndFileExtensions.get(cd.ContentVersion.ContentDocumentId).put(cd.DistributionPublicURL, cd.ContentVersion.FileExtension);
	        	continue;
        	}else if(cd.ContentVersion.File_Type__c != null && cd.ContentVersion.File_Type__c.equalsIgnoreCase('Combined Version File')){
	        	if(!CombFile.containsKey(cd.ContentVersion.ContentDocumentId)){
	        		CombFile.put(cd.ContentVersion.ContentDocumentId,  new Map<String,String>());
	        	}
				if(!combFilesCDIDToURLAndFileExtensions.containsKey(cd.ContentVersion.ContentDocumentId)){
	        		combFilesCDIDToURLAndFileExtensions.put(cd.ContentVersion.ContentDocumentId,  new Map<String,String>());
	        	}
	        	CombFile.get(cd.ContentVersion.ContentDocumentId).put(cd.DistributionPublicURL, cd.Name);
				combFilesCDIDToURLAndFileExtensions.get(cd.ContentVersion.ContentDocumentId).put(cd.DistributionPublicURL, cd.ContentVersion.FileExtension);
	        	continue;
        	}

        	if(!docs2Links.containsKey(cd.ContentVersion.ContentDocumentId)){
        		docs2Links.put(cd.ContentVersion.ContentDocumentId,  new Map<String,String>());
        	}

        	docs2Links.get(cd.ContentVersion.ContentDocumentId).put(cd.DistributionPublicURL, cd.Name);
        }

        system.debug('RiaFile '+ RiaFile);
        system.debug('MainFile '+ MainFile);
        system.debug('CombFile '+ CombFile);
        system.debug('docs2Links '+ docs2Links);

        for(Id liID :docsByLawItem.keySet()){
        	if(!LawItem2Links.containsKey(liID)){
        		LawItem2Links.put(liID,new Map<String,String>());	
        	}
        	if(!LawItem2MainLinks.containsKey(liID)){
        		LawItem2MainLinks.put(liID,new Map<String,String>());	
        	}
        	if(!LawItem2RiaLinks.containsKey(liID)){
        		LawItem2RiaLinks.put(liID,new Map<String,String>());	
        	}
        	if(!LawItem2CombLinks.containsKey(liID)){
        		LawItem2CombLinks.put(liID,new Map<String,String>());	
        	}
			if(!mainFilesLawItemToURLAndFileExtensions.containsKey(liID)){
        		mainFilesLawItemToURLAndFileExtensions.put(liID,new Map<String,String>());	
        	}
			if(!riaFilesLawItemToURLAndFileExtensions.containsKey(liID)){
        		riaFilesLawItemToURLAndFileExtensions.put(liID,new Map<String,String>());	
        	}
			if(!combFilesLawItemToURLAndFileExtensions.containsKey(liID)){
        		combFilesLawItemToURLAndFileExtensions.put(liID,new Map<String,String>());	
        	}
	        for(Id docId : docsByLawItem.get(liID)){
        		if(docs2Links.containsKey(docId)){
	        		LawItem2Links.get(liID).putAll(docs2Links.get(docId));
	        	}
        		if(RiaFile.containsKey(docId)){
	        		LawItem2RiaLinks.get(liID).putAll(RiaFile.get(docId));
	        	}
	        	if(MainFile.containsKey(docId)){
	        		LawItem2MainLinks.get(liID).putAll(MainFile.get(docId));
	        	}
	        	if(CombFile.containsKey(docId)){
	        		LawItem2CombLinks.get(liID).putAll(CombFile.get(docId));
	        	}
				if(mainFilesCDIDToURLAndFileExtensions.containsKey(docId)){
	        		mainFilesLawItemToURLAndFileExtensions.get(liID).putAll(mainFilesCDIDToURLAndFileExtensions.get(docId));
	        	}
				if(riaFilesCDIDToURLAndFileExtensions.containsKey(docId)){
	        		riaFilesLawItemToURLAndFileExtensions.get(liID).putAll(riaFilesCDIDToURLAndFileExtensions.get(docId));
	        	}
				if(combFilesCDIDToURLAndFileExtensions.containsKey(docId)){
	        		combFilesLawItemToURLAndFileExtensions.get(liID).putAll(combFilesCDIDToURLAndFileExtensions.get(docId));
	        	}
	        }
        }

		for(Id li : LawItemIds ){

			//for(String lang : new List<String>{'Hebrew', 'Arabic'} ){	
				/// demo params
				Map<String,Object> EmailParams = new Map<String,Object>();

																		/*'from_email' => 'dontReply@gmail.com' ,
																		'from_name' => 'מערכת דיוור משרד המשפטים'/* ,
																		'isFromUser' => 'false'*/ /*,
																		'from_user_id' => 'Test from_user_id' *//*,
																		'email_template_id' => 'Test email_template_id'*/
																														//};
				
				Map<String,String> Links = new Map<String,String>();
				if(LawItem2Links.containsKey(li)){
					Links.putAll(LawItem2Links.get(li));
				}
		        system.debug('Links '+ Links);
		        system.debug('LawItem2MainLinks.get('+li+') '+ LawItem2MainLinks.get(li));
		        system.debug('LawItem2RiaLinks.get('+li+') '+ LawItem2RiaLinks.get(li));

				EmailParams.putAll(Util_Email_Templates.sendEmail(li, lang, LawItem2MainLinks.get(li), LawItem2RiaLinks.get(li), LawItem2CombLinks.get(li), mainFilesLawItemToURLAndFileExtensions.get(li), riaFilesLawItemToURLAndFileExtensions.get(li), combFilesLawItemToURLAndFileExtensions.get(li))) ;

				if(pardotLists == null || pardotLists.isEmpty()){
					Utils.sendEmailsToLists(ApiKey, null, EmailParams);
				} else {
					for(String thisList : pardotLists){
						if(opperation.equalsIgnoreCase('immediate')){
							Utils.sendEmailsToLists(ApiKey, thisList, EmailParams);
							system.debug('EmailParams '+ EmailParams);
						}
					}
				}
				//}
			//}
		}
	}
	
}