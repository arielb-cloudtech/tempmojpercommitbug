public without sharing class UserTriggerHandler {
	/* public UserTriggerHandler() {

	}

	private Map<Id, Id> collectCommunityContacts(Map<Id, User> newMap, Profile communityProfile){
		Map<Id, Id> toReturn = new Map<Id, Id>();
		for(User u : newMap.values()){
			if(u.profileId == communityProfile.Id){
				toReturn.put(u.Id, u.ContactId);
			}
		}
		return toReturn;
	}

	private Map<Id, Contact> getcommunityContacts(List<Id> contactIds){
		Map<Id, Contact> toReturn = new Map<Id, Contact>([SELECT Id, User__c, Active_User__c, Role__c FROM contact WHERE ID IN :contactIds]);
		return toReturn;

	}

	private void updateCommunityContacts(Map<Id, Contact> communityContacts, Map<Id, Id> userToContact, Profile communityProfile){
		List<Id> contactsToUpdate = new List<Id>();
		for(Id userId : userToContact.keySet()){
			if(userId != null && userToContact.get(userId) != null){
				Id contactId = userToContact.get(userId);
				if(communityContacts.containsKey(contactId) && communityContacts.get(contactId) != null){
					System.debug('~~~ If: ');
					communityContacts.get(contactId).User__c = userId;
					communityContacts.get(contactId).Active_User__c = true;
					communityContacts.get(contactId).Role__c = communityProfile.Name;

					contactsToUpdate.add(contactId);
				}
			}
		}
		System.debug('~~~ communityContacts.values(): ' + communityContacts.values());
		//UserTriggerHandler.futureUpdateContact(JSON.serialize(communityContacts.values()));

		//System.enqueueJob(new contactToUpdateQueueable(communityContacts.values()));
		//update communityContacts.values();
	}

	public void updateCommunityContactFields(Map<Id, User> newMap){
		// Set<Id> communityContactIds = collectCommunityContacts(newMap);
		System.debug('~~~ newMap: ' + newMap);
		Profile communityProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Customer Community MOJ' LIMIT 1];
		System.debug('~~~ communityProfile: ' + communityProfile);
		Map<Id, Id> communityUserToContact = collectCommunityContacts(newMap, communityProfile);
		Map<Id, Contact> communityContacts = getcommunityContacts(communityUserToContact.values());
		//updateCommunityContacts(communityContacts, communityUserToContact, communityProfile);
		System.enqueueJob(new Queuable_updateContacts(communityContacts, communityUserToContact, communityProfile));

	}

	@future
	public static void futureUpdateContact(String contactsJson){
		upsert (List<Contact>)JSON.deserialize(contactsJson, List<Contact>.class);
	} */
}
