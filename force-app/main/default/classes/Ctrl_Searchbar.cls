public with sharing class Ctrl_Searchbar {
	public Ctrl_Searchbar() {
		
	}

    @AuraEnabled
    public static String RetrevieDataSearch(String nameSearch){
		List<Law_Item__c> LawItems = new List<Law_Item__c>();
		if(nameSearch == 'all'){
        	 LawItems = [SELECT Document_Brief__c, Law_Item_Name__c, Status__c, Account_Responsibility__r.Name, Last_Date_for_Comments__c, Publish_Date__c,Type__c,RecordTypeId 
        	FROM Law_Item__c WHERE Status__c != null limit 3];
		}else{
        	 LawItems = [SELECT Document_Brief__c, Law_Item_Name__c, Status__c, Account_Responsibility__r.Name, Last_Date_for_Comments__c,Publish_Date__c,Type__c,RecordTypeId 
        	FROM Law_Item__c WHERE Status__c != null AND Law_Item_Name__c LIKE:nameSearch+'%'  limit 3];
		}

		AggregateResult[] LawItemsCom2 = [SELECT Related_to_Law_Item__r.Id, COUNT(Related_to_Law_Item__c) FROM Comment__c WHERE  Related_to_Law_Item__r.Id in:LawItems GROUP BY Related_to_Law_Item__r.Id];
        system.debug('eeeeeeeeeeeeeeeee'+LawItemsCom2);
		Map<String,String> CmmCountTable = new Map<String,String>();
        List<ItemCard> CardList = new List<ItemCard>();
        for (AggregateResult a : LawItemsCom2) {
            if(a.Id != null)
            {
                CmmCountTable.put(String.valueOf(a.get('Id')), String.valueOf(a.get('expr0')));
            }
        }
        system.debug('eeeeeeeeeeeeeeeee'+LawItems);
        for (Law_Item__c item : LawItems) 
        {
            ItemCard yay = new ItemCard(item,CmmCountTable.get(item.Id));
            CardList.add(yay);
        }
        return  json.serialize(CardList);
    }
    
    public class ItemCard{

          @AuraEnabled public Law_Item__c Itemcard;
          @AuraEnabled public String CommCont;
        
        public ItemCard(Law_Item__c Itemcard,String CommCont){
            this.Itemcard = Itemcard;
            this.CommCont = CommCont;
        }
    }
}