public with sharing class AccountTriggerFutureHandler {
	public AccountTriggerFutureHandler() {
		
	}
	private class GlobalValueSetWrapper {
		public List<Records> records; 
	}
	private class Records {
		public String Id;
	}

	@future(callout=true)
	public static void updateFuture2(String vals){
		String pickListId = getGlobalPicklistId('Units');
		if(String.isNotBlank(pickListId)){
			HttpRequest req = new HttpRequest();
				req.setBody(vals);
				req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
				req.setHeader('Content-Type', 'application/json');      
				req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v42.0/tooling/sobjects/GlobalValueSet/'+ pickListId +'?_HttpMethod=PATCH');
				req.setMethod('POST');
				//req.setHeader('X-HTTP-Method-Override','PATCH');
				Http httpreq = new Http();
				HttpResponse res  = httpreq.send(req);
				System.debug('+++res.getStatusCode(): ' + res.getStatusCode());
				System.debug('+++res.getBody(): ' + res.getBody());
		}
	}

	//this quries the SF environment for the Unit picklist values Id and deserialize the json respone to get the id 
	private static String getGlobalPicklistId(String picklistName){
		String valueSetId;
		HttpRequest req = new HttpRequest();
		req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
		req.setHeader('Content-Type', 'application/json');      
		req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v42.0/tooling/query/?q=select+id+from+globalvalueset+Where+developername=\''+picklistName+'\'');
		req.setMethod('GET');
		Http httpreq = new Http();
		HttpResponse res  = httpreq.send(req);
		GlobalValueSetWrapper wrp = new GlobalValueSetWrapper();
		if(res.getStatusCode() >= 200 && res.getStatusCode() <= 202){
			String respBody = res.getBody();
			system.debug(respBody);
			if(respBody != null){
				try {
				   wrp = (GlobalValueSetWrapper)JSON.deserialize(respBody, GlobalValueSetWrapper.class);
				   if(wrp != null && wrp.records != null && !wrp.records.isEmpty() ){
				   		system.debug('+++wrp ' + wrp.records[0].Id);
				   		valueSetId = wrp.records[0].Id;
				   }
				} catch (Exception ex) {
					System.Debug('Exception: '+ ex.getMessage() + ex.getLineNumber());
				}
			}
		}

		if(String.isNotBlank(valueSetId)){
			return valueSetId;
		}
		return null;
	}

}