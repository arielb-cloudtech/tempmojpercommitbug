@isTest
public with sharing class Test_GetKnessetData {

    public class Mock implements HttpCalloutMock{
  		public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            if(req.getEndpoint().containsIgnoreCase('KNS_IsraelLaw')){
                System.debug('KNS_IsraelLaw');
                res.setBody('{"odata.metadata":"http://knesset.gov.il/Odata/ParliamentInfo.svc/$metadata#KNS_IsraelLaw","value":[{"IsraelLawID":2000193,"KnessetNum":9,"Name":"edgfe","IsBasicLaw":false,"IsFavoriteLaw":false,"PublicationDate":"1980-12-01T00:00:00","LatestPublicationDate":"2019-01-06T00:00:00","IsBudgetLaw":null,"LawValidityID":6079,"LawValidityDesc":"1","ValidityStartDate":null,"ValidityStartDateNotes":null,"ValidityFinishDate":null,"ValidityFinishDateNotes":null,"LastUpdatedDate":"2016-03-10T15:16:30.147"}]}');
                System.debug(res.getBody());
            } else if(req.getEndpoint().containsIgnoreCase('KNS_Law')) {
                System.debug('KNS_Law');
                res.setBody('{"odata.metadata":"http://knesset.gov.il/Odata/ParliamentInfo.svc/$metadata#KNS_LawBinding","value":[{"LawBindingID":41753,"LawID":165164,"IsraelLawID":2000023,"ParentLawID":2000023,"LawTypeID":2,"LawParentTypeID":6003,"BindingType":6013,"BindingTypeDesc":"1","PageNumber":"99","AmendmentType":6011,"AmendmentTypeDesc":"1","LastUpdatedDate":"2019-09-11T16:28:09.72"}]}');
            } else if(req.getEndpoint().containsIgnoreCase('KNS_LawBinding')) {
                System.debug('KNS_LawBinding');
                res.setBody(('{"odata.metadata":"http://knesset.gov.il/Odata/ParliamentInfo.svc/$metadata#KNS_LawBinding","value":[{"LawBindingID":41753,"LawID":165164,"IsraelLawID":2000023,"ParentLawID":2000023,"LawTypeID":2,"LawParentTypeID":6003,"BindingType":6013,"BindingTypeDesc":"1","PageNumber":"99","AmendmentType":6011,"AmendmentTypeDesc":"1","LastUpdatedDate":"2019-09-11T16:28:09.72"}]}'));
            } else if(req.getEndpoint().containsIgnoreCase('KNS_Bill')) {
                System.debug('KNS_Bill');
                res.setBody(('{"odata.metadata":"http://knesset.gov.il/Odata/ParliamentInfo.svc/$metadata#KNS_LawBinding","value":[{"LawBindingID":41753,"LawID":165164,"IsraelLawID":2000023,"ParentLawID":2000023,"LawTypeID":2,"LawParentTypeID":6003,"BindingType":6013,"BindingTypeDesc":"1","PageNumber":"99","AmendmentType":6011,"AmendmentTypeDesc":"1","LastUpdatedDate":"2019-09-11T16:28:09.72"}]}'));
            } else  if(req.getEndpoint().containsIgnoreCase('KNS_IsraelLawClassificiation')) {

            } else {

            }

            return res;
        }

    }

    @isTest
    public static void test(){
        Test.setMock(HttpCalloutMock.class, new Mock());
        new Sch_getKnessetData();
        Test.startTest();
        Queue_IntegrateWithKnesset integrateWithKnessetClass = new Queue_IntegrateWithKnesset();
        ID jobID = System.enqueueJob(integrateWithKnessetClass);
        // System.schedule('jobname', '0 0 15 ? * SUN,MON,TUE,WED,THU,FRI *', new Sch_getKnessetData());
        Test.stopTest();
       
    }
}
