public without sharing class Ctrl_Community_Tzkirim {
	public Ctrl_Community_Tzkirim() {
		
	}

	public class customException extends Exception {}

	@AuraEnabled
	public static ReturnObjects.ItemCard getLawItem( String lawItemId, 
													 String draftId) {

		Law_Item__c lawItem = [SELECT Id, Last_Date_for_Comments__c, Law_Item_Name__c, CreatedDate ,Publish_Date__c, Publisher_Account__r.Name,
									Document_Brief__c, Law__c, Type__c, RecordType.DeveloperName,
									(SELECT Id, Law_Item__c, Law__c FROM Related_to_Laws__r),
									(SELECT Id, Name, Classification_Name__c  FROM Law_Item_Classifications__r)
								FROM Law_Item__c WHERE Id = :lawItemId LIMIT 1];

		if(lawItem == null){
			return null;
		}
		
		Set<Id> lawIds = new Set<Id>();
		for(Related_to_Law__c rtl : lawItem.Related_to_Laws__r){
			lawIds.add(rtl.Law__c);
		}

		Id amendmentToLaw = null;
		
		if((lawItem.RecordType.DeveloperName == 'Memorandum_of_Law' && lawItem.Type__c == 'Legislation Amendment')
			|| (lawItem.RecordType.DeveloperName == 'Secondary_Legislation_Draft' && lawItem.Type__c == 'Secondary Legislation Amendment')
			|| (lawItem.RecordType.DeveloperName == 'Support_Test_Draft' && lawItem.Type__c == 'Support Test Amendment Draft')){
				amendmentToLaw = lawItem.law__c;
		}

		List<Law__c> lawList = [SELECT Id, Law_Name__c, LawID__c FROM Law__c WHERE Id IN :lawIds OR Id = :amendmentToLaw];
		List<WrapperLaw> laws = new List<WrapperLaw>();
		WrapperLawAmendment lawAmendment = null;
		for(Law__c law : lawList){
			if(law.Id == amendmentToLaw){
				// lawAmendment.mName = law.Law_Name__c;
				// lawAmendment.mLawId = law.LawID__c;
				lawAmendment = new WrapperLawAmendment(law.Law_Name__c, law.LawID__c);
			}else{
				laws.add(new WrapperLaw(law.Law_Name__c, law.LawID__c));
			}
		}

		ReturnObjects.LawItemComment draft = null;
		if(draftId != null){

			// protect comment from unauthorize editing
			Id userId = UserInfo.getUserId();
			User currentUser = [SELECT ContactId FROM USER WHERE Id = :userId LIMIT 1];
			Id contactId = currentUser.ContactId;

			List<String> draftWhere = new List<String>{
				'Id = :draftId',
				'Comment_Status__c =\'Draft\'',
				'Comment_By__c = :contactId'
			};
			List<Comment__c> DBComments = Database.query(getCommentQuery(draftWhere));
			if( DBComments.size() > 0 ) {
				Map<Id, List<WrapperDocument>> draftFilesList = getDocumentsForIds( new Set<Id> { DBComments[0].Id } );
				draft = new ReturnObjects.LawItemComment( DBComments[0], draftFilesList, false );
			}
		}

		Map<Id, List<WrapperDocument>> entityToCdl = getDocumentsForIds(new Set<Id>{lawItemId});
		
		List<WrapperDocument> sortedDocs;

		if(entityToCdl.get(lawItemId) == null || entityToCdl.get(lawItemId).isEmpty()){
			sortedDocs = new List<WrapperDocument>();
		} else {
			sortedDocs = entityToCdl.get(lawItemId);
			sortedDocs = setDocumentsDisplayName(sortedDocs, lawItem.RecordType.DeveloperName);
			sortedDocs = sortDocuments(sortedDocs);
		}

		ReturnObjects.ItemCard wli =  new ReturnObjects.ItemCard( 
							lawItem,
							lawAmendment,
							draft,
							sortedDocs,
							laws );
		return wli;
	}
	
	public static String getCommentQuery(List<String> queryWhere){
		List<String> queryFields = new List<String> {
			'Id',
			'OwnerId',
			'Comment_By__c',
			'Related_to_Law_Item__c',
			'CreatedDate',
			'Comment_Status__c',
			'Comment_Text__c',
			'Comment_By__r.Name',
			'Comment_By__r.FirstName',
			'Comment_By__r.LastName',
			'Comment_By__r.Company__c',
			'Comment_By__r.User__r.Role__c',
			'Is_Private_Comment__c',
			'CreatedById',
			'Related_to_Law_Item__r.RecordTypeId',
			'Related_to_Law_Item__r.Publisher_Account__r.Name',
			'Related_to_Law_Item__r.Document_Brief__c',
			'Related_to_Law_Item__r.Last_Date_for_Comments__c',
			'Related_to_Law_Item__r.Law_Item_Name__c',
			'Related_to_Law_Item__r.Publish_Date__c',
			'Related_to_Law_Item__r.CreatedDate',
			'Related_to_Law_Item__r.Account_Responsibility__r.Name',
			'Related_to_Law_Item__r.Id'
		};
		queryWhere.add('Sub_Comment__c =  null');
		queryWhere.add('Comment_Status__c !=\'Deleted\'');

		String commentResponseFields = '(SELECT Id, Reply__c,Read__c, Display_Reply__c, CreatedDate FROM Ministry_Responses__r ORDER BY CreatedDate ASC) ';
		String commentSectionFields = '(SELECT Id, Comment_Text__c ,Bullet_Number__c FROM Comments1__r) ';

		String query =  'SELECT '+String.join( queryFields , ', ') +
								', '+commentResponseFields+ ' '+
								', '+commentSectionFields+ ' '+
						'FROM Comment__c '+
					    'WHERE '+ String.join( queryWhere , ' AND ') + ' '+
					    'ORDER BY CreatedDate DESC LIMIT 5';
		
		return query;
	  }

	@AuraEnabled
	public static commentsResponse getComments( String lawItemId, List<String> excludedCommentIds,  String draftId , String requireComment ){

		commentsResponse response = new commentsResponse(lawItemId);
		
		Set<Id> contacts = new Set<Id>{ userinfo.getUserId() };

		for ( User us : [ Select Id, ContactId, Role__c, Company__c 
						  From User 
						  Where Id IN :contacts ] ) {
			contacts.add(us.ContactId);
		}

		List<Comment__c> DBComments = new List<Comment__c>();

		if ( requireComment != '' ) {
			Set<String> excludedComments = new Set<String>(excludedCommentIds);
			if( ! excludedComments.contains(lawItemId) ) {
				String requireQuery = getCommentQuery(new List<String> {'Id = :requireComment'});
				for ( Comment__c comment : Database.query(requireQuery) ) {
					excludedCommentIds.add(comment.Id);
					DBComments.add(comment);
				}
			}
		}

		List<String> queryWhere = new List<String> {
			'Related_to_Law_Item__c = :lawItemId',
			'Id NOT IN :excludedCommentIds',
			'( Is_Private_Comment__c = false AND Display_Comment__c = true AND Comment_Status__c != \'Draft\') '
		};

		String query = getCommentQuery(queryWhere);


		for(Comment__c comment :  Database.query(query)){
			DBComments.add(comment);
		}

		Set<Id> LinkedEntityIds = new Set<Id>();
		Set<Id> contctIds = new Set<Id>();
		for(Comment__c comment : DBComments){
			LinkedEntityIds.add(comment.Id);
			contctIds.add(comment.Comment_By__c);
			for(Comment_Reply__c reply : comment.Ministry_Responses__r){
				LinkedEntityIds.add(reply.Id);
			}
		}

		Map<Id, List<WrapperDocument>> entityToCdl = getDocumentsForIds(LinkedEntityIds);

		for(Comment__c comment : DBComments){
			
			response.comments.add(new ReturnObjects.LawItemComment( comment, entityToCdl, false ) );
		}
		response.message = '';
		response.setSuccessStatus();
		return response;
	}
	
	public class commentsResponse extends ReturnObjects.AjaxMessage {
         @AuraEnabled public List<ReturnObjects.LawItemComment> comments = new  List<ReturnObjects.LawItemComment>();
         @AuraEnabled public Integer total = 0;

		 public commentsResponse(String lawItemId) {
			Id userId = userinfo.getUserId();
			this.total = database.countQuery(
					'SELECT count() '+
					'FROM Comment__c '+
					'WHERE '+
					'Related_to_Law_Item__c =  \''+lawItemId+'\' '+
					'AND Sub_Comment__c =  null '+
					'AND Is_Private_Comment__c = false '+
					'AND Display_Comment__c = true '+
					'AND Comment_Status__c != \'Draft\'');
		 }
    }

	public static Map<Id, List<WrapperDocument>> getDocumentsForIds( Set<Id> ContantDocumentIds ) {
		if(ContantDocumentIds.size()==0){
			return null;
		}
		
		List<ContentDocumentLink> cdlList = [	SELECT  Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId,
														ContentDocument.LatestPublishedVersion.File_Type__c, ContentDocument.FileExtension, ContentDocument.ContentSize, 
														ContentDocument.LatestPublishedVersion.Title, LinkedEntityId, ContentDocument.LatestPublishedVersion.Description
												FROM ContentDocumentLink
												WHERE LinkedEntityId IN :ContantDocumentIds ];

		Map<Id, List<WrapperDocument>> entityToCdl = new Map<Id, List<WrapperDocument>>();
		for(ContentDocumentLink cdl : cdlList){

			if(!entityToCdl.containsKey(cdl.LinkedEntityId)){
				entityToCdl.put(cdl.LinkedEntityId, new List<WrapperDocument>());
			}
	
			entityToCdl.get(cdl.LinkedEntityId).add( new WrapperDocument(cdl) );
		}
		return entityToCdl;
	}


	private static List<WrapperDocument> setDocumentsDisplayName(List<WrapperDocument> docList, String lawItemRecordType){
	
		for(WrapperDocument doc : docList){
			if(doc.mFileType == 'Main File'){
				if(lawItemRecordType == 'Memorandum_of_Law'){
					doc.mDisplayName = 'תזכיר החוק';
				}else if(lawItemRecordType == 'Secondary_Legislation_Draft'){
					doc.mDisplayName = 'טיוטת חקיקת משנה';
				}else if(lawItemRecordType == 'Support_Test_Draft'){
					doc.mDisplayName = 'טיוטת מבחן התמיכה';
				}else if(lawItemRecordType == 'Ministry_Directive_or_Procedure_Draft'){
					doc.mDisplayName = 'טיוטת הנוהל או ההנחיה';
				}
			}else{
				if(doc.mFileType == 'Combined Version File'){
					doc.mDisplayName = 'נוסח משולב (לא רשמי)';
				}else if(doc.mFileType == 'RIA File'){
					doc.mDisplayName = 'קובץ RIA';
				}else if(doc.mFileType == 'Extra File'){
					if(String.isBlank(doc.mDescription)){
						doc.mDisplayName = doc.name;
					}else{
						doc.mDisplayName = doc.mDescription;
					}
				}
			}
		}
		return docList;
	}

	private static List<WrapperDocument> sortDocuments(List<WrapperDocument> docList){
		List<WrapperDocument> type_1 = new List<WrapperDocument>();
		List<WrapperDocument> type_2 = new List<WrapperDocument>();
		List<WrapperDocument> type_3 = new List<WrapperDocument>();
		List<WrapperDocument> type_4 = new List<WrapperDocument>();
		for(WrapperDocument doc : docList){
			if(doc.mFileType == 'Main File'){
				type_1.add(doc);
			}else if(doc.mFileType == 'Combined Version File'){
				type_2.add(doc);
			}else if(doc.mFileType == 'RIA File'){
				type_3.add(doc);
			}else if(doc.mFileType == 'Extra File'){
				type_4.add(doc);
			}
		}
		
		docList.clear();

		docList.addAll(type_1);
		docList.addAll(type_2);
		docList.addAll(type_3);
		docList.addAll(type_4);

		return docList;
	}

	@AuraEnabled
	public static void deleteDocument(String documentId){
		ContentDocument cd = [SELECT Id FROM ContentDocument WHERE Id = :documentId LIMIT 1];
		if(cd != null){
			delete cd;
		}
	}

	@AuraEnabled
	public static ReturnObjects.AjaxMessage updateComment(  String lawItemId, String commentId, String commentBody, Boolean isPrivate, 
															String commentStatus, List< Map<String,String> > commentSections ){
		ReturnObjects.AjaxMessage response = new ReturnObjects.AjaxMessage();						 
		User currentUser = [SELECT ContactId, Name FROM User WHERE Id = :userinfo.getUserId() LIMIT 1];
		
		Comment__c parentComment = new Comment__c(	Id = commentId != '' ? commentId : null,
													Related_to_Law_Item__c = lawItemId,
													Comment_Text__c = commentBody,
													Comment_Status__c = commentStatus,
													Display_Comment__c = true,
													Comment_By__c = currentUser.ContactId,
													Is_Private_Comment__c = isPrivate
												);
		upsert parentComment;
		commentId = parentComment.Id;

		List<Comment__c> subComments = new List<Comment__c>();
		Set<Id> commentsToDelete = (new Map<Id,Comment__c>([SELECT Id FROM  Comment__c WHERE Sub_Comment__c = :commentId ])).keySet();
		
		for( Map<String,String> commentSection: commentSections){
			Id commentSectionId = commentSection.get('Id')==''?null:commentSection.get('Id');
			if( commentSectionId!= null ){
				commentsToDelete.remove(commentSectionId);
			}
			subComments.add(
				new Comment__c(	
						Id = commentSectionId,	
						Related_to_Law_Item__c = lawItemId,
						Bullet_Number__c = commentSection.get('Section'),
						Comment_Text__c = commentSection.get('Comment'),
						Comment_Status__c = commentStatus,
						Sub_Comment__c = commentId,
						Display_Comment__c = true,
						Comment_By__c = currentUser.ContactId,
						Is_Private_Comment__c = isPrivate
					)
				);
		}
		
		// delete comments
		List<Comment__c> itemsToDelete = [ SELECT id FROM Comment__c WHERE id IN :commentsToDelete];
		if ( itemsToDelete.size() > 0 ) {
			delete itemsToDelete;
		}
		upsert subComments;
		
		response.message = '';
		response.setSuccessStatus();
		return response;
	}

	public class WrapperDocument{
		@AuraEnabled public String Id;
		@AuraEnabled public String attachmentId;
		@AuraEnabled public String mVersionId;
		@AuraEnabled public String mFileType;
		@AuraEnabled public String name;
		@AuraEnabled public String type;
		@AuraEnabled public String mDescription;
		@AuraEnabled public String mDisplayName;
		@AuraEnabled public Integer size;
		@AuraEnabled public Integer uploadPercent;

		public WrapperDocument( ContentDocumentLink documentLink ){
			this.Id = documentLink.ContentDocumentId;
			this.attachmentId = documentLink.ContentDocumentId;
			this.mVersionId = documentLink.ContentDocument.LatestPublishedVersionId;
			this.mFileType = documentLink.ContentDocument.LatestPublishedVersion.File_Type__c;
			this.name = documentLink.ContentDocument.LatestPublishedVersion.Title;
			this.type = documentLink.ContentDocument.FileExtension != 'pdf'?'word':'pdf';
			this.size = documentLink.ContentDocument.ContentSize;
			this.mDescription = documentLink.ContentDocument.LatestPublishedVersion.Description;
			this.uploadPercent = 100;
		}
	}
	
	public class WrapperLaw{
		@AuraEnabled public String mName;
		@AuraEnabled public String mLawId;

		public WrapperLaw(String name, String lawId){
			mName = name;
			mLawId = lawId;
		}
	}
	
	public class WrapperLawAmendment{
		@AuraEnabled public String mName;
		@AuraEnabled public String mLawId;

		public WrapperLawAmendment(String name, String lawId){
			mName = name;
			mLawId = lawId;
		}
	}
	public class WrapperDraft{
		@AuraEnabled public String mId;
		@AuraEnabled public String mBody;
		@AuraEnabled public String mIsPrivate;
		@AuraEnabled public List<WrapperDocument> mFiles;

		public WrapperDraft(String draftId, String draftBody, Boolean isPrivate, List<WrapperDocument> draftFiles){
			mId = draftId;
			mBody = draftBody;
			if(isPrivate){
				mIsPrivate = 'true';
			}else {
				mIsPrivate = 'false';
			}
			mFiles = draftFiles;
		}
	}

	public class WrapperCommentReply{
		@AuraEnabled public String mId;
		@AuraEnabled public String mBody;
		@AuraEnabled public Boolean bHaveFiles = false;
		@AuraEnabled public DateTime mCreatedDate;
		@AuraEnabled public Boolean bNew = false;
		@AuraEnabled public List<WrapperDocument> mFiles;


		public WrapperCommentReply(String recordId, String body, DateTime createdDate, Boolean bNotReaded, List<WrapperDocument> files){
			mId = recordId;
			mBody = body;
			mCreatedDate = createdDate;
			bNew = false;
			if (!bNotReaded){
				bNew = true;
			}
			mFiles = files;

		}
	}
}