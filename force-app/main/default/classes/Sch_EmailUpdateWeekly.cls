global class Sch_EmailUpdateWeekly implements Schedulable {
	global void execute(SchedulableContext sc) {
		Batch_SendPeriodicalyUpdateEmails b = new Batch_SendPeriodicalyUpdateEmails('7','Hebrew');
		// b.LastDays = '7';
		// b.Language = 'Hebrew';
		database.executeBatch(b);

		// Batch_SendPeriodicalyUpdateEmails c = new Batch_SendPeriodicalyUpdateEmails('7','Arabic');
		// c.LastDays = '7';
		// c.Language = 'Arabic';
		// database.executeBatch(c);
	}
}