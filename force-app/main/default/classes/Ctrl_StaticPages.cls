public without sharing class Ctrl_StaticPages 
{
	public Ctrl_StaticPages() 
	{
		
	}

	@AuraEnabled    
	public static Page_Parent__c getFAQData	()
	{
		Page_Parent__c pagePar = [SELECT Id, Page_Name__c, (Select Id, Body_Arabic__c, Body_Hebrew__c, Header_Arabic__c, Header_Hebrew__c, Order__c From Page_Childs__r Order By Order__c Asc) 
										From Page_Parent__c WHERE Name = 'Questions & Answers' Limit 1];
		System.debug('~~~pagePar: ' + pagePar);
		//return JSON.serialize(pagePar);
		return pagePar;

	}

	@AuraEnabled    
	public static Page_Parent__c getAboutData()
	{
		Page_Parent__c pagePar = [SELECT Id, Page_Name__c, (Select Id, Body_Arabic__c, Body_Hebrew__c, Header_Arabic__c, Header_Hebrew__c, Order__c From Page_Childs__r Order By Order__c Asc) 
										From Page_Parent__c WHERE Name = 'Main Page' Limit 1];
		System.debug('~~~pagePar: ' + pagePar);
		//return JSON.serialize(pagePar);
		return pagePar;

	}

	@AuraEnabled    
	public static Page_Parent__c getLegalData()
	{
		Page_Parent__c pagePar = [SELECT Id, Page_Name__c, (Select Id, Body_Arabic__c, Body_Hebrew__c, Header_Arabic__c, Header_Hebrew__c, Order__c From Page_Childs__r Order By Order__c Asc) 
										From Page_Parent__c WHERE Name = 'Legal Terms' Limit 1];
		System.debug('~~~pagePar: ' + pagePar);
		//return JSON.serialize(pagePar);
		return pagePar;

	}

	@AuraEnabled    
	public static Page_Parent__c getTermsData()
	{
		Page_Parent__c pagePar = [SELECT Id, Page_Name__c, (Select Id, Body_Arabic__c, Body_Hebrew__c, Header_Arabic__c, Header_Hebrew__c, Order__c From Page_Childs__r Order By Order__c Asc) 
										From Page_Parent__c WHERE Name = 'Terms Of Use' Limit 1];
		System.debug('~~~pagePar: ' + pagePar);
		//return JSON.serialize(pagePar);
		return pagePar;

	}

	@AuraEnabled    
	public static Page_Parent__c getRegTermsData()
	{
		Page_Parent__c pagePar = [SELECT Id, Page_Name__c, (Select Id, Body_Arabic__c, Body_Hebrew__c, Header_Arabic__c, Header_Hebrew__c, Order__c From Page_Childs__r Order By Order__c Asc) 
										From Page_Parent__c WHERE Name = 'Registration Terms Of Use' Limit 1];
		System.debug('~~~pagePar: ' + pagePar);
		//return JSON.serialize(pagePar);
		return pagePar;

	}

	@AuraEnabled    
	public static Page_Parent__c getPrivacyPolicy()
	{
		Page_Parent__c pagePar = [SELECT Id, Page_Name__c, (Select Id, Body_Arabic__c, Body_Hebrew__c, Header_Arabic__c, Header_Hebrew__c, Order__c From Page_Childs__r Order By Order__c Asc) 
										From Page_Parent__c WHERE Name = 'Privacy Policy' Limit 1];
		System.debug('~~~pagePar: ' + pagePar);
		//return JSON.serialize(pagePar);
		return pagePar;

	}
}