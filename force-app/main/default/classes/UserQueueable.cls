public with sharing class UserQueueable implements Queueable {

	List<id> userIds;
	List <Contact> contactsList;
	public UserQueueable(List<id> theUsers, List <Contact> theContacts) {
		userIds = theUsers;
		contactsList= theContacts;	
	}
	
	public void execute(QueueableContext context) {
		System.debug('userqueusearch: execute');
		//update Lookup To user in contact:
		if (contactsList != null || !contactsList.isEmpty()){
			try{
				System.enqueueJob(new contactToUpdateQueueable(contactsList));
			}
			catch(exception ex){
				system.debug('*** Error in updating Contact-2: ' + ex);
			}				
		}

		Set<String> assigneeIdSet = new Set<String>();
		id permission_SetId;
		for (PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'F_Secure_Cloud_Protection_User'  Limit 1]) {
			permission_SetId = ps.Id;
			for (PermissionSetAssignment psa : [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :ps.Id]) { //query for the permission set Users that are already assigned
				assigneeIdSet.add(psa.AssigneeId); //add the Id of each assigned User to our set
			}
		}
		if (permission_SetId == null) {
			system.debug('*** No Permission Set Id found by Name');
			return;
		}

		List<PermissionSetAssignment> permissionSetAssignmentList = new List<PermissionSetAssignment>();	
		if (userIds != null || !userIds.isEmpty()){
			for ( Id usr : userIds ){
				if (permission_SetId != null && !assigneeIdSet.contains(usr) ){
					permissionSetAssignmentList.add( new PermissionSetAssignment (PermissionSetId = permission_SetId , AssigneeId = usr) );						
				}
				else{
					system.debug('*** permission Set Assignee exist ');
				}	
			}
			try{
				insert permissionSetAssignmentList;
			}
			catch(exception ex){
				system.debug('*** Error in insert to Permission Set Assignment: ' + ex);
			}				
		}			
	}
}