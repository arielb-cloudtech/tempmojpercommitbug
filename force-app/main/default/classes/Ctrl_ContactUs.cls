public without sharing class Ctrl_ContactUs {
	public Ctrl_ContactUs() {		
	}

	/** 
		Return current user for default form field values
	**/
	@AuraEnabled
	public static ReturnObjects.MojUser getCurrentUser(){
		return Ctrl_Community_Register.getCurrentUser();
	}

	@AuraEnabled
	public static void deleteDocument(String documentId){
		ContentDocument cd = [SELECT Id FROM ContentDocument WHERE Id = :documentId LIMIT 1];
		if(cd != null){
			delete cd;
		}
	}

	@AuraEnabled
	public static void cancelCase(String caseId, List<String> documents){
		System.debug('TTTTTTTTTTTTTTT - cancelCase');
		System.debug(documents);
		System.debug(caseId);
		Case caseObj = [SELECT Id FROM CASE WHERE Id = :caseId LIMIT 1];
		delete caseObj;		 		
		if (documents != null && documents.size() > 0){			
			ContentDocument cd = [SELECT Id FROM ContentDocument WHERE Id IN :documents];
			if(cd != null){
				delete cd;
			}
		}
	}


	/**
	 Submit a contact us form
	**/
	@AuraEnabled
	public static ReturnObjects.AjaxMessage SaveFeedback(
						String caseId, String FirstName, String LastName, string Email, String Phone , 
						String Company, String Role, String Subject, String Message ) {

		ReturnObjects.AjaxMessage response = new ReturnObjects.AjaxMessage();
		Case feedBack = createFeedBackCase( caseId, FirstName, LastName, Email, Phone, Company, Role, Subject, Message );
		response.message =feedBack.Id;
		response.setSuccessStatus();

		return response;
	}

	public static Case createFeedBackCase(
						String caseId, String FirstName, String LastName, string Email, String Phone , 
						String Company, String Role, String Subject, String Message ) {					
		
		string title = FirstName + ' ' + LastName;

		if (Role != '') {
			title = Role + '. ' + title;
		}

		Case caseObj = new Case(
			Id = caseId,
			Status = 'New',
			Origin = 'Web',
			Subject = Subject,
			OriginalText__c = Message,
			SuppliedName = title,
			SuppliedCompany = Company,
			SuppliedEmail = Email,
			SuppliedPhone = Phone
		);

		upsert caseObj;
		return caseObj;
	}

}