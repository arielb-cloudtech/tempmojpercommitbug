public class ReturnObjects {

    /**
        Generic method for ajax request for short message
    */
    public virtual class AjaxMessage {
        // default to error 
        @AuraEnabled public String status = 'error';
        @AuraEnabled public String message = '';

        public void setSuccessStatus( ){
            this.status = 'success';
        }

        public AjaxMessage() {
        }

        public AjaxMessage(String message){
            this.message = message;
        }
    }
    
    /**
        Data for Author Icon 
     */
    public class Author {
        @AuraEnabled public String Initials = '';
        @AuraEnabled public String Name;
        // User icon background image (RGB - Red, Green, Blue)
        @AuraEnabled public List<Integer> BgColor = new  List<Integer>{256,256,256};
        
        public Author(User author){
            if( author!= null ) {
                this(author.firstName, author.lastName);
                this.Name = author.Name;
            }
        }

         public Author(Contact author){
            if( author!= null ) {
                this(author.firstName, author.lastName);
                this.Name = author.Name;
            }
        }
        
        public Author(String firstName, String lastName){    
			if(!String.isBlank(firstName)){
				this.Initials +=firstName.substring(0, 1); 
			}
			if(!String.isBlank(lastName)){
				this.Initials += lastName.substring(0, 1); 
			}
           
            String userUniqueNumber =  String.valueOf(System.hashCode(firstName+' '+lastName));
            this.BgColor[0] = 100 + math.mod( Integer.valueOf(userUniqueNumber.substring(0,3)) , 156 );
            this.BgColor[1] = 100 + math.mod( Integer.valueOf(userUniqueNumber.substring(1,4)) , 156 );
            this.BgColor[2] = 100 + math.mod( Integer.valueOf(userUniqueNumber.substring(2,5)) , 156 );
        }
    }

    /**
     Law Item Cart- contains information about law
     TODO: Rename to lawItem (or any other informative name)
     */
    public virtual class ItemCard {
        private Law_Item__c Itemcard;

        @AuraEnabled public String Id;
		@AuraEnabled public String LastCommentDate;
		@AuraEnabled public boolean IsOpenForComments;
		@AuraEnabled public String Title;
		@AuraEnabled public String PublicationDate;
		@AuraEnabled public String PublisherName;
		@AuraEnabled public String PublisherId;
		@AuraEnabled public String DocumentBrief;
        
		
		@AuraEnabled public Integer CommentsCount;
		@AuraEnabled public List<String> Classifications = new  List<String>();

        @AuraEnabled public LawItemComment Draft;
        @AuraEnabled public Ctrl_Community_Tzkirim.WrapperLawAmendment LawAmendment;

        @AuraEnabled public List<Ctrl_Community_Tzkirim.WrapperDocument> Documents = new List<Ctrl_Community_Tzkirim.WrapperDocument>();
		@AuraEnabled public List<Ctrl_Community_Tzkirim.WrapperLaw> Laws = new List<Ctrl_Community_Tzkirim.WrapperLaw>();
		@AuraEnabled public List<LawItemComment> Comments = new List<LawItemComment>();
        
        @AuraEnabled public String File;
        @AuraEnabled public String FileType;
        @AuraEnabled public Boolean HasFile;
        @AuraEnabled public String OwnerType;
        @AuraEnabled public String DocumentReadableType;
        @AuraEnabled public String FixedCreatedDate;

        public ItemCard(  Law_Item__c lawObject) {
            this(lawObject, new  Map<Id, Integer>());
        }

        public ItemCard(  Law_Item__c lawObject, Map<Id, Integer> commentsByLawItem ) {
            
            this.Id = String.valueOf(lawObject.Id);
            this.Itemcard = lawObject;
			this.Title = this.Itemcard.Law_Item_Name__c;
            
            if ( commentsByLawItem.keySet().contains(lawObject.Id) ) {
                this.CommentsCount = commentsByLawItem.get(lawObject.Id);
            }

            this.HasFile = false; 
            
            Schema.RecordTypeInfo rt = Schema.SObjectType.Law_Item__c.getRecordTypeInfosById().get(lawObject.RecordTypeId);
            this.OwnerType = String.valueOf(rt.DeveloperName);
            this.DocumentReadableType = rt.name;
            this.PublisherName = this.Itemcard.Publisher_Account__r.Name;
            this.PublisherId = this.Itemcard.Publisher_Account__c;
            this.DocumentBrief = this.Itemcard.Document_Brief__c;
            this.FixedCreatedDate = this.Itemcard.CreatedDate.format('dd/MM/yyyy');
            
            if ( this.Itemcard.Last_Date_for_Comments__c != null && this.Itemcard.Last_Date_for_Comments__c >= Date.today() ){
				this.IsOpenForComments = true;
			} else {
				this.IsOpenForComments = false;
			}
            if ( null != this.Itemcard.Last_Date_for_Comments__c ) {
                this.LastCommentDate = String.valueOf(this.Itemcard.Last_Date_for_Comments__c.date().format());
            }

            if ( this.Itemcard.Publish_Date__c != null ) {
				this.PublicationDate = String.valueOf(this.Itemcard.Publish_Date__c.date().format());
			} else {
				this.PublicationDate = null;
			}
            
            for ( Law_Item_Classification__c classification : lawObject.Law_Item_Classifications__r ) {
                this.Classifications.add( classification.Classification_Name__c);
            }
            
        }


        public ItemCard(  Law_Item__c lawObject, 
                          Ctrl_Community_Tzkirim.WrapperLawAmendment lawAmendment,
                          LawItemComment draft,
                          List<Ctrl_Community_Tzkirim.WrapperDocument> documents,
                          List<Ctrl_Community_Tzkirim.WrapperLaw> laws) {
                
            this(lawObject);

			if(documents != null) {
				this.Documents = documents;
			} 

			if ( laws != null ) {
				this.Laws = laws;
			} 

			if ( lawAmendment != null ) {
				this.LawAmendment = lawAmendment;
			} 

			this.Draft = draft;

		}
    }

    /** 
        Sub Entety of comment
        comment for section of a Comment
    **/
    public class commentSection {
        @AuraEnabled public String Id;
        @AuraEnabled public String Comment;
        @AuraEnabled public String Section;

        public commentSection(Comment__c commentsSectionDB){
            this(commentsSectionDB.Bullet_Number__c , commentsSectionDB.Comment_Text__c);
            this.Id = commentsSectionDB.Id;
        }
        public commentSection(String Section, String Comment){
            this.Section = Section;
            this.Comment = Comment;
        }
    }

    /**
     Law Item Comment
     Users comments on law item
    ***/
    public virtual class LawItemComment{
		@AuraEnabled public String Id;
		@AuraEnabled public Author Author;
		@AuraEnabled public String Header;
		@AuraEnabled public String CommentDate;
		@AuraEnabled public String Body;
		@AuraEnabled public String Status;
		@AuraEnabled public Boolean IsUnread;
		@AuraEnabled public Boolean IsPrivate;
        @AuraEnabled public List<commentSection> commentSections = new List<commentSection>();
		@AuraEnabled public List<LawItemComment> replies = new List<LawItemComment>();
		@AuraEnabled public List<Ctrl_Community_Tzkirim.WrapperDocument> Files;

        /** 
            Set title of the comment: 
            name, organization, role
        **/
        private void setHeader( Comment__c DBComment){
            this.Header = '';
        
			if( !String.isBlank(DBComment.Comment_By__r.Name) ) {
                
				this.Header = DBComment.Comment_By__r.Name;
				if(!String.isBlank(DBComment.Comment_By__r.User__r.Role__c)){
					this.Header += ', ' + DBComment.Comment_By__r.User__r.Role__c;
				}
				if(!String.isBlank( DBComment.Comment_By__r.Company__c)){
					this.Header += ', ' +  DBComment.Comment_By__r.Company__c;
				}	
			}
            if(DBComment.Comment_Status__c == 'draft') {
                this.Header = Label.draft;
            }
        }

        // Constructor for comment reply.
        // should be called only from constructor with files
        public LawItemComment ( Comment_Reply__c DBComment,  Map<Id, List<Ctrl_Community_Tzkirim.WrapperDocument>> files ) {
            this.Id = DBComment.Id;
			this.CommentDate = DBComment.CreatedDate.format('dd/MM/yyyy');
			this.Body = DBComment.Reply__c;
			this.IsUnread = !DBComment.Read__c;
            this.Header = Label.Ministry_response;
            this.Files = files.get( DBComment.Id );
        }

        // Constructor for normal comment
        public LawItemComment ( Comment__c DBComment,  Map<Id, List<Ctrl_Community_Tzkirim.WrapperDocument>> files, Boolean showPrivateReplys  ) {
            
			this.Id = DBComment.Id;
            this.Author = new Author( DBComment.Comment_By__r );
			this.CommentDate = DBComment.CreatedDate.format('dd/MM/yyyy');
			this.Body = DBComment.Comment_Text__c;
            this.IsPrivate = DBComment.Is_Private_Comment__c;
            
            for ( Comment_Reply__c reply : DBComment.Ministry_Responses__r ) {
                // hide private replys
                if ( showPrivateReplys || reply.Display_Reply__c ) {
                    this.replies.add(
                        new LawItemComment( reply, files )
                    );
                }
            }

            for ( Comment__c subComment : DBComment.Comments1__r ) {
				this.commentSections.add(
                    new commentSection( subComment )
                );
            }

            this.setHeader( DBComment );
            this.Files = files.get( DBComment.Id );
		}
	}

    /** 

    Normal Portal client - can add comment, save drafts and edit his own details.

    **/
    public class MojUser {
        @AuraEnabled public String Id;
		@AuraEnabled public Author Author;
		@AuraEnabled public String Username;
		@AuraEnabled public String Phone;
		@AuraEnabled public String Email;
		@AuraEnabled public String FirstName;
		@AuraEnabled public String LastName;
		@AuraEnabled public String Company;
		@AuraEnabled public String Role;

        public MojUser(User u){
            this.Id = u.Id;
            this.Username = u.Username;
            this.Phone = u.MobilePhone;
            this.Email = u.Email;
            this.FirstName = u.FirstName;
            this.LastName = u.LastName;
            this.Company = u.Company__c;
            this.Role = u.Role__c;
        }
    }

    public class MojException extends Exception {}
}