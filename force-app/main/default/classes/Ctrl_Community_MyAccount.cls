public without sharing class Ctrl_Community_MyAccount {

    /** 
	 Response with counters 
	**/
	public virtual class MyAccountCountersResponse extends ReturnObjects.AjaxMessage {

		@AuraEnabled public Map<String, Integer> Counters = new Map<String, Integer>();
		@AuraEnabled public List<ItemCardUserComment> Comments = new List<ItemCardUserComment>();

		public MyAccountCountersResponse( String filter ) {
			super();

			Id contactId = getCurrentContactId();
			if( filter=='drafts' ) {
				this.Counters.put('total',  database.countQuery('select count() FROM Comment__c Where Comment_By__c = \''+contactId+'\' AND Sub_Comment__c =  null AND Comment_Status__c = \'Draft\' '));
			}
			else {
				this.Counters.put('total',  database.countQuery('select count() FROM Comment__c Where Comment_By__c = \''+contactId+'\' AND Sub_Comment__c =  null AND Comment_Status__c NOT IN (\'Deleted\', \'Draft\')'));
				this.Counters.put('replies',  database.countQuery('SELECT count() FROM  Comment_Reply__c WHERE Reply_to_Comment__r.Comment_By__c = \''+contactId+'\''));
				this.Counters.put('unread',  database.countQuery('SELECT count() FROM  Comment_Reply__c WHERE Reply_to_Comment__r.Comment_By__c = \''+contactId+'\' AND Read__c != TRUE'));
			}
		}
	}
	
	/** 
		Response with comments lists
	**/
    public class UserCommentsResponse  extends ReturnObjects.AjaxMessage {
		@AuraEnabled public List<ItemCardUserComment> Comments = new List<ItemCardUserComment>();
	}	

	/** 
	LawItemComment with itemCard to be used in myaccount pages
	**/
	public class ItemCardUserComment  extends ReturnObjects.LawItemComment {
		@AuraEnabled public ReturnObjects.ItemCard itemCard;

		public ItemCardUserComment( Comment__c DBComment , Map<Id, List<Ctrl_Community_Tzkirim.WrapperDocument>> files) {
			super(DBComment, files, true);
			this.itemCard = new ReturnObjects.ItemCard(DBComment.Related_to_Law_Item__r);	
			this.Header = Label.My_comment;					
		}
	}


    public class WrapperComment {
		@AuraEnabled public String Id;
		@AuraEnabled public String Body;
		@AuraEnabled public List<WrapperCommentReply> CommentReplys;
		@AuraEnabled public String Title;
		@AuraEnabled public DateTime LawItemPublishDate;
		@AuraEnabled public String LawItemResponsibility;		
		@AuraEnabled public List<WrapperFile> Files;
		@AuraEnabled public DateTime CreatedDate;
		@AuraEnabled public Boolean IsPrivate;
		@AuraEnabled public String LawItemId;
		@AuraEnabled public Boolean Sent;
		@AuraEnabled public Boolean SentWithReply;
		@AuraEnabled public Boolean SentNotReaded;
		@AuraEnabled public Boolean IsDisp;
		@AuraEnabled public String OwnerType;
		@AuraEnabled public String FixedCreatedDate;
		
		public WrapperComment( Comment__c DBComment) {
			this.Id = DBComment.Id;
			this.Body = DBComment.Comment_Text__c;
			this.LawItemPublishDate = DBComment.Related_to_Law_Item__r.Publish_Date__c;
			this.LawItemResponsibility = DBComment.Related_to_Law_Item__r.Account_Responsibility__r.Name;
			this.CreatedDate = DBComment.CreatedDate;
			this.LawItemId = DBComment.Related_to_Law_Item__c;		
			this.Title = DBComment.Related_to_Law_Item__r.Law_Item_Name__c;		

			Schema.RecordTypeInfo rt = Schema.SObjectType.Law_Item__c.getRecordTypeInfosById().get(DBComment.Related_to_Law_Item__r.RecordTypeId);
            this.OwnerType = String.valueOf(rt.DeveloperName);	
			this.FixedCreatedDate = DBComment.Related_to_Law_Item__r.CreatedDate.format('dd/MM/yyyy');						
		}

		public WrapperComment(String recordId, String body, List<WrapperCommentReply> commentReplys, List<WrapperFile> versionIds, String lawItemName, DateTime publishDate, String responsibility, 
													DateTime dateCreate, String status, String lawItemId, Boolean bSent, Boolean bSentWithReply, Boolean bSentNotReaded){
			this.Id = recordId;
			this.Body = body;
			this.CommentReplys = commentReplys;
			this.Files = versionIds;
			this.Title = lawItemName;
			this.LawItemPublishDate = publishDate;
			this.LawItemResponsibility = responsibility;
			this.CreatedDate = dateCreate;
			this.LawItemId = lawItemId;
			this.Sent = bSent;
			this.SentWithReply = bSentWithReply;
			this.SentNotReaded = bSentNotReaded;
			this.IsPrivate = false;

			if (status == 'Published Private') {
				this.IsPrivate = true;
			}
		}

		public WrapperComment(Boolean isDisp, String recordId, String body, List<WrapperCommentReply> commentReplys, List<WrapperFile> versionIds, String lawItemName, DateTime publishDate, String responsibility, 
													DateTime dateCreate, String status, String lawItemId, Boolean bSent, Boolean bSentWithReply, Boolean bSentNotReaded){
			this.IsDisp = !isDisp;
			this.Id = recordId;
			this.Body = body;
			this.CommentReplys = commentReplys;
			this.Files = versionIds;
			this.Title = lawItemName;
			this.LawItemPublishDate = publishDate;
			this.LawItemResponsibility = responsibility;
			this.CreatedDate = dateCreate;
			this.LawItemId = lawItemId;
			this.Sent = bSent;
			this.SentWithReply = bSentWithReply;
			this.SentNotReaded = bSentNotReaded;
			this.IsPrivate = false;
			if (status == 'Published Private')
				this.IsPrivate = true;
		}
	}

	public class WrapperCommentReply{
		@AuraEnabled public String Id;
		@AuraEnabled public String Body;
		@AuraEnabled public List<WrapperFile> Files;
		@AuraEnabled public Boolean HasFiles = false;
		@AuraEnabled public DateTime CreatedDate;
		@AuraEnabled public Boolean IsNew = false;

		public WrapperCommentReply(String recordId, String body, List<WrapperFile> versionIds, DateTime createdDate, Boolean NotReaded){
			this.Id = recordId;
			this.Body = body;
			this.Files = versionIds;
			this.CreatedDate = createdDate;
			this.IsNew = false;
			if (!NotReaded)
				this.IsNew = true;
		}
	}

	public class WrapperFile{
		@AuraEnabled public String Id;
	
		@AuraEnabled public String DocumentId;
		@AuraEnabled public String VersionId;
		@AuraEnabled public String FileType;
		@AuraEnabled public String Title;
		@AuraEnabled public String Extension;
	
		public WrapperFile(Id documentIs, Id versionId, String fileType, String title, String extension){
			// mId = recordId;
			// documentId = mId;
			this.Id = String.valueOf(documentIs);
			this.DocumentId = String.valueOf(documentIs);
			this.VersionId = String.valueOf(versionId);
			this.FileType = fileType;
			this.Title = title;
			if ( extension != 'pdf' ) {
				this.Extension = 'word';
			} else{
				this.Extension = 'pdf';
			}
		}
	}

	/** 
	Set comment visibility on published comment
	allow user to show/hide his comments
	**/
	@AuraEnabled
	public static UserCommentsResponse setCommentVisibility(String commentId, Boolean isPrivate){
		 
		 UserCommentsResponse response = new UserCommentsResponse();

		 Id userId = UserInfo.getUserId();
		 User currentUser = [SELECT ContactId FROM USER WHERE Id = :userId LIMIT 1];
		 
		if ( currentUser==null || currentUser.ContactId == null ) {
			response.message = 'Unauthorized User';
			return response;
		}
		Id contactId = currentUser.ContactId;
		List<String> queryWhere = new List<String> {
			'Id = :commentId',
			'Comment_By__c = :contactId'
		};

		String query = Ctrl_Community_Tzkirim.getCommentQuery(queryWhere);

		List<Comment__c> userComments = Database.query(query);
		
		Set<Id> fileParents = new Set<Id>();
		for ( Comment__c  userComment : userComments) {
			userComment.Is_Private_Comment__c  = isPrivate==null?False:isPrivate;
			fileParents.add(userComment.Id);
			for ( Comment_Reply__c commentReplay : userComment.Ministry_Responses__r ) {
				fileParents.add(commentReplay.Id);
			}
		}

		update userComments;

		Map<Id, List<Ctrl_Community_Tzkirim.WrapperDocument>> commentAttachments = Ctrl_Community_Tzkirim.getDocumentsForIds(fileParents);
		
		List<ItemCardUserComment> responseComment = new List<ItemCardUserComment>();
		for(Comment__c comment : userComments) {
			responseComment.add(new ItemCardUserComment(comment, commentAttachments));
		}
		response.Comments = responseComment;
		response.message = '';

		response.setSuccessStatus();

		return response;
	}

	public static Id getCurrentContactId(){

		 Id userId = UserInfo.getUserId();
		 User currentUser = [SELECT ContactId FROM USER WHERE Id = :userId LIMIT 1];
		 
		if ( currentUser==null || currentUser.ContactId == null ) {
			return null;
		}

		return currentUser.ContactId;
	}
	/** 
		Mark Ministry comment as read by the user
		
		params : 
			commentsToMark - set of ids of comment (not replies) to mark all their repsonses as read
			userId - set contains the contact Id of comment creator (required for security)
	**/
	@AuraEnabled
	public static MyAccountCountersResponse markCommentAsRead( Id commentsToMark , String filter ) {
		Id contactId = getCurrentContactId();

		List<Comment_Reply__c> readComments = [ SELECT Id, Read__c FROM Comment_Reply__c WHERE Id = :commentsToMark AND Reply_to_Comment__r.Comment_By__c = :contactId AND Read__c != TRUE ];

		for ( Comment_Reply__c readComment : readComments ) {
			readComment.Read__c = true;
		}
		update readComments;
		
		MyAccountCountersResponse response = new MyAccountCountersResponse( filter );
		response.setSuccessStatus();
		return response;
	}

	 @AuraEnabled
	 public static MyAccountCountersResponse getUserComments(String filter, List<Id> excludeIds){

		MyAccountCountersResponse response = new MyAccountCountersResponse(filter);

		Id contactId = getCurrentContactId();

		List<String> queryWhere = new List<String> {
			'Id NOT IN :excludeIds',
			'Comment_By__c = :contactId'
		};
		
		if(filter=='drafts') {
			queryWhere.add('Comment_Status__c =\'Draft\'');
		}
		else {
			queryWhere.add('Comment_Status__c NOT IN (\'Deleted\', \'Draft\')');
		}

		if(filter=='replies') {
			queryWhere.add('Id in (SELECT Reply_to_Comment__c FROM Comment_Reply__c)');
		}
		if(filter=='unread') {
			queryWhere.add('Id in (SELECT Reply_to_Comment__c FROM Comment_Reply__c WHERE Read__c IN (FALSE, NULL))');
		}

		String query = Ctrl_Community_Tzkirim.getCommentQuery(queryWhere);

		List<Comment__c> userComments = Database.query(query);
		
		List<ItemCardUserComment> responseComment = new List<ItemCardUserComment>();

		Set<Id> fileParents = new Set<Id>();
		
		for ( Comment__c  userComment : userComments) {
			fileParents.add(userComment.Id);
			for ( Comment_Reply__c commentReplay : userComment.Ministry_Responses__r ) {				
				fileParents.add(commentReplay.Id);				
			}
		}
		
		Map<Id, List<Ctrl_Community_Tzkirim.WrapperDocument>> commentAttachments = Ctrl_Community_Tzkirim.getDocumentsForIds(fileParents);
		
		for(Comment__c comment : userComments) {
			responseComment.add(new ItemCardUserComment(comment, commentAttachments));
		}
		response.Comments = responseComment;
		response.message = '';

		response.setSuccessStatus();

		return response;
	}

}