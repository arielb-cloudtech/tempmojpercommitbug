public with sharing class CommentTriggerHandler {
    public CommentTriggerHandler() {
        
    }
    public void ProfanityHandler(List<Comment__c> newList) {
        List<Case> caseList = new List<Case>();
        Map<Id,Id> lawItem2Owner = new Map<Id,Id>();
        Set<Id> relatedItems = new Set<Id>();
        for(Comment__c com : newList){
            if(com.Related_to_Law_Item__c!=null){
                relatedItems.add(com.Related_to_Law_Item__c);
            }
        }
        for(Law_Item__c l : [Select Id, OwnerId FROM Law_Item__c WHERE Id IN: relatedItems]){
            lawItem2Owner.put(l.Id, l.OwnerId);
        }

        for(Comment__c com : newList){
            Case cs = Util_ProfanityCheck.ProfanityCheck(com.Comment_Text__c, com.Id, true, null);
            system.debug('^^^ '+cs);
            system.debug('^^^ '+com);
            com.Comment_Text__c = cs.ReplacementText__c;
            system.debug('^^^ '+com);
            system.debug(com.Comment_Text__c);
            cs.OwnerId = com.Related_to_Law_Item__c!=null && lawItem2Owner.containsKey(com.Related_to_Law_Item__c)? lawItem2Owner.get(com.Related_to_Law_Item__c) : UserInfo.getUserId();
            caseList.add(cs);
        }
        system.debug('^^^^ caseList '+caseList);
        upsert caseList;
    }   
}