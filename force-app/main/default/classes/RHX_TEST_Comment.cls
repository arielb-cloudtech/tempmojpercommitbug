@isTest(SeeAllData=true)
public class RHX_TEST_Comment {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Comment__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Comment__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}