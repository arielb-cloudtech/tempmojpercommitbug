global with sharing class Sch_getKnessetData implements Schedulable {
    
    global Sch_getKnessetData() {
    }

    global void execute(SchedulableContext context) {
        System.enqueueJob(new Queue_IntegrateWithKnesset());
    }
}
