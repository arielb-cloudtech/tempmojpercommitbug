public without sharing class Ctrl_Wizard{

	public Ctrl_Wizard(){
	}

	@AuraEnabled
	public static InitObjectData initObject( String recId ){

		InitObjectData objData = new InitObjectData();
		objData.rtLst = [SELECT Id, Name FROM Recordtype WHERE SobjectType = 'Law_Item__c'];

		if( recId == null ) {
			objData.itm = new Law_Item__c(Name = 'placeholder');
			insert objData.itm;
		}
		else {
			Law_Item__c newLawItem = [SELECT Id, Law__r.Law_Name__c, Law_Item_Name__c, RecordTypeId, Law__c, Law__r.Name, Secondary_Law_Item__c,
										Short_Time_Approval__c, Publisher_Account__c, Legal_Counsel_Name__c, Legal_Counsel_Name__r.Name, Publisher_Account_Additional__c,
										Extra_Updates_Recipients__c, Legal_Counsel_Name_Additional__c, Main_File_URL__c, Document_Brief__c, Status__c,
										Last_Date_for_Comments__c, Publish_Date__c, Five_Year_Plan_URL__c, Reason_for_Gov_Decision__c, No_RIA_Reason__c,
										RIA_Attachment__c, No_RIA_Other_Reason__c, Time_Deduction_Reason__c, Other_Time_Deduction_Reason__c,
										RecordType.Name, Updates_Recipients__c, Number_of_Days_to_Respond__c, Publisher_Account__r.Name,
										(SELECT Id, Law__c, Law_Type__c, Law__r.Name, Law__r.RecordTypeId, Memorandum_of_Law__c, Memorandum_of_Law__r.Law_Item_Name__c
											FROM Related_to_Laws__r LIMIT 200)
										FROM Law_Item__c
										WHERE Id =: recId Limit 1];
			objData.itm = newLawItem;
		}

		if	( recId != null ) {
			Map<Id, ContentDocumentLink> cvs  = new Map<Id, ContentDocumentLink>();
			Set<Id> cdIds = new Set<Id>();
			List<fileLine> fLines = new List<fileLine>();

			for(ContentDocumentLink cdl : [SELECT Id, ContentDocumentId, ContentDocument.Title, ContentDocument.LatestPublishedVersion.File_Type__c,
											ContentDocument.Description
											FROM ContentDocumentLink
											WHERE LinkedEntityId = :recId
												AND ContentDocument.LatestPublishedVersion.File_Type__c != null]){
				cvs.put(cdl.ContentDocumentId, cdl);
				cdIds.add(cdl.ContentDocumentId);
			}
			List<ContentDocument> cds = [SELECT Id,Title, Description FROM ContentDocument WHERE Id IN :cdIds];
			for(ContentDocument cd : cds){
				fileLine fl = new fileLine();
				fl.Name = cd.Title;
				fl.Description = cd.Description;
				fl.Id = cd.Id;
				fl.FileType = cvs.get(cd.Id).ContentDocument.LatestPublishedVersion.File_Type__c;
				objData.itemFiles.add(fl);
			}
		}
		return objData;
	}

	@AuraEnabled
    public static String getAdvisor(String pubAccId){
		System.debug('~~~ method: getAdvisor');
        try{
			System.debug('~~~ method: getAdvisor');
            List<Contact> conLst = [SELECT Id, Name, FirstName, LastName FROM Contact WHERE AccountId =: pubAccId];
            conLst.sort();
            return JSON.serialize(conLst);
        }catch(Exception ex){
            System.debug('!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber());
            return '!!!Exception: ' + ex.getCause() + ' At Line: ' + ex.getLineNumber();
        }
    }

	@AuraEnabled
	public static String deleteObject(String objId){
		System.debug('~~~ method: deleteObject');
		Law_Item__c newLawItem = new Law_Item__c(Id = objId);
		delete newLawItem;
		return objId + ' Deleted';
	}

	@AuraEnabled
	public static String deleteFileServer(String objId){
		System.debug('~~~ method: deleteFileServer');
		ContentDocument cd = new ContentDocument(Id = objId);
		delete cd;
		return objId + ' Deleted';
	}

	@AuraEnabled
	public static String deleteFileServerBulk(List<String> fileIds){
		if(fileIds != null && !fileIds.isEmpty()){
			List<ContentDocument> documentsToDelete = new List<ContentDocument>();
			for(String fileId : fileIds){
				documentsToDelete.add(new ContentDocument(Id = fileId));
			}
			delete documentsToDelete;
		}
		return 'Files Deleted';
	}

	@AuraEnabled
	public static List<WrapperClassification> populateClasses(String lawItemId, List<String> lawsIds){
		List<WrapperClassification> ReturnObjects = new List<WrapperClassification>();

		List<Law_Item_Classification__c> lawItmClsLst = [	SELECT Id, Classification__c, Law_Item__c, Classification_Name__c
															FROM Law_Item_Classification__c
															WHERE Law_Item__c =: lawItemId	];


		for  (Law_Item_Classification__c lawItemClass : lawItmClsLst ){
			ReturnObjects.add( new WrapperClassification( lawItemClass.Classification__c,
															lawItemClass.Classification_Name__c ));
		}

		if ( ReturnObjects.size() == 0 ) {
			LawClassification__c a;
			for ( AggregateResult ar : [SELECT ClassificiationID__c, ClassificiationID__r.Name Name FROM LawClassification__c WHERE IsraelLawID__c IN: lawsIds GROUP BY ClassificiationID__c, ClassificiationID__r.Name] ){
				ReturnObjects.add( new WrapperClassification((String)ar.get('ClassificiationID__c'),
															 (String) ar.get('Name')));
			}
		}

		return ReturnObjects;
	}


	@AuraEnabled
	public static String getParentLaws(String searchVar, String inc){
		List<Law__c> parLaws = new List<Law__c>();
		List<lawLine> lls = new List<lawLine>();
		Boolean inc2 = inc != null && !String.isBlank(inc) ? Boolean.valueOf(inc) : false;

		Boolean flag = ( searchVar == '' || searchVar == null || String.isEmpty(searchVar) ) ? true : false;

		if(!inc2){ //unchecked law items included
			searchVar = '%' + searchVar + '%';
			RecordType parentLawRt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Parent_Law' LIMIT 1];
			for(Law__c l : [SELECT Id, Law_Name__c FROM Law__c WHERE Law_Name__c LIKE :searchVar AND RecordTypeId =: parentLawRt.Id AND LawValidityDesc__c = 'תקף' LIMIT 50]){
				lawLine ll = new lawLine();
				ll.Id = l.Id;
				ll.Name = l.Law_Name__c;

				if( flag ) {
					if( !ll.Name.contains('שגיאה') ){
						lls.add(ll);
					}
				} else {
					lls.add(ll);
				}
			}
		} else {//checked law items included
			searchVar = '%' + searchVar + '%';
			RecordType parentLawRt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Parent_Law' LIMIT 1];
			for(Law__c lw : [SELECT Id, Law_Name__c FROM Law__c WHERE Law_Name__c LIKE :searchVar AND RecordTypeId =: parentLawRt.Id AND LawValidityDesc__c = 'תקף' LIMIT 50]){
				lawLine ll = new lawLine();
				ll.Id = lw.Id;
				ll.Name = lw.Law_Name__c;
				ll.IsLi = false;
				//lls.add(ll);

				if( flag ) {
					if( !ll.Name.contains('שגיאה') ){
						lls.add(ll);
					}
				} else {
					lls.add(ll);
				}
			}
			for(Law_Item__c li : [SELECT Id, Law_Item_Name__c FROM Law_Item__c WHERE Law_Item_Name__c LIKE :searchVar LIMIT 50]){
				lawLine ll = new lawLine();
				ll.Id = li.Id;
				ll.Name = li.Law_Item_Name__c;
				ll.IsLi = true;
				lls.add(ll);
			}
		}
		return JSON.serialize(lls);
	}

	// *********************************************************************************************************************************************
	// *********************************************************************************************************************************************
	// *********************************************************************************************************************************************

	@AuraEnabled
	public static String getParentLaws(String searchVar, String inc, String lawId){

		if(String.isBlank(lawId)){
			return getParentLaws(searchVar, inc);
		}
		searchVar = '%' + searchVar + '%';
		Id parentLawRtId = Schema.SObjectType.Law__c.getRecordTypeInfosByDeveloperName().get('Parent_Law').getRecordTypeId();
		Law__c selectedLaw = [SELECT Id, (SELECT Id, ParentLawID__c, ParentLawID__r.Law_Name__c
											FROM LawBindings__r
											WHERE ParentLawID__r.Law_Name__c LIKE :searchVar
											  AND ParentLawID__r.RecordTypeId =: parentLawRtId AND ParentLawID__r.LawValidityDesc__c = 'תקף')
							FROM Law__c WHERE Id = :lawId LIMIT 1];
		List<lawLine> lls = new List<lawLine>();
		for(LawBinding__c lb : selectedLaw.LawBindings__r){
				lawLine ll = new lawLine();
				ll.Id = lb.ParentLawID__c;
				ll.Name = lb.ParentLawID__r.Law_Name__c;
				ll.IsLi = false;
				lls.add(ll);
		}
		Boolean inc2 = inc != null && !String.isBlank(inc) ? Boolean.valueOf(inc) : false;
		if(inc2){
			for(Law_Item__c li : [SELECT Id, Law_Item_Name__c FROM Law_Item__c WHERE Law_Item_Name__c LIKE :searchVar LIMIT 50]){
				lawLine ll = new lawLine();
				ll.Id = li.Id;
				ll.Name = li.Law_Item_Name__c;
				ll.IsLi = true;
				lls.add(ll);
			}
		}

		// lls = appendObjectsWithErrorName(lls);

		return JSON.serialize(lls);
	}

	// *********************************************************************************************************************************************
	// *********************************************************************************************************************************************
	// *********************************************************************************************************************************************

	@AuraEnabled
	public static String getSpecLaw(String searchVar){
		System.debug('~~~ method: getSpecLaw');
		List<Law__c> parLaws = new List<Law__c>();
		if(true){
			RecordType parentLawRt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Parent_Law' LIMIT 1];
			parLaws = [SELECT Id, Law_Name__c FROM Law__c WHERE Id =: searchVar];
		}
		return JSON.serialize(parLaws);
	}

	@AuraEnabled
	public static String getMotherLaws(String searchVar){
		System.debug('~~~ method: getMotherLaws');
		List<Law__c> parLaws = new List<Law__c>();
		if(true){
			searchVar = '%' + searchVar + '%';
			RecordType parentLawRt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Secondary_Law_Mother' LIMIT 1];
			parLaws = [SELECT Id, Law_Name__c FROM Law__c WHERE Law_Name__c LIKE :searchVar AND RecordTypeId =: parentLawRt.Id LIMIT 50];
		}
		return JSON.serialize(parLaws);
	}

	@AuraEnabled
	public static String getChildLaws(String parent){
		System.debug('~~~ method: getChildLaws');
		RecordType childLawRt = new RecordType();
		List<Law__c> childLaws = new List<Law__c>();
		List<LawBinding__c> lawsJunc = [SELECT Id, LawID__c FROM LawBinding__c WHERE IsraelLawID__c =: parent];
		Set<Id> childLawsIdSet = new Set<Id>();
		for(LawBinding__c lb : lawsJunc){
			childLawsIdSet.add(lb.LawID__c);
		}
		if(parent != null && !String.isBlank(parent)){
			childLawRt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Law' LIMIT 1];
			childLaws = [SELECT Id, Law_Name__c FROM Law__c WHERE RecordTypeId =: childLawRt.Id AND Id IN :childLawsIdSet];
		}
		String childLawsSerialize = JSON.serialize(childLaws);
		return childLawsSerialize;
	}

	@AuraEnabled
	public static String getLawItems(){
		System.debug('~~~ method: getLawItems');
		String rtId = Schema.SObjectType.Law_Item__c.getRecordTypeInfosByDeveloperName().get('Memorandum_of_Law').getRecordTypeId();
		List<Law_Item__c> lis = [SELECT Id, Law_Item_Name__c
									FROM Law_Item__c
									WHERE Law_Item_Name__c != 'placeholder'
										AND Law_Item_Name__c != ''
										AND RecordTypeId =: rtId];
		return JSON.serialize(lis);
	}

	@AuraEnabled
	public static String getObjects() {
		page2Objects objs = new page2Objects();
		objs.lawClasses = [ SELECT Id, Name FROM Classification__c ORDER BY Name ASC ];
		objs.contacts = [ SELECT Id, FirstName, LastName FROM Contact LIMIT 10 ];
		List<Contact> con = [SELECT Id, Account.Name, Account.Id FROM Contact WHERE User__c =: UserInfo.getUserId() LIMIT 1];
		if ( con != null && !con.isEmpty() ) {
			objs.autoPublisher = con[0].Account.Id + ',' + con[0].Account.Name;
		}

		Profile prof = [SELECT Id, Name FROM Profile WHERE Id =: UserInfo.getProfileId() LIMIT 1];
		if(prof.Name == 'Referent'){
			Id accountId = con[0].Account.Id;
			objs.accounts = [SELECT Id, Name FROM Account WHERE IsUnit__c = true AND IsActive__c = true AND Id = :accountId ];
		}
		else {
			objs.accounts = [SELECT Id, Name FROM Account WHERE IsUnit__c = true AND IsActive__c = true];
		}
		String objsSerialize = JSON.serialize(objs);
		return objsSerialize;
	}

	@AuraEnabled
	public static String getHebYear(){
		System.debug('~~~ method: getHebYear');
		Map<String,String> resultMap = new Map<String,String>();
		resultMap = Util_Email_Templates.getHebDate(Date.today());
		List<String> hebDate = resultMap.get('hebrew').split(' ');
		String hebYear = hebDate[hebDate.size()-1];
		return hebYear;
	}

	@AuraEnabled
	public static String updateFileDescs(String files){
		try{
			List<fileLine> extraFiles = (List<fileLine>)JSON.deserialize(files, List<fileLine>.class);
			List<ContentDocument> docsToUpdate = new List<ContentDocument>();
			for(fileLine fl : extraFiles){
				docsToUpdate.add(new ContentDocument(Id = fl.Id, Description = fl.Description));
			}
			if(docsToUpdate != null && !docsToUpdate.isEmpty()){
				update docsToUpdate;
			}
			return 'Success';
		}catch(Exception ex){
			return '!!!Exception: ' + ex.getMessage() +  ' At Line: ' + ex.getLineNumber();
		}
	}

	/** 
	Set law item to be related to an uploaded file.
	delete previous file if nessesery
	**/
	@AuraEnabled
	public static Boolean deleteFile(String fileId){
		// Delete Old file
		if(!String.isBlank(fileId)){
			List<ContentDocument> documents = [SELECT Id FROM ContentDocument WHERE Id = :fileId];
			if(!documents.isEmpty()){
				delete documents;
			}
			return true;
		}
		return false;
	}

	@AuraEnabled
	public static String saveLawItem( String lawItem, String dates, String lawLines, String childLawLines, String removedLawLines, String classLst, String removedClsLst, String mainFileId) {
		try{
		
			List<Law_Item_Classification__c> lawItmClsLstToRemove = new List<Law_Item_Classification__c>();
			List<String> clsToRemove = (List<String>)JSON.deserialize(removedClsLst, List<String>.class);
			lawLines = lawLines.remove('\\"');
			lawLines = '{"lawLines":' + lawLines + '}';
			removedLawLines = '{"lawLines":' + removedLawLines + '}';
			childLawLines = childLawLines.remove('\\"');
			childLawLines = '{"lawLines":' + childLawLines + '}';
			Law_Item__c lawItm = (Law_Item__c)JSON.deserialize(lawItem, Law_Item__c.class);
			lawItm.Name = lawItm.Id;
			

			lawItmClsLstToRemove = [SELECT Id FROM Law_Item_Classification__c WHERE Law_Item__c =: lawItm.Id AND Classification__c IN :clsToRemove];
			List<classification> clsLst = (List<classification>)JSON.deserialize(classLst, List<classification>.class);
			System.debug('~~~clsLst: ' + clsLst);
			List<Law_Item_Classification__c> existingClsJuncs = [SELECT Id, Classification__c, Law_Item__c FROM Law_Item_Classification__c WHERE Law_Item__c =: lawItm.Id];
			Map<Id, Law_Item_Classification__c> existingClsJuncsMap = new Map<Id, Law_Item_Classification__c>();
			for(Law_Item_Classification__c cls : existingClsJuncs){
				existingClsJuncsMap.put(cls.Classification__c, cls);
			}
			List<Law_Item_Classification__c> clsJuncToInsert = new List<Law_Item_Classification__c>();
			for(classification cls : clsLst){
				if(!existingClsJuncsMap.containsKey(cls.sfId)){
					clsJuncToInsert.add(new Law_Item_Classification__c(Classification__c = cls.sfId, Law_Item__c = lawItm.Id));
				}
			}
			List<Related_to_Law__c> existingJuncs = [SELECT Id, Law__c, Memorandum_of_Law__c FROM Related_to_Law__c WHERE Law_Item__c =: lawItm.Id];
			Map<Id, Related_to_Law__c> existingJuncsMap = new Map<Id, Related_to_Law__c>();

			if(existingJuncs != null && !existingJuncs.isEmpty()){
				for(Related_to_Law__c junc : existingJuncs){
					if(!String.isBlank(junc.Memorandum_of_Law__c)){
						existingJuncsMap.put(junc.Memorandum_of_Law__c, junc);
					}else{
						existingJuncsMap.put(junc.Law__c, junc);
					}
				}
			}

			LawLineContainer removedLawLinesLst = (LawLineContainer)JSON.deserialize(String.valueOf(removedLawLines), LawLineContainer.class);
			List<Related_to_Law__c> lineToRemove = new List<Related_to_Law__c>();
			LawLineContainer lawLineLst = new LawLineContainer();
			lawLineLst = (LawLineContainer)JSON.deserialize(String.valueOf(lawLines), LawLineContainer.class);
			LawLineContainer childLawLineLst = new LawLineContainer();
			childLawLineLst = (LawLineContainer)JSON.deserialize(String.valueOf(childLawLines), LawLineContainer.class);
			List<Related_to_Law__c> juncLstToInsert = new List<Related_to_Law__c>();
			List<Related_to_Law__c> childJuncLstToInsert = new List<Related_to_Law__c>();
			Map<String, Object> datesMap = (Map<String, Object>)JSON.deserializeUntyped(dates);
			TimeZone tz = UserInfo.getTimeZone();
			for(LawLine ll : lawLineLst.lawlines){
				if(ll.Id != null && !String.isBlank(ll.Id)){
					if(Id.valueOf(ll.Id).getSobjectType() == Schema.Law__c.SObjectType){
						if(!existingJuncsMap.containsKey(ll.Id)){
							juncLstToInsert.add(new Related_to_Law__c(Id = ll.JuncId, Law__c = ll.Id, Law_Item__c = lawItm.Id));
						}
					}else if(Id.valueOf(ll.Id).getSobjectType() == Schema.Law_Item__c.SObjectType){
						if(!existingJuncsMap.containsKey(ll.Id)){
							juncLstToInsert.add(new Related_to_Law__c(Id = ll.JuncId, Memorandum_of_Law__c = ll.Id, Law_Item__c = lawItm.Id));
						}
					}
				}
			}

			for(LawLine ll : childLawLineLst.lawlines){
				if(ll.Id != null && !String.isBlank(ll.Id)){
					childJuncLstToInsert.add(new Related_to_Law__c(Id = ll.JuncId, Law__c = ll.Id, Law_Item__c = lawItm.Id));
				}
			}
			if(removedLawLinesLst.lawlines != null && !removedLawLinesLst.lawlines.isEmpty()){
				for(LawLine line : removedLawLinesLst.lawlines){
					lineToRemove.add(new Related_to_Law__c(Id = line.JuncId));
				}
			}

			if(lawItm.Publish_Date__c != null){
				if(LawItm.Status__c == 'Distributed'){
					lawItm.Publish_Date__c = Datetime.now();
				}else{
					lawItm.Publish_Date__c = lawItm.Publish_Date__c.addSeconds(tz.getOffset(lawItm.Publish_Date__c)/-1000);
				}
			}

			if(LawItm.Status__c == 'Distributed'){
				lawItm.Notify_Users__c = true;
			}
			System.debug('>>>>>LawItm.Status__c: ' + LawItm.Status__c);
			System.debug('>>>>>lawItm.Notify_Users__c: ' + lawItm.Notify_Users__c);


			if(!String.isBlank(mainFileId)){
				List<ContentVersion> cvs  = [SELECT Id, Title, ContentDocumentId, File_Type__c FROM ContentVersion WHERE ContentDocumentId = :mainFileId];
				ContentVersion cv = cvs[0];
				cv.File_Type__c = 'Main File';
				update cv;
				lawItm.Main_File_URL__c = '/' + cv.ContentDocumentId;
				lawItm.Main_File_Name__c = cv.Title;
			}

			if(lawItm.Last_Date_for_Comments__c != null){
				lawItm.Last_Date_for_Comments__c = lawItm.Last_Date_for_Comments__c.addSeconds(tz.getOffset(lawItm.Last_Date_for_Comments__c)/-1000);
				lawItm.Last_Date_for_Comments__c = Datetime.newInstance(lawItm.Last_Date_for_Comments__c.year(), lawItm.Last_Date_for_Comments__c.month(), lawItm.Last_Date_for_Comments__c.day(), 23, 59, 59);
			}

			if(lawItmClsLstToRemove != null && !lawItmClsLstToRemove.isEmpty()){
				delete lawItmClsLstToRemove;
			}
			if(clsJuncToInsert != null && !clsJuncToInsert.isEmpty()){
				System.debug('~~~clsJuncToInsert: ' + clsJuncToInsert);
				upsert clsJuncToInsert;
			}
			if(juncLstToInsert != null && !juncLstToInsert.isEmpty()){
				upsert juncLstToInsert;
			}
			if(childJuncLstToInsert != null && !childJuncLstToInsert.isEmpty()){
				upsert childJuncLstToInsert;
			}
			if(lineToRemove != null && !lineToRemove.isEmpty()){
				delete lineToRemove;
			}



			System.debug('>>>>>SHORT TIME Approval: ' + lawItm.Short_Time_Approval__c);



			update lawItm;
			return JSON.serialize(lawItm);
		}catch(Exception ex){
			System.debug('!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber() + ' Stack Trace: ' + ex.getStackTraceString());
			return '!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber() + ' Stack Trace: ' + ex.getStackTraceString();
		}
	}

	public static Datetime dateTimeParser(String dtStr){
		System.debug('~~~ method: dateTimeParser');
		if(dtStr != null && !String.isBlank(dtStr)){
            Datetime dt = Datetime.newInstanceGMT(Integer.valueOf(dtStr.subString(0,4)), Integer.valueOf(dtStr.subString(5,7)), Integer.valueOf(dtStr.subString(8,10)), Integer.valueOf(dtStr.subString(11,13)), Integer.valueOf(dtStr.subString(14,16)), 0);
        	return dt;
        }else{
            return null;
        }
    }

	@AuraEnabled
	public static String getUsers(){
		System.debug('~~~ method: getUsers');
		List<User> userLst = [SELECT Id, FirstName, LastName, Email FROM User];
		return JSON.serialize(userLst);
	}

	@AuraEnabled
	public static String getLawsP41(){
		System.debug('~~~ method: getLawsP41');
		Recordtype lawRt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Secondary_Law_Mother' AND SobjectType = 'Law__c' LIMIT 1];
		List<Law__c> laws = [SELECT Id, Law_Name__c FROM Law__c WHERE RecordTypeId =: lawRt.Id];
		return JSON.serialize(laws);
	}

	/** 
	
	Callback for various file uploader. 
	Set thet law item with a path for the uploaded file.
	Adds type description for the uploaded file.

	Params:
	lawItemId - current lawId of the edit item
	filesId - list of file ids (TODO: Replace to single file (?))
	fileType - code of the section of the uploaded file
	oldFileId (optional) - file to delete in case that the current file should replace an old one
	**/
	@AuraEnabled
	public static String updateFileTypes(String lawItemId, List<String> filesId, String fileType, String oldFileId){
		
		List<ContentVersion> contentVersions  = [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :filesId LIMIT 1];
		
		String fileFullType = 'Other File';
		
		switch on fileType {
			when 'Main File' {
				update new Law_Item__c(Id = lawItemId, Main_File_Name__c = contentVersions[0].Title);
				fileFullType = 'Main File';
			}
			when 'RIAFile' {
				update new Law_Item__c(Id = lawItemId, RIA_File_Name__c = contentVersions[0].Title);
				fileFullType = 'RIA File';
			}
			when 'HomeshFile' {
				update new Law_Item__c(Id = lawItemId, Five_Year_Plan_File_Name__c = contentVersions[0].Title);
				fileFullType = 'Homesh File';
			}
			when 'Combined Version File' {
				update new Law_Item__c(Id = lawItemId, Combined_File_Name__c = contentVersions[0].Title);
				fileFullType = 'Combined Version File';
			}
			when 'NegligibleEffectFile' {
				update new Law_Item__c(Id = lawItemId, Negligible_Impact_Name__c = contentVersions[0].Title);
				fileFullType = 'Negligible Effect File';
			}
			when 'OtherFile' {
				update new Law_Item__c(Id = lawItemId, No_RIA_Other_Name__c = contentVersions[0].Title);
			}
		}
			
		for(ContentVersion cv : contentVersions){
			cv.File_Type__c = fileFullType;
		}

		update contentVersions;

		deleteFile(oldFileId);

		return 'success';
	}

	@AuraEnabled
	public static String updateFileTypes(List<String> cvId, String fType){
		try {
			List<ContentVersion> cvs  = [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :cvId LIMIT 1];
			for(ContentVersion cv : cvs){
				cv.File_Type__c = fType;
			}
			update cvs;
		} catch(Exception ex){
			System.debug('!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber());
			return ex.getMessage() + ' At Line: ' + ex.getLineNumber();
		}
		return 'success';
	}

	@AuraEnabled
	public static String deleteOldFile(String cvId){
		System.debug('~~~ method: deleteOldFile');
		try{
			System.debug('~~~ method: deleteOldFile');
			ContentDocument cv  = new ContentDocument(Id = cvId);
			delete cv;
		}catch(Exception ex){
			System.debug('!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber());
			return ex.getMessage() + ' At Line: ' + ex.getLineNumber();
		}
		return 'deleted ' + cvId;
	}

	public class LawLineContainer{
		@AuraEnabled public List<lawLine> lawLines;

		public LawLineContainer(){

		}
	}

	public class fileLine{
		@AuraEnabled public String Id;
		@AuraEnabled public String Description;
		@AuraEnabled public String Name;
		@AuraEnabled public String FileType;

		public fileLine(){

		}
	}

	public class lawLine{
		@AuraEnabled  public String Id;
		@AuraEnabled  public String Name;
		@AuraEnabled  public String JuncId;
		@AuraEnabled  public Boolean IsLi;

		public lawLine(){

		}
	}

	public class AppData{
		@AuraEnabled public List<Law__c> parentLaws;
		@AuraEnabled public List<Law__c> motherLaws;

		public AppData(){

		}
	}

	public class InitObjectData{
		@AuraEnabled public Law_Item__c itm;
		@AuraEnabled public List<Recordtype> rtLst;
		@AuraEnabled public List<fileLine> itemFiles  = new List<fileLine>();

		public InitObjectData(){
		}
	}

	public class InitObjectEditData{
		@AuraEnabled public Law_Item__c itm;
		@AuraEnabled public List<Recordtype> rtLst;

		public InitObjectEditData(){

		}
	}

	public class page2Objects{
		@AuraEnabled public List<Classification__c> lawClasses;
		@AuraEnabled public List<Account> accounts;
		@AuraEnabled public List<Contact> contacts;
		@AuraEnabled public String hebYear;
		@AuraEnabled public String autoPublisher;

		public page2Objects(){

		}
	}

	public class classification{
		@AuraEnabled public String sfId;
		@AuraEnabled public String label;

		public classification(){

		}
	}

	public class WrapperClassification{
		@AuraEnabled public String Id;
		@AuraEnabled public String Name;

		public WrapperClassification(String recId, String recName){
			this.Id = recId;
			this.Name = recName;
		}
	}
}