@isTest
public with sharing class Test_Ctrl_LawItem_CommentPage {

    public Test_Ctrl_LawItem_CommentPage() {

    }

    @TestSetup
    static void makeData(){
        //data creator obj
        ClsObjectCreator cls = new ClsObjectCreator();
        //accounts
        Account acc = cls.createAccount('name');
        //Contact 
        List<Contact> contacts = new List<Contact>();
        contacts.add(cls.returnContact('Jamchi', UserInfo.getUserId()));
        insert contacts;
        //law items
        List<Law_Item__c> liList = new List<Law_Item__c>();
        liList.add(cls.returnLawItem('Draft', 'wow1', acc.Id));
        liList.add(cls.returnLawItem('Draft', 'wow2', acc.Id));
        insert liList;
        //comments
        List<Comment__c> comments = new List<Comment__c>();
        comments.add(cls.returnComment('Published'));
        comments.add(cls.returnComment('Published'));
        comments[0].Bullet_Number__c = '1';
        comments[0].Comment_By__c = contacts[0].Id;
        comments[0].Related_to_Law_Item__c = liList[0].Id; 
        comments[1].Bullet_Number__c = '2';
        comments[1].Comment_By__c = contacts[0].Id;
        comments[1].Related_to_Law_Item__c = liList[0].Id; 
        insert comments;
        //comment replys
        List<Comment_Reply__c> replys = new List<Comment_Reply__c>();
        replys.add(cls.returnCommentReply(true, true, comments[0].Id));
        replys.add(cls.returnCommentReply(true, true, comments[1].Id));
        insert replys;
        //cv cd cdl
        List<ContentVersion> cvList = new List<ContentVersion>();
        cvList.add(cls.returnContentVersion('name', 'name.txt', 'vd'));
        cvList.add(cls.returnContentVersion('name2', 'name2.txt', 'vd'));
        cvList.add(cls.returnContentVersion('name3', 'name3.txt', 'vd'));
        cvList.add(cls.returnContentVersion('name4', 'name4.txt', 'vd'));
        cvList.add(cls.returnContentVersion('name5', 'name5.txt', 'vd'));
        insert cvList;
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[0].Id Limit 1].ContentDocumentId, comments[0].Id));
        cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[1].Id Limit 1].ContentDocumentId, comments[0].Id));
        cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[2].Id Limit 1].ContentDocumentId, comments[1].Id));
        cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[3].Id Limit 1].ContentDocumentId, comments[1].Id));
        cdlList.add(cls.returnCDL([Select ContentDocumentId From ContentVersion Where Id = :cvList[4].Id Limit 1].ContentDocumentId, replys[0].Id));      
        insert cdlList;
    }

    @isTest
    public static void test(){
        Id cid = [Select Id From Comment__c Where Bullet_Number__c = '1' Limit 1].Id;
        Id cid2 = [Select Id From Comment__c Where Bullet_Number__c = '2' Limit 1].Id;
        Id rid = [Select Id From Comment_Reply__c Where Reply_to_Comment__c = :cid].Id;

        system.debug('!!!!!!!!'+cid);
        system.debug('######'+ cid2);

        Ctrl_LawItem_CommentPage o = new Ctrl_LawItem_CommentPage();
        
        Ctrl_LawItem_CommentPage.getUsers('p');
        
        Ctrl_LawItem_CommentPage.getUserId();
       
        Ctrl_LawItem_CommentPage.getUserInfo();
        
        Ctrl_LawItem_CommentPage.deletePdfFile([Select ContentDocumentId From ContentVersion Limit 1].ContentDocumentId);
       
        //comment action
        Ctrl_LawItem_CommentPage.CreateComment('comment', [Select Id From Law_Item__c Where Law_Item_Name__c = 'wow1' Limit 1].Id, cid);
        //not comment action ->else->reply
        Ctrl_LawItem_CommentPage.CreateComment('piupiuf', [Select Id From Law_Item__c Where Law_Item_Name__c = 'wow1' Limit 1].Id, cid);
        List<String> lst = new List<String>();
        for(ContentDocument cd : [Select Id From ContentDocument Limit 1]){
            lst.add(cd.Id);
        }
        Ctrl_LawItem_CommentPage.CancelCommentUpdate(lst);

        //connect api test method
       /*List<String> lst2 = new List<String>();
        for(Comment__c c : [Select Id From Comment__c]){
            lst2.add(c.Id);
        }
        Ctrl_LawItem_CommentPage.AddChatter(lst2, UserInfo.getUserId());*/
        
        List<String> lst3 = new List<String>();
        for(ContentDocumentLink cdl : [Select ContentDocumentId From ContentDocumentLink Where LinkedEntityId = :cid Limit 1]){
            lst3.add(cdl.ContentDocumentId);
        }

        Ctrl_LawItem_CommentPage.DeleteComment(cid2);

        //map is null
        Ctrl_LawItem_CommentPage.getData([Select Id From Law_Item__c Where Law_Item_Name__c = 'wow2' Limit 1].Id);
        //map has data
        Ctrl_LawItem_CommentPage.getData([Select Id From Law_Item__c Where Law_Item_Name__c = 'wow1' Limit 1].Id); //not good coverage

        List<String> lst4 = new List<String>();
        for(ContentDocumentLink cdl : [Select ContentDocumentId From ContentDocumentLink Where LinkedEntityId = :rid]){
            lst3.add(cdl.ContentDocumentId);
        }
        Ctrl_LawItem_CommentPage.SaveNewReply('b', rid, lst4, true);
    }
}