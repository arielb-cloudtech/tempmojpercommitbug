public with sharing class LawItemTriggerHandler {
    public LawItemTriggerHandler() {
        
    }
    public void splitDocBriefInsert(List<Law_Item__c> newList)
    {
        for(Law_Item__c li : newList)
        {
            if((li.Document_Brief__c != null && !String.isBlank(li.Document_Brief__c)) && li.Document_Brief__c.length() > 512)
            {
                li.Document_Brief_Short_1__c = li.Document_Brief__c.subString(0,254);
                li.Document_Brief_Short_1__c.subString(0,li.Document_Brief_Short_1__c.lastIndexOf(' '));
                li.Document_Brief_Short_2__c = li.Document_Brief__c.subString(254,509);
                li.Document_Brief_Short_2__c.subString(0,li.Document_Brief_Short_2__c.lastIndexOf(' '));
            }
        }
    }

    public void splitDocBriefUpdate(Map<Id, Law_Item__c> newMap, Map<Id, Law_Item__c> oldMap)
    {
        //Map<Id, Law_Item__c> relevantMap = new Map<Id, Law_Item__c>();

        for(Law_Item__c li : newMap.values())
        {
            if((li.Document_Brief__c != null && !String.isBlank(li.Document_Brief__c)) &&li.Document_Brief__c.length() > 512)
            {
                li.Document_Brief_Short_1__c = li.Document_Brief__c.subString(0,254);
                li.Document_Brief_Short_1__c.subString(0,li.Document_Brief_Short_1__c.lastIndexOf(' '));
                li.Document_Brief_Short_2__c = li.Document_Brief__c.subString(254,509);
                li.Document_Brief_Short_2__c.subString(0,li.Document_Brief_Short_2__c.lastIndexOf(' '));
            }
        }
    }

    public void ImmediateMailing(List<Law_Item__c> newList, Map<Id, Law_Item__c> newMap, String Language){
        Map<Id, Law_Item__c> rels = new Map<Id, Law_Item__c>();
        for(Law_Item__c l : newList){
            if(l.Notify_Users__c){
                rels.put(l.Id, l);
            }
        }

        System.debug('>>>>>Law Items w/ checked notify users: ' + rels);

        if(!rels.isEmpty()){
            Map<String,Set<String>> ProspectsByLawItems = Utils.ReturnProspectsByLawItems(rels.KeySet(),Language );
            system.debug('Language '+ Language);
            system.debug('ProspectsByLawItems '+ ProspectsByLawItems);

            if(Test.isRunningTest()){
                System.debug('>>>>>Test Cover for Utils class');
                Utils.ReturnLawItemsByProspects(rels.KeySet(),Language );
                Decimal d= 1;
                Utils.convertKNSIdToString(d);
                Utils.convertStringToDate(String.valueOf(DateTime.now()));
            }

            for(String LawItem : ProspectsByLawItems.KeySet()){
                System.debug('ProspectsByLawItems '+ ProspectsByLawItems);
                System.debug('>>>>>Executing batches here!!');
                //Batch_SendEmailsViaPardot bsevp = new Batch_SendEmailsViaPardot('Immediate', '', new Set<Id>{LawItem} ,Language,ProspectsByLawItems.get(LawItem) );
                Batch_SendEmailsViaPardot bsevp = new Batch_SendEmailsViaPardot('Immediate', Language, ProspectsByLawItems.get(LawItem), new Set<Id>{LawItem});
                // bsevp.opperation ='Immediate';
                // bsevp.lang = Language;
                // bsevp.prospects = ProspectsByLawItems.get(LawItem);
                // bsevp.LawItemIds = new Set<Id>{LawItem};
                Database.executeBatch(bsevp, 8);
                //// 8 cuz number of callouts per execute with keeping in mind that the entire batch period under an hour and the api key might expire in the middle (1 to gain new API KEY + 1 to resend current request + 8)
            }
        }
/*      Map<String,Set<String>> LawItemsByProspects = Utils.ReturnLawItemsByProspects(newMap.KeySet());
        System.debug('ProspectsByLawItems '+ LawItemsByProspects);      */
    }

    public void UnCheckNotify(List<Law_Item__c> newList){
        List<Law_Item__c> rels = new List<Law_Item__c>();
        for(Law_Item__c l : newList){
            if(l.Notify_Users__c){
                rels.add(new Law_Item__c(Id=l.Id, Notify_Users__c = false));
            }
        }

        if(!rels.isEmpty()){
           update rels;
        }
    }

///////////////////// cant use callouts in trigger /////////////////////
/*    public void UpdateLastCommentDateOnInsert(List<Law_Item__c> newList){
       system.debug('^^^  UpdateLastCommentDateOnInsert  method ont law item trigger');

        UpdateLastCommentDate(newList, new Set<Id>());
    }
    public void UpdateLastCommentDateOnUpdate(Map<Id, Law_Item__c> newMap, Map<Id, Law_Item__c> oldMap){
        system.debug('^^^  UpdateLastCommentDateOnUpdate  method ont law item trigger');
        Set<Id> relevantSet = new Set<Id>(); 
        for(Id l : newMap.keySet()){
            if(oldMap.containsKey(l) && newMap.get(l).Last_Date_for_Comments__c != oldMap.get(l).Last_Date_for_Comments__c){
                relevantSet.add(l);
            }
        }
        UpdateLastCommentDate(newMap.values(), relevantSet);
    }


    public void UpdateLastCommentDate(List<Law_Item__c> newList, Set<Id> relIds){
        system.debug('^^^  UpdateLastCommentDate  method ont law item trigger');
        system.debug('^^^  newList '+ newList);

        for(Law_Item__c li :newList){
            if(li.Last_Date_for_Comments__c!= null && (relIds.isEmpty() || (li.Id != null && relIds.contains(li.Id)) )){
                li.Last_Date_for_Comments_Heb__c = Util_Email_Templates.getHebDate(li.Last_Date_for_Comments__c).containsKey('hebrew') ? Util_Email_Templates.getHebDate(li.Last_Date_for_Comments__c).get('hebrew'): null;
            }
        }
    }*/

///////////////////// cant use callouts in trigger /////////////////////


}