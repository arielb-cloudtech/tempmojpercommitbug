public without sharing class LawItemShareTriggerHandler {

    public LawItemShareTriggerHandler() {
	}	
    
    
    public void UpdatePermissions (Map<Id, Law_Item__c> newMap,  Map<Id, Law_Item__c> oldMap)
    {
        try
        {
            List<Law_Item__c> newList = new List<Law_Item__c>();

            for (Id newLawItem : newMap.KeySet()) 
            {
                if (Trigger.isInsert)
                    newList.add(newMap.get(newLawItem));
                else {
                    if (newMap.get(newLawItem).Status__c != oldMap.get(newLawItem).Status__c )
                        newList.add(newMap.get(newLawItem));
                }    
            }  

            System.debug('Tatiana newList  - ' + newList);

            if (newList.size() == 0)
                return;

            String currentUserId = UserInfo.getUserId();
        
            //All Contacts belong to MOJ
            Map<Id, Account> accMap = new Map<Id, Account>([SELECT Id FROM Account WHERE IsActive__c = true AND IsUnit__c = true]);
            Map<Id, Contact> allContactMap = new Map<Id, Contact> ([SELECT Id, User__c FROM CONTACT WHERE AccountId in :accMap.keySet() AND User__c != NULL AND User__c != :currentUserId]);
            Set<Id> allId = new Set<Id>();
            for (Id c : allContactMap.KeySet()) 
            {
                allId.add(allContactMap.get(c).User__c);
            }  

            //Users belong to LawItem
            for (Integer i = 0; i < newList.size(); i++){
            Law_Item__c Li = newList[i];
            String AccId = Li.Publisher_Account__c;
            System.debug('Tatiana Publisher_Account__c XXXXXXXXX - ' + AccId);
            Map<Id, Contact> contactMap = new Map<Id, Contact> ([SELECT Id, User__c FROM CONTACT WHERE AccountId = :AccId AND User__c != NULL AND User__c != :currentUserId]);           
            Set<Id> uId = new Set<Id>();
            if (contactMap.size() > 0){
                for (Id c : contactMap.KeySet()) 
                {
                    System.debug('Tatiana contactMap XXXXXXXXX - ' + contactMap.get(c).User__c + ' - ' + c);
                    uId.add(contactMap.get(c).User__c);
                } 
            }
            System.debug('Tatiana uId XXXXXXXXX - ' + uId.size());
            
            Map <Id, User> userMap = new Map<Id, User>([SELECT Id FROM USER WHERE Id IN :uId AND IsActive = true AND Id != :currentUserId]);         

            List<Law_Item__Share> shareList  = new List<Law_Item__Share>();
            for (Id u : userMap.KeySet()){
                    Law_Item__Share s = new Law_Item__Share(ParentID = Li.Id, UserOrGroupId = u);                    
                    s.AccessLevel = 'Edit';                    
                    System.debug('Tatiana XXXXXXXXX - ' + s);
                    shareList.add(s);
            }
            System.debug('Tatiana XXXXXXXXX shareList.size - ' + shareList);
            if (shareList.size() > 0)
            {
                System.debug('Tatiana XXXXXXXXX - ' + shareList);
                upsert shareList;
                System.debug('Tatiana XXXXXXXXX - DONE ALL');
            }

            //Users belong to MOJ and  do not belong to Law Item
            Map <Id, User> notLawItemUser = new Map<Id, User>([SELECT Id FROM USER WHERE Id IN :allId AND Id NOT IN :userMap.keySet() AND IsActive = true AND Id != :currentUserId]);
            
            Set<Id> notLawItemUserIds = new Set<Id>();
            notLawItemUserIds.addAll(notLawItemUser.keySet());
            System.debug('Tatiana XXXXXXXXX Status__c - ' + Li.Status__c);
            if (Li.Status__c != 'Distributed')
            {
                System.debug('Tatiana XXXXXXXXX Li.Status__c != Distributed ');
                List<Law_Item__Share> shareObjList = [SELECT Id FROM Law_Item__Share WHERE ParentID = :Li.Id AND UserOrGroupId IN :notLawItemUserIds];
                System.debug('Tatiana XXXXXXXXX shareObjList for delete - ' + shareObjList);
                if (shareObjList.size() > 0)
                    delete shareObjList;
            }
            else {
                System.debug('Tatiana XXXXXXXXX notLawItemUser.size - ' + notLawItemUser.size());
                shareList  = new List<Law_Item__Share>();
                for (Id u : notLawItemUser.KeySet()){
                    Law_Item__Share s = new Law_Item__Share(ParentID = Li.Id, UserOrGroupId = u);                     
                    s.AccessLevel = 'Read';
                    shareList.add(s);
                    System.debug('Tatiana XXXXXXXXX - ' + s);
                }
                
                if (shareList.size() > 0)
                {
                    System.debug('Tatiana XXXXXXXXX - ' + shareList);
                    upsert shareList;
                    System.debug('Tatiana XXXXXXXXX - DONE READ');

                }
            }
        }
        }catch(Exception ex)
        {
            System.debug('!!!Exception: ' + ex.getMessage() + ' At Line: ' + ex.getLineNumber() + ' Stack Trace: ' + ex.getStackTraceString());
        }
        

    }
}