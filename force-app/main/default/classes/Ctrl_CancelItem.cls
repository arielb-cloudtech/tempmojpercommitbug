public class Ctrl_CancelItem{

	private final Law_Item__c li;
	public Boolean hasError {get; set;}

	public Ctrl_CancelItem(ApexPages.StandardController controller){
		this.li = (Law_Item__c)controller.getRecord();
		hasError = false;
	}

	public PageReference cancelItem(){
		try {
				li.Status__c = 'Canceled';
				update li;
				PageReference p = new PageReference('/' + li.Id);
				p.setRedirect(true);
				return p;
		} catch (Exception ex){
			hasError = true;
			return null;
		}
	}
}