@isTest
public with sharing class Test_Ctrl_Header {
    
    public Test_Ctrl_Header() {

    }

    @TestSetup
    static void makeData(){
        //cls object creator instance
        ClsObjectCreator cls = new ClsObjectCreator();
        //comments
        Comment__c c = cls.createComment('Published');
        //comment replys
        Comment_Reply__c cr = cls.createCommentReply(true, false, c.Id);
        cr.Read__c =false;
        update cr;
    }

    @isTest
    public static void testGetSession(){
        Ctrl_Header.getSession();
    }

    @isTest
    public static void test1ChangeLang(){ //correct picklist value
        Ctrl_Header.changeLang('en_US');
    }

    @isTest
    public static void test2ChangeLang(){ // incorrect language picklist value - exception
        Ctrl_Header.changeLang('d,gonrfigbnifrgbirdeb');
    }

    @isTest
    public static void testGetData(){
        Ctrl_Header.getData(UserInfo.getUserId());
    }
}