public with sharing class ClsObjectCreator {

	 public User thisUser;

	public ClsObjectCreator () {
		 thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() limit 1];       
	}

	
	public Law__c createLaw(String LawId){
		Law__c l = new Law__c(LawID__c = '568879', Law_Name__c ='test');
		insert l;
		return l;
	}

	public Law__c returnLaw(String LawId, String someThing){
		Law__c l = new Law__c(LawID__c = '568879', Law_Name__c ='test '+someThing);
		return l;
	}

	public Account createAccount(String accName, Id recTypeId){
		Account acc = new Account(Name = accName, RecordTypeId = recTypeId);
		insert acc;
		return acc;
	}
	
	public Account returnAccount(String acclName, Id recTypeId){
		Account acc = new Account(Name=acclName, RecordTypeId = recTypeId);		
		return acc;
	}

	//public Profile getProfileByName(String prof){
 //         Profile p = new Profile();
 //         p = [SELECT Id, Name FROM Profile WHERE Name=:prof limit 1];
 //         return p; 
 //   }  
 	public Contact createContactWithAccount (id thiAccountId){
 		String randomstr = string.valueof( (Math.floor(Math.random() * 999999) + 1).intValue() ) ;
 		Contact con = new Contact ( User__c = null, ID_Card__c = '111111118' ,  FirstName= 'Testing123'+randomstr ,LastName='Testing123'+randomstr,Email = 'Test'+randomstr+'@gmail.com'+'.'+randomstr , Role__c = 'Referent'  ,  Active_User__c = true, AccountId= thiAccountId);
		System.runAs (thisUser) {
			insert con;
		}
		return con;
 	}

	public PermissionSet createPermissionSet (String PermissionSetName){
		PermissionSet pr = new PermissionSet(Label=PermissionSetName,Name=PermissionSetName);
		System.runAs (thisUser) {
			insert pr;
		}
		return pr; 
	}

	public void createDynamicPardotLists(){
		List<DynamicList__c> dls = new List<DynamicList__c>();
		dls.add(new DynamicList__c(Name	='Immediate Hebrew', Frequency__c='Immediate', Language__c='Hebrew', Pardot_List_Id__c='1111'));
		dls.add(new DynamicList__c(Name	='Immediate Arabic', Frequency__c='Immediate', Language__c='Arabic', Pardot_List_Id__c='2222'));
		dls.add(new DynamicList__c(Name	='Daily Hebrew', Frequency__c='Daily', Language__c='Hebrew', Pardot_List_Id__c='3333'));
		dls.add(new DynamicList__c(Name	='Daily Arabic', Frequency__c='Daily', Language__c='Arabic', Pardot_List_Id__c='4444'));
		dls.add(new DynamicList__c(Name	='Weekly Hebrew', Frequency__c='Weekly', Language__c='Hebrew', Pardot_List_Id__c='5555'));
		dls.add(new DynamicList__c(Name	='Weekly Arabic', Frequency__c='Weekly', Language__c='Arabic', Pardot_List_Id__c='6666'));
		insert dls;
	}

	public void createPardotCampign(){
		insert new Pardot_Campaign__c(Name='Email Distribution',PardotId__c='0000');
	}

	public Mailing_Subscription__c returnMailingSubsciptionsList(Id conId, String regType, String freq, String ministyId, String lang, String lawItemId, String lawId, Boolean withComments, String classify, String pref, String itemType, Boolean withRia){
		Mailing_Subscription__c lms = new Mailing_Subscription__c(Contact__c = conId,
																  Registration_Type__c = String.isNotBlank(regType)? regType : null, 
																  Mailing_Frequency__c = String.isNotBlank(freq)? freq : null, 
																  Ministry__c = String.isNotBlank(ministyId)? ministyId : null, 
																  Mailing_Language__c = String.isNotBlank(lang)? lang : null, 
																  Law_Item__c = String.isNotBlank(lawItemId)? lawItemId : null, 
																  Law__c = String.isNotBlank(lawId)? lawId : null, 
																  Include_Comments_Updates__c = withComments, 
																  Classification__c = String.isNotBlank(classify)? classify : null, 
																  Items_With_RIA__c = withRia,
																  Personal_Preference_Type__c = String.isNotBlank(pref)? pref : null, 
																  Item_Type__c = String.isNotBlank(itemType)? itemType : null);
		return lms;
	}

	public Law_Item__c returnLawItem(Id recTId, String hebName, String arbName, Id pubAccId, String type, String lawItemId, String lawId, Id accRespId, Boolean withRia,  String status){
		Law_Item__c lms = new Law_Item__c(RecordTypeId = recTId,
										  Law_Item_Name__c = String.isNotBlank(hebName)? hebName : null, 
										  Arabic_Name__c = String.isNotBlank(arbName)? arbName : null, 
										  Publisher_Account__c = String.isNotBlank(pubAccId)? pubAccId : null, 
										  Type__c = String.isNotBlank(type)? type : null, 
										  Law_Item__c = String.isNotBlank(lawItemId)? lawItemId : null, 
										  Law__c = String.isNotBlank(lawId)? lawId : null, 
										  Account_Responsibility__c = String.isNotBlank(accRespId)? accRespId : null, 
										  Publish_Date__c = Datetime.Now().addDays(-24), 
										  X2118_Regulations__c = true,
										  Last_Date_for_Comments__c = Datetime.Now().addDays(45),
										  Document_Brief__c = 'this is a Document Brief', 
										  RIA_Attachment__c = withRia==true? 'Attach RIA Report' : 'RIA Report Not Required', 
										  Status__c = String.isNotBlank(status)? status : null);
		return lms;
	}

	public ContentVersion returnContentVersion(String someText,  Boolean isRia, Boolean isMain){
		Blob beforeblob=Blob.valueOf('someText');
		ContentVersion cv = new ContentVersion();
        cv.title = 'title '+ someText;      
        cv.PathOnClient ='test';           
        cv.Main__c = true;           
        cv.isRIA__c = false;           
        cv.VersionData =beforeblob;  
		return cv;
	}

	public Void CreatePardotIntegration_CS(){
		Pardot_Integeration__c PI = new Pardot_Integeration__c();
		PI.Name='Standard';
		PI.API_Key__c='erdfgthjuk26a5d4sasdw5d4';
		PI.BASE_URL__c='https://PardotBase.com';
		PI.Password__c='ssshhhhPassword';
		PI.UserName__c='un@bla.com';
		insert PI;
	}


	public Classification__c returnClassification(String name, String calId){
		Classification__c c = new Classification__c(Name=name, ClassificiationID__c=calId);
		return c;
	}

	public Contact ReturnContact(Id accId, String fName, String lName, String pardotId, String email, String isrID){
		//return null;
		return new Contact(AccountId = accId, FirstName = fName, LastName = lName, Email = email, pi__url__c ='http://pi.pardot.com/prospect/read?id='+pardotId, ID_Card__c = isrID);
	}

	//overloading
	public Account createAccount(String name){
		Account a = new Account(Name = name);
		insert a;
		return a;
	}	

	//overloading
	public Account returnAccount(String name){
		Account a = new Account(Name = name);
		return a;
	}

	public Contact createContact(String ln, Id uId){
		Contact c = new Contact(LastName = ln, User__c = uId);
		insert c;
		return c;
	}	

	public Contact returnContact(String ln, Id uId){
		Contact c = new Contact(LastName = ln, User__c = uId);
		return c;
	}

	public Mailing_Subscription__c createMailingSubscription(Id u, String type){
            Mailing_Subscription__c ms = new Mailing_Subscription__c(User__c = u, Item_Type__c = type);
            insert ms;
            return ms;
    }

    public Mailing_Subscription__c returnMailingSubscription(Id u, String type){
            Mailing_Subscription__c ms = new Mailing_Subscription__c(User__c = u, Item_Type__c = type);
            return ms;
    }

	public ContentVersion createContentVersion(String title, String path, String vd){
		ContentVersion cv = new ContentVersion(Title = title, PathOnClient = path, versionData = Blob.valueOf(vd));//, ContentLocation = 'L', FirstPublishLocationId = null);
		insert cv; 
		return cv;
	}

	public ContentVersion returnContentVersion(String title, String path, String vd){
		ContentVersion cv = new ContentVersion(Title = title, PathOnClient = path, versionData = Blob.valueOf(vd));//, ContentLocation = 'L', FirstPublishLocationId = null);
		return cv;
	}

	public ContentDocumentLink createCDL(Id cdId, Id leId){
		ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = cdId, LinkedEntityId = leId, Visibility = 'ALLUsers', ShareType = 'I');
		insert cdl;
		return cdl;
	} 
	
	public ContentDocumentLink returnCDL(Id cdId, Id leId){
		ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = cdId, LinkedEntityId = leId, Visibility = 'ALLUsers', ShareType = 'I');
		return cdl;
	}

	public Case createCase(){
		Case c = new Case();
		insert c;
		return c;
	}

	public Case returnCase(){
		Case c = new Case();
		return c;
	}

	public Comment__c createComment(String status){
		Comment__c c = new Comment__c(Comment_Status__c = status, Display_Comment__c = true);
		insert c;
		return c;
	}

	public Comment__c returnComment(String status){
		Comment__c c = new Comment__c(Comment_Status__c = status, Display_Comment__c = true);
		return c;
	}

	public Comment_Reply__c createCommentReply(boolean dr, boolean read, id comment){
		Comment_Reply__c cr = new Comment_Reply__c(Display_Reply__c = dr, Read__c = read, Reply_to_Comment__c = comment);
		insert cr;
		return cr;
	}

	public Comment_Reply__c returnCommentReply(boolean dr, boolean read, id comment){
		Comment_Reply__c cr = new Comment_Reply__c(Display_Reply__c = dr, Read__c = read, Reply_to_Comment__c = comment);
		return cr;
	}

	public Law_Item__c createLawItem(String status, String name, Id acc){
		Law_Item__c li = new Law_Item__c(Status__c = status, Law_Item_Name__c = name, Account_Responsibility__c = acc);
		insert li;
		return li;
	}

	public Law_Item__c returnLawItem(String status, String name, Id acc){
		Law_Item__c li = new Law_Item__c(Status__c = status, Law_Item_Name__c = name, Account_Responsibility__c = acc);
		return li;
	}

	public Page_Parent__c createPageParent(String name){
		Page_Parent__c pp = new Page_Parent__c(Name = name);
		insert pp;
		return pp;
	}

	public Page_Parent__c returnPageParent(String name){
		Page_Parent__c pp = new Page_Parent__c(Name = name);
		return pp;
	}

	public Page_Child__c createPageChild(Id pp){
		Page_Child__c pc = new Page_Child__c(Page_Parent__c = pp);
		insert pc;
		return pc;
	}

	public Page_Child__c returnPageChild(Id pp){
		Page_Child__c pc = new Page_Child__c(Page_Parent__c = pp);
		return pc;
	}

	public Law__c createLaw(){
		Law__c l = new Law__c();
		insert l;
		return l;
	}

	public Law__c returnLaw(){
		Law__c l = new Law__c();
		return l;
	}

	public Classification__c createClassification(){
		Classification__c c = new Classification__c();
		insert c; 
		return c;
	}

	public Classification__c returnClassification(){
		Classification__c c = new Classification__c();
		return c;
	}

	public Law_Item_Classification__c createLIClassification(Id c, Id li){
		Law_Item_Classification__c lic = new Law_Item_Classification__c(Classification__c = c, Law_Item__c = li);
		insert lic;
		return lic;
	}

	public Law_Item_Classification__c returnLIClassification(Id c, Id li){
		Law_Item_Classification__c lic = new Law_Item_Classification__c(Classification__c = c, Law_Item__c = li);
		return lic;
	}

	public LawClassification__c createLC(Id c, Id l){
		LawClassification__c lc = new LawClassification__c(ClassificiationID__c =c, IsraelLawID__c = l);
		insert lc;
		return lc;
	}

	public LawClassification__c returnLC(Id c, Id l){
		LawClassification__c lc = new LawClassification__c(ClassificiationID__c =c, IsraelLawID__c = l);
		return lc;
	}

	public LawBinding__c createLawBinding(Id child, Id parent){
		LawBinding__c lb = new LawBinding__c(LawID__c = child, IsraelLawID__c = parent);
		insert lb;
		return lb;
	}

	public LawBinding__c returnLawBinding(Id child, Id parent){
		LawBinding__c lb = new LawBinding__c(LawID__c = child, IsraelLawID__c = parent);
		return lb;
	}

	public Related_to_Law__c createRelatedToLaw(Id liId, Id lId){
		Related_to_Law__c rtl = new Related_to_Law__c(Law_Item__c = liId, Law__c = lId);
		insert rtl;
		return rtl;
	}

	public Related_to_Law__c returnRelatedToLaw(Id liId, Id lId){
		Related_to_Law__c rtl = new Related_to_Law__c(Law_Item__c = liId, Law__c = lId);
		return rtl;
	}
}