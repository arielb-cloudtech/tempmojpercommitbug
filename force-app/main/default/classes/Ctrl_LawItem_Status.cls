public with sharing class Ctrl_LawItem_Status {
    
    @AuraEnabled    
    public static MAP<String, String> getStatusList(){    
       System.debug('ZZZZZZZZZZZZZZZZZZZZ - run getStatusList' );              
       Schema.DescribeFieldResult fieldResult = Comment__c.Comment_Status__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       MAP<String, String> mStatus = new MAP<String, String>();
       mStatus.put('','');
       for( Schema.PicklistEntry f : ple)
       {
           mStatus.put(f.getLabel() , f.getValue());
       }   
       return mStatus;
    }
    
}