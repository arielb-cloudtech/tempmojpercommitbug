public without sharing class Ctrl_LawItem_CommentPage {
	public Ctrl_LawItem_CommentPage() {

	}

	@AuraEnabled
	public static List<User> getUsers(String userPreffix){
		System.debug('~~~userList: ');

		List<User> userList = [SELECT Id, Name, Email FROM User WHERE Name LIKE :('%' + userPreffix + '%') LIMIT 5];
		System.debug('~~~userList: ' + userList);
		return userList;

	}

	@AuraEnabled
	public static String getUserId(){
		/*
		Contact con = [Select Id From Contact Where User__c =: userinfo.getUserId() Limit 1];
		return con.Id;*/
		return UserInfo.getUserId();

	}

	@AuraEnabled
	public static String saveReplyEdit(String replyId, String body, Boolean disp){
		Comment_Reply__c rep = new Comment_Reply__c(Id = replyId, Reply__c = body,
															Display_Reply__c = disp);
		update rep;
		return 'success';

	}

	@AuraEnabled
	public static String getUserInfo(){
		List<User> userList = [SELECT Id, Name, Email, UserRole.Name, Company__c, Role__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
		System.debug('~~~userList: ' + userList);
		return JSON.serialize(userList);

	}

	@AuraEnabled
	public static void deletePdfFile(String documentId){
		ContentDocument cd = [SELECT Id FROM ContentDocument WHERE Id = :documentId];
		delete cd;
	}

	@AuraEnabled
	public static WrapperDocument downloadPdfFile(String fileBody){
		System.debug('~~~CreateAttach');
		Blob txtBlob = Blob.valueOf(fileBody); //Convert it to a blob

		System.debug('~~~fileBody: ' + fileBody);

		ContentVersion cv = new ContentVersion();
		cv.Title = 'קובץ הערות.txt';
		cv.PathOnClient = 'קובץ הערות.txt';
		cv.VersionData = Blob.valueOf(fileBody);
		insert cv;
		Id cdId = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :cv.Id LIMIT 1].Id;
		System.debug('~~~cdId: ' + cdId);
		String downloadUrl = System.URL.getSalesforceBaseUrl().toexternalform() + '/sfc/servlet.shepherd/version/download/' + cv.Id;
		System.debug('~~~downloadUrl: ' + downloadUrl);
		return new WrapperDocument(downloadUrl, cdId);
	}



	@AuraEnabled
	public static String CreateComment(String action, String lawItemId, String commentId){
		if (action == 'comment'){
			Comment__c newComment = new Comment__c(Comment_Type__c = 'Government User Comment',
													Related_to_Law_Item__c = lawItemId,
													Display_Comment__c = false);
			insert newComment;
			return newComment.Id;
		}
		else {
			Comment_Reply__c newReply = new Comment_Reply__c(Reply_to_Comment__c = commentId,
															Related_to_Item__c = lawItemId,
															Display_Reply__c = false);
			insert newReply;
			return newReply.Id;
		}
	}


	@AuraEnabled
	public static void CancelCommentUpdate(List<String> filesList) {
		if (filesList != null && filesList.size() > 0)
		{
			List<ContentDocument> cd = [SELECT id FROM ContentDocument WHERE id in :filesList];
			delete cd;
		}
	}

	@AuraEnabled
	public static void AddChatter(List<String> commentIDList, String userId)
	{
		System.debug('MMM: AddChatter ...');
		System.debug('MMM: commentIdList=' + commentIDList.size());


		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/';


		for(String  commentID : commentIDList){
			String body = '\n טקסט כלשהוא במידת הצורך \n' + baseUrl + commentID;
			ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
			ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
			ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
			ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

			messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

			mentionSegmentInput.id = Id.valueOf(userId);
			messageBodyInput.messageSegments.add(mentionSegmentInput);

			textSegmentInput.text = body;
			messageBodyInput.messageSegments.add(textSegmentInput);

			feedItemInput.body = messageBodyInput;
			feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
			feedItemInput.subjectId = 'me';

			ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);

		}
	}

	@AuraEnabled
	public static WrapperComment SaveComment(String body, String commentId, String status, List<String> filesToDelete, Boolean dispCom, String subComments, String deleteComments) {

		System.debug('ZZZZZZZZZZZZZZZZZZZZ - run SaveComment ' + subComments);
		if(true)//body != null && !String.isBlank(body))
		{
	        User u = [SELECT Id, ContactId FROM user where id = :userinfo.getUserId() LIMIT 1];
			Id contId = u.ContactId;
			if(u.ContactId == null || String.isBlank(u.ContactId))
	        {
	        	List<Contact> con = [Select Id From Contact Where User__c =: userinfo.getUserId() Limit 1];	 
				if(con != null && !con.isEmpty())
				{
					contId = con[0].Id;
				}       	
	        }

			List<Comment__c> allComments = new List<Comment__c>();

			List<WrapperCommentSection> sectionList = new List<WrapperCommentSection>();
			if (subComments != null && subComments != '') {
				List <WrapperCommentSection> subCommentList = (List<WrapperCommentSection>)JSON.deserialize(subComments, List<WrapperCommentSection>.class);   
			
				for (WrapperCommentSection sec : subCommentList) {					
					
					Comment__c subComment = new Comment__c(Bullet_Number__c = sec.bulletNumber,
														Id = sec.subCommentId,
														Comment_Text__c = sec.body,
														Sub_Comment__c = id.valueof(commentId),
														Comment_Status__c = status,														
														Display_Comment__c = true,
														Is_Private_Comment__c = !dispCom,
														Comment_By__c = contId);	
					allComments.add(subComment);
					WrapperCommentSection wcs = new WrapperCommentSection(sec.bulletNumber, sec.body, sec.subCommentId);
					sectionList.add(wcs);
				}
			}

			if (deleteComments != null && deleteComments != '') {
				List <WrapperCommentSection> deleteCommentList = (List<WrapperCommentSection>)JSON.deserialize(deleteComments, List<WrapperCommentSection>.class);  
				List<Id> delId = new List<Id>();
				for (WrapperCommentSection sec : deleteCommentList) {
					delId.add(sec.subCommentId);
				}

				List<Comment__c> delList = [SELECT Id FROM Comment__c WHERE Id in :delId];
				delete delList;
			}

			Comment__c newComment = new Comment__c(Id = id.valueof(commentId),
													Comment_Text__c = body,
													Comment_Status__c = status,													
													Display_Comment__c = true,
													Is_Private_Comment__c = !dispCom,
													Comment_By__c = contId);
			allComments.add(newComment);

			upsert allComments;
			System.debug(newComment);
			List<WrapperCommentReply> replys = new List<WrapperCommentReply>();

			System.debug('ZZZZZZZZZZZZZZZZZZZZ - deletedFiles' + 'size = ' + filesToDelete.size() + ' ' + filesToDelete  );

			if (filesToDelete != null && filesToDelete.size() > 0)
			{
				List<ContentDocument> cd = [SELECT id FROM ContentDocument WHERE id in :filesToDelete];
				delete cd;
			}
			Comment__c updatedComment = [SELECT Id, Comment_Text__c, Comment_Status__c, Is_Private_Comment__c FROM Comment__c WHERE Id = :newComment.Id];

			//
			
			//
			Map<Id, ContentDocumentLink> cdlMap = new Map<Id, ContentDocumentLink>([SELECT Id, ContentDocumentId, LinkedEntityId
																					FROM ContentDocumentLink
																					WHERE LinkedEntityId = :commentId]);
			if (cdlMap != null && cdlMap.size() > 0) {
				Map<Id, List<WrapperFile>> entityToVersions = new Map<Id, List<WrapperFile>>();

				for(Id cdlId : cdlMap.keySet()){
					if(!entityToVersions.containsKey(cdlMap.get(cdlId).LinkedEntityId)){
						entityToVersions.put(cdlMap.get(cdlId).LinkedEntityId, new List<WrapperFile>());
					}
					entityToVersions.get(cdlMap.get(cdlId).LinkedEntityId).add(new WrapperFile(cdlMap.get(cdlId).ContentDocumentId));
				}
				//TODO
				WrapperComment c = new WrapperComment(updatedComment.Id, updatedComment.Comment_Text__c, replys, entityToVersions.get(commentId), null, updatedComment.Comment_Status__c, sectionList);
				c.bEditable = true;
				c.bFiles = true;
				//if (updatedComment.Comment_Status__c == 'Published Private')
				//	c.bPrivate = true;
				if (updatedComment.Is_Private_Comment__c)
					c.bPrivate = true;
				return c;
			}
			else {
				//TODO
				WrapperComment c = new WrapperComment(updatedComment.Id, updatedComment.Comment_Text__c, replys, null, null, updatedComment.Comment_Status__c, sectionList);
				c.bEditable = true;
				c.bFiles = false;
				//if (updatedComment.Comment_Status__c == 'Published Private')
				//	c.bPrivate = true;
				if (updatedComment.Is_Private_Comment__c)
					c.bPrivate = true;
				return c;
			}
		}
		return null;
    }


	@AuraEnabled
	public static void DeleteComment(String commentId) {
		System.debug('ZZZZZZZZZZZZZZZZZZZZ - run DeleteComment' );
			LIST<ContentDocumentLink> cdl = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :commentId];
			if (cdl != null && cdl.size() > 0){
				Set <Id> lId = new Set <Id>();
				for(ContentDocumentLink c : cdl){
					lId.Add(c.ContentDocumentId);
				}
				List<ContentDocument> cd = [SELECT id FROM ContentDocument WHERE id in :lId];
				delete cd;
			}
			Comment__c newComment = [SELECT Id,  Comment_Text__c, Comment_Status__c FROM Comment__c WHERE Id = :commentId];
			delete newComment;
    }

	@AuraEnabled
	public static WrapperData getData(String lawItemId){
		System.debug('~~~ in getData method');

		Law_Item__c lawItemRecord = [SELECT Id, Last_Date_for_Comments__c FROM Law_Item__c WHERE Id = :lawItemId LIMIT 1];
		System.debug('!!!lawItemRecord: ' + lawItemRecord);

		User u = [SELECT Id, ContactId FROM user where id = :userinfo.getUserId() LIMIT 1];
		List<Contact> conLst = [Select Id, User__c From Contact Where User__c =: userinfo.getUserId() Limit 1];
		Contact con;
		if(conLst != null && !conLst.isEmpty())
		{
			con = conLst[0];
		}
		if(con == null && u.ContactId != null)
		{
			con = new Contact(Id = u.ContactId);
		}

		Map<Id, Comment__c> lawItemCommentsMap = new Map<Id, Comment__c>([SELECT Id, Related_to_Law_Item__c, Comment_Status__c, Is_Private_Comment__c,
																			Comment_Text__c, Comment_By__c, Comment_By__r.Name, Comment_By__r.User__r.Role__c,
																			Comment_By__r.User__r.Company__c, CreatedById, OwnerId,
																			(SELECT Id, Reply__c, OwnerId, Display_Reply__c FROM Ministry_Responses__r ORDER BY CreatedDate ASC),
																			(SELECT Id, Comment_Text__c, Bullet_Number__c FROM Comments1__r ORDER BY Bullet_Number__c ASC)
																		FROM Comment__c
																		WHERE Related_to_Law_Item__c = :lawItemId AND Display_Comment__c = true AND Sub_Comment__c = null
																			AND (Comment_Status__c != 'Draft' OR (Comment_Status__c = 'Draft' AND OwnerId = :u.Id)) ORDER BY CreatedDate DESC]);

		if (lawItemCommentsMap == null || lawItemCommentsMap.size() == 0){
			return new WrapperData(new List<WrapperComment>(), 'fail', lawItemRecord.Last_Date_for_Comments__c);
		}
		Map<Id, Comment_Reply__c> commentReplyMap = new Map<Id, Comment_Reply__c>();

		for(Id commentId : lawItemCommentsMap.keySet()){
			for(Comment_Reply__c reply : lawItemCommentsMap.get(commentId).Ministry_Responses__r){
				commentReplyMap.put(reply.Id, reply);
			}
		}
		Set<Id> entityIds = new Set<Id>();
		entityIds.addAll(lawItemCommentsMap.keySet());
		entityIds.addAll(commentReplyMap.keySet());
		System.debug('~~~entityIds: ' + entityIds);
		Map<Id, ContentDocumentLink> cdlMap = new Map<Id, ContentDocumentLink>([SELECT Id, ContentDocumentId, LinkedEntityId
																				FROM ContentDocumentLink
																				WHERE LinkedEntityId IN :entityIds]);
		Map<Id, List<WrapperFile>> entityToVersions = new Map<Id, List<WrapperFile>>();

		for(Id cdlId : cdlMap.keySet()){
			if(!entityToVersions.containsKey(cdlMap.get(cdlId).LinkedEntityId)){
				entityToVersions.put(cdlMap.get(cdlId).LinkedEntityId, new List<WrapperFile>());
			}
			entityToVersions.get(cdlMap.get(cdlId).LinkedEntityId).add(new WrapperFile(cdlMap.get(cdlId).ContentDocumentId));
		}

		System.debug('~~~entityToVersions: ' + entityToVersions);

		List<WrapperComment> comments = new List<WrapperComment>();
		String currentUser = userinfo.getUserId();
		Boolean	bEditable;

		for(Id commentId : lawItemCommentsMap.keySet()){
			List<WrapperCommentReply> replys = new List<WrapperCommentReply>();

			for(Comment_Reply__c reply : lawItemCommentsMap.get(commentId).Ministry_Responses__r){
				WrapperCommentReply replyToAdd = new WrapperCommentReply(reply.Id, reply.Reply__c, entityToVersions.get(reply.Id), reply.OwnerId, reply.Display_Reply__c);

				if(entityToVersions.get(reply.Id) != null && !entityToVersions.get(reply.Id).isEmpty()){
					replyToAdd.bHaveFiles = true;
				}else{
					replyToAdd.bHaveFiles = false;
				}
				replys.add(replyToAdd);
			}

			List<WrapperCommentSection> sections = new List<WrapperCommentSection>();
			for(Comment__c section : lawItemCommentsMap.get(commentId).Comments1__r){
				sections.add(new WrapperCommentSection(section.Bullet_Number__c, section.Comment_Text__c, section.Id));
			}

			WrapperComment commentToAdd = new WrapperComment(commentId,
											lawItemCommentsMap.get(commentId).Comment_Text__c,
											replys,
											entityToVersions.get(commentId),
											lawItemCommentsMap.get(commentId).Comment_By__r.Name,
											lawItemCommentsMap.get(commentId).Comment_Status__c,
											sections);

			commentToAdd.uComp = lawItemCommentsMap.get(commentId).Comment_By__r.User__r.Company__c;
			commentToAdd.uRole = lawItemCommentsMap.get(commentId).Comment_By__r.User__r.Role__c;

			if(entityToVersions.get(commentId) != null && !entityToVersions.get(commentId).isEmpty()){
				commentToAdd.bFiles = true;
			}else{
				commentToAdd.bFiles = false;
			}
			if(lawItemCommentsMap.get(commentId).Is_Private_Comment__c){
				commentToAdd.bPrivate = true;
			}else{
				commentToAdd.bPrivate = false;
			}

			if (lawItemCommentsMap.get(commentId).OwnerId == currentUser)
				commentToAdd.bEditable = true;
			else
				commentToAdd.bEditable = false;
			comments.add(commentToAdd);
		}

		System.debug('~~~comments: ' + comments);
		System.debug('~~~lawItemRecord.Last_Date_for_Comments__c: ' + lawItemRecord.Last_Date_for_Comments__c);
		return new WrapperData(comments, 'success', lawItemRecord.Last_Date_for_Comments__c);

	}

	@AuraEnabled
	public static WrapperCommentReply SaveNewReply(String body, String replyId, List<String> filesToDelete, Boolean disp) {
		System.debug('ZZZZZZZZZZZZZZZZZZZZ - run SaveNewReply' + body );
		System.debug('~~~ filesToDelete: ' + filesToDelete );

		Comment_Reply__c newReply = new Comment_Reply__c(Id = id.valueof(replyId),
															Reply__c = body,
															Display_Reply__c = disp);

		update newReply;
		System.debug(newReply);

		System.debug('ZZZZZZZZZZZZZZZZZZZZ - deletedFiles' + 'size = ' + filesToDelete.size() + ' ' + filesToDelete  );

		if (filesToDelete != null && filesToDelete.size() > 0)
		{
			List<ContentDocument> cd = [SELECT Id FROM ContentDocument WHERE Id IN :filesToDelete];
			delete cd;
		}
		Map<Id, ContentDocumentLink> cdlMap = new Map<Id, ContentDocumentLink>([SELECT Id, ContentDocumentId, LinkedEntityId
																				FROM ContentDocumentLink
																				WHERE LinkedEntityId = :replyId]);

		Comment_Reply__c udatedReply = [SELECT Id, Reply__c FROM Comment_Reply__c WHERE Id = :newReply.Id];

		if (cdlMap != null && cdlMap.size() > 0) {
			Map<Id, List<WrapperFile>> entityToVersions = new Map<Id, List<WrapperFile>>();

			for(Id cdlId : cdlMap.keySet()){
				if(!entityToVersions.containsKey(cdlMap.get(cdlId).LinkedEntityId)){
					entityToVersions.put(cdlMap.get(cdlId).LinkedEntityId, new List<WrapperFile>());
				}
				entityToVersions.get(cdlMap.get(cdlId).LinkedEntityId).add(new WrapperFile(cdlMap.get(cdlId).ContentDocumentId));
			}
			WrapperCommentReply c = new WrapperCommentReply(udatedReply.Id, udatedReply.Reply__c, entityToVersions.get(replyId));
	 		c.bHaveFiles = true;
	 		return c;
		}
		else {
			WrapperCommentReply c = new WrapperCommentReply(udatedReply.Id, udatedReply.Reply__c, null);
			c.bHaveFiles = false;
	 		return c;
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	public class WrapperData{
		@AuraEnabled public List<WrapperComment> mComments;
		@AuraEnabled public String mString;
		@AuraEnabled public Boolean mAllowToComment;

		public WrapperData(List<WrapperComment> comments, String msg, Datetime lastDateforComments){
			mString = msg;
			mComments = comments;
			System.debug('!!!lastDateforComments: ' + lastDateforComments);
			System.debug(lastDateforComments > Datetime.now());
			if(lastDateforComments == null || lastDateforComments > Datetime.now()){
				mAllowToComment = false;
			}else {
				mAllowToComment = true;
			}
			System.debug(mAllowToComment);
		}
	}

	public class WrapperComment{
		@AuraEnabled public String mId;
		@AuraEnabled public String mBody;
		@AuraEnabled public List<WrapperCommentReply> mCommentReplys;
		@AuraEnabled public String uName;
		@AuraEnabled public String uComp;
		@AuraEnabled public String uRole;
		@AuraEnabled public Boolean bFiles;
		@AuraEnabled public Boolean bPrivate;
		@AuraEnabled public Boolean bEditable;
		@AuraEnabled public String mStatus;
		@AuraEnabled public List<WrapperCommentSection> sections;

		// @AuraEnabled public List<String> mVersionIds;
		@AuraEnabled public List<WrapperFile> mFiles;

		public WrapperComment(String recordId, String body, List<WrapperCommentReply> commentReplys, List<WrapperFile> versionIds, string userName, string status, List<WrapperCommentSection> commentSections){
			mId = recordId;
			mBody = body;
			mCommentReplys = commentReplys;
			mFiles = versionIds;
			uName = userName;
			//uComp = comp;
			//uRole = role;
			mStatus = status;
			sections = commentSections;
		}
	}

	public class WrapperCommentReply{
		@AuraEnabled public String mId;
		@AuraEnabled public String mUser;
		@AuraEnabled public String mBody;
		// @AuraEnabled public List<String> mVersionIds;
		@AuraEnabled public List<WrapperFile> mFiles;
		@AuraEnabled public Boolean bHaveFiles = false;
		@AuraEnabled public Boolean bDisplay = false;

		public WrapperCommentReply(String recordId, String body, List<WrapperFile> versionIds){
			mId = recordId;
			mBody = body;
			mFiles = versionIds;
		}
		public WrapperCommentReply(String recordId, String body, List<WrapperFile> versionIds, String uId, Boolean isPublish){
			mId = recordId;
			mBody = body;
			mFiles = versionIds;
			mUser = uId;
			bDisplay = isPublish;
		}
	}

	public class WrapperFile{
		@AuraEnabled public String mId;
		@AuraEnabled public String documentId;
		// @AuraEnabled public String mBody;
		// @AuraEnabled public List<String> mVersionIds;

		public WrapperFile(String recordId/* , String body, List<String> versionIds */){
			mId = recordId;
			documentId = mId;
			// mBody = body;
			// mVersionIds = versionIds;
		}
	}

	public class WrapperDocument{
		@AuraEnabled public String mDownloadUrl;
		@AuraEnabled public String mId;

		public WrapperDocument(String downloadUrl, String docId){
			mDownloadUrl = downloadUrl;
			mId = docId;
		}
	}

	public class WrapperCommentSection{
		@AuraEnabled public String bulletNumber;
		@AuraEnabled public String body;
		@AuraEnabled public String subCommentId;

		public WrapperCommentSection(String sectionNumber, String text, String commId){
			bulletNumber = sectionNumber;
			body = text;
			subCommentId = commId;
		}
	}
}