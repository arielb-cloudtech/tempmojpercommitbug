trigger LawItemTrigger on Law_Item__c ( before insert, before update, after insert, after update) {
	LawItemTriggerHandler Handler = new LawItemTriggerHandler();

    if ( Trigger.isInsert ) {
        if ( Trigger.isBefore ) {
            Handler.splitDocBriefInsert(Trigger.new);
        } 
        else if ( Trigger.isAfter ) {
            Handler.ImmediateMailing(Trigger.new, trigger.newMap, 'Hebrew');
            // Handler.ImmediateMailing(Trigger.new, trigger.newMap, 'Arabic');
            Handler.UnCheckNotify(Trigger.new);

        } 
    }
    else if ( Trigger.isUpdate) {

        if ( Trigger.isBefore ) {
            Handler.splitDocBriefUpdate(Trigger.newMap, Trigger.oldMap);
        } 
        else if (Trigger.isAfter && Trigger.isUpdate) {
            Handler.ImmediateMailing(Trigger.new, trigger.newMap, 'Hebrew');
            // Handler.ImmediateMailing(Trigger.new, trigger.newMap, 'Arabic');
            Handler.UnCheckNotify(Trigger.new);
        }
    }

    if ( trigger.isBefore ) {
        for( Law_Item__c item : Trigger.new ) {
        }
    }
}