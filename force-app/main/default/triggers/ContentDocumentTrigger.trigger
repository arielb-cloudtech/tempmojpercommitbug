trigger ContentDocumentTrigger on ContentDocument (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {
		ContentDocumentTriggerHandler handler = new ContentDocumentTriggerHandler();

		if (Trigger.isBefore) {
	    	if(Trigger.isInsert)
	    	{
	    		handler.beforeIns(Trigger.new);
	    	}
	    
		} else if (Trigger.isAfter) {
	    	//call handler.after method
	    
		}
}