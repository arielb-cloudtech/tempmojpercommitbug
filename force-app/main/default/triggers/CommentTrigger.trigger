trigger CommentTrigger on Comment__c (after delete, after insert, after undelete,  after update, before delete, before insert, before update) {
	CommentTriggerHandler handler = new CommentTriggerHandler();

	if (Trigger.isBefore && Trigger.isInsert) {
		handler.ProfanityHandler(trigger.new);
    } else if (Trigger.isAfter && Trigger.isInsert) {
    } else if (Trigger.isBefore && Trigger.isUpdate) {
    	handler.ProfanityHandler(trigger.new);
    } else if (Trigger.isAfter && Trigger.isUpdate) {
    } else if (Trigger.isBefore && Trigger.isDelete) {
    }

}